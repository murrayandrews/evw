# Simple Makefile to generate man files from markdown
#
#MD_TO_MAN = ronn --roff --organization MA --manual AWStools --date `date +'%Y-%m-%d'`
MD_TO_MAN = ronn --roff --organization MA --manual AWStools
CFDOC=cfdoc -f md

LOCAL_MANFILES=evworker.1
MAN_SECTION=1
MANFILES=$(addprefix $(MAN)/man$(MAN_SECTION)/,$(LOCAL_MANFILES))


#$(MAN)/man*/%: %
#	$(INSTALL) -m $(MANMODE) $< $@

%.$(MAN_SECTION): %.md
	$(MD_TO_MAN) $<

%.cfn.md: %.cfn.json
	$(CFDOC) $< > $@

man:	$(LOCAL_MANFILES)  # Create man files locally

clean:	
	$(RM) $(LOCAL_MANFILES)

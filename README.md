# AWS Lambda Event Worker (Evw)

## Quick Summary

**Evw** is a flexible deployment framework that allows the same Python event
handler  to be run in AWS Lambda or a remote Linux/Mac OSx environment and for
AWS events to be seamlessly handed off from Lambda to a remote worker.

It supports multiple deployment options without recoding event handlers.

**Evw** now requires Python 3.6+.

## The Problem


AWS Lambda is a useful microservice environment for handling AWS _events_, (not
a patch on Google App Engine, aka GAE, IMHO), but it has some limitations:

*   Lambda functions have a maximum run time of 5 minutes (unlike GAE with its
    various queuing mechanisms). While this is ok for
    many tasks, it's not suitable for some naturally long running tasks, such as
    loading large data sets into Redshift.

*   Lambda functions are not easy to debug because there is no desktop development
    environment that integrates nicely with debuggers, IDEs etc (unlike GAE with
    its desktop SDK and native support from IDEs such as PyCharm). The functions
    must run in Lambda itself which has a medieval logging mechanism using
    CloudWatch.  Debugging basically means inserting lots of log/print
    statements, uploading to Lambda, triggering the function initiating event
    and then hunting through CloudWatch log streams to find your output. Then
    doing it all again. And again.  Seriously.

*   Lambda function code is fairly specific to the Lambda environment and some
    redevelopment is required if it needs to be migrated to run in another
    environment.
    
## About Evw

**Evw** is a simple Python based framework that attempts to address these
limitations by separating the operating environment wrapper from the actual
event processing logic for asynchronous events.

It consists of the following primary components:

1.  **evlambda**: A simple wrapper that runs in AWS Lambda. It invokes an
    application specific plugin to process events within Lambda or to hand them
    off (via SQS) to a remote worker running elsewhere.

2.  **evworker**: A multi-threaded daemon that runs in a Linux/Mac OSx environment.
    It invokes exactly the same application specific plugin to process events
    that have been handed off by **evlambda** or delivered directly by some
    other AWS event source (e.g. S3 events delivered by SQS).
    [More information on evworker](evworker.md)

3.  **Event processing plugins**: These have a very simple structure that allow
    you to concentrate on the event processing logic rather than the peculiarities
    of the run-time context or the deployment pattern. There are a number of
    existing plugins and it is straightforward to implement new ones.
    [More information on plugins](handlers/README.md)

## Event Processing Plugins

Event processing plugins are Python classes that inherit from the
`common.event.EventProto()` abstract base class. Plugin classes are required to
implement only one method. A couple of other methods provide additional control.

|Method|Required|Description|
|-|-|-|
|process()|Yes|Process an incoming event. This may be invoked within Lambda (by **evlambda**) or within a remote event worker (by **evworker**). It must be thread-safe. Fear not ... it's not that hard.|
|handoff()|No|Decide whether to handoff an incoming event to a remote worker.|
|finish()|No|Final cleanup after an event has been processed.|

The `handoff()` method is key. It is run only only within the Lambda
environment. If it returns `False`, the event is sent to the `process()` method
within Lambda. The default implementation always returns `False`.

If the `handoff()` method returns `True`, the **evlambda** wrapper will place
the event into an SQS queue where it can be retrieved by any **evworker**
attached to the queue. The **evworker** runs exactly the same event processing
plugin and uses the same `process()` method to handle the event but without the
time limitations of Lambda. The remote **evworker** is also a more convenient
place for debugging the event processing logic than within Lambda.

The **evworker** worker can also process events placed directly on the
appropriate SQS queue by an AWS service rather than by Lambda. For example, S3
events can be sent directly to an SQS queue, bypassing Lambda, and the
**evworker** can process them directly from there.

When running in Lambda, an event handler plugin has a maximum of 5 minutes
run time. When running in **evorker**, it has up to 12 hours (depending on
the SQS queue configuration.)

[More information on plugins](handlers/README.md)

## Deployment Patterns

**Evw** can support various deployment patterns simply by manipulating the
value returned by the event plugin's `handoff()` method.

Note that it is possible to manually override handoff behaviour using
Lambda environment variables (see below). This is
generally used for debugging

![](misc/evw-1.png)

### Lambda only event processing

If the event processing plugin runs in Lambda and the `handoff()` method
always returns `False`, events will always be sent to the plugin's
`process()` method within the Lambda environment.

This is suitable for short lived tasks invoked as a result of a trigger
sent to Lambda.

### Lambda unconditional handoff to a remote event worker 

If the event processing plugin runs in Lambda and the `handoff()` method
always returns `True`, events will always be sent to an SQS queue for 
remote processing by a remote event worker (**evworker**). The remote worker
runs exactly the same plugin as would have run within Lambda.

This option is particularly useful for debugging a Lambda function, as the
remote worker can be running on a desktop development environment (subject to
all of the normal AWS IAM and related security guff).

This allows the event handling logic to be exercised within a local desktop
environment.  Normal IDE tools, debuggers etc can be used.

### Lambda conditional handoff to a remote event worker

If the event processing plugin runs in Lambda, the `handoff()` method can
inspect the event details and decide whether to process the event locally or
dispatch it to a remote event worker. Typically, short running tasks will be
processed in Lambda and long running tasks will be handed off to remote event
workers via SQS.

### Direct remote worker invocation

In the cases above, AWS must be configured to send the trigger event to the
relevant Lambda function.

In some cases, AWS can submit events directly to an SQS queue, bypassing Lambda.
S3, for example can do this.

Remote event workers can attach directly to the relevant SQS queue and process
events.

## Lambda Environment Variables

AWS Lambda allows environment variables to be made available to Lambda functions.
These can be set from the AWS Lambda console or via the CLI.

**Evlambda** recognises the following (optional) environment variables.

|Name|Description|
|-|-|
|EVW_LOGLEVEL|The logging level. The standard logging level names are available but `debug`, `info`, `warning` and `error` are most useful. The Default is `info`.|
|EVW_HANDOFF|This can be used to override the normal handoff behaviour between **evlambda** and **evworker**. If set to `never`, all handoff is suppressed and the `handoff()` method is not called. If set to `always`, handoff is forced and the `handoff()` method is not called. All other values result in the default handoff behaviour via the `handoff()` decision method.|
|EVW_WORKER|The name of the worker module. If not specified, this is derived from the Lambda function name. **Evlambda** will attempt to import the worker module using this name.|

## Evworkers

Event worker instances (**evworker**) are multi-threaded programs that read
incoming events from an SQS queue and process them by running the same
plugin as the Lambda wrapper (**evlambda**).

You can run as many evworkers as required, either interactively or as a daemon.


Because the retrieve events from AWS SQS they do not need to coordinate
with one another.

Subject to AWS IAM requirements, they can run on EC2 instances, Docker nodes
or other Linux-like environments (including Mac OSx).

Windows is not supported.

[More information on evworker](evworker.md)

## Getting Started

Installation involves the following steps:

1. Get the software.
2. Select (or write) an event processing plugin.
3. Create the Lambda Function in AWS.
4. Create the SQS queue in AWS (if event handoff is required).
5. Setup messenger channels.
6. Upload the **evlambda** bundle to AWS Lambda.
7. Install the **evworker** bundle on a worker node.
8. Create the event trigger in AWS.
9. Run the **evworker**.

### 1. Get the software

Follow these instructions. Don't get creative.

1. Install the AWS CLI

2. Clone the **evw** repository.

3. Change directory to the top level of the cloned repository.

4. Create a Python virtual environment:

    `virtualenv -p python3 venv`
    
5. Activate the virtual environment:

    `source venv/bin/activate`
    
6. Install the required Python components:

    `pip install -r requirements.txt`
    
7. [Install](etc/evw-install.md) the **evworker**:

    `etc/evw-install.sh`.

    This will prompt for an install location. The default is /usr/local
    but it can be installed anywhere where the user has appropriate permissions.

8.  Make sure **evworker** can run:

    `evworker -h`
    
    This should produce a help listing.
    

### 2. Select (or write) an event processing plugin

[Event processing plugins](handlers/README.md) are Python modules located in the `handlers` directory.

Each plugin will have definition of an `Event` class that inherits from `EventProto`.

You can either use one of the existing plugins or write your own.

To get started, there are two demo plugins available that simply log the incoming
event.

* **demo_l**: This plugin has a `handoff()` method that always returns `False` so
  the processing is always done within Lambda. 
  
* **demo_w**: This plugin has a  `handoff()` method that always returns `True` so
  the event is always handed off to a remote worker.

The plugin name is used in the name of the Lambda function and also as an argument
to **evworker** so they know which plugin to load.

### 3. Create the Lambda function in AWS

In this step we create a spot for the **evlambda** wrapper to reside. The actual
function contents don't matter at this stage but its import to get the IAM aspects
correct.

1. Login to the AWS console and go to the Lambda service console.

2. Create a new Lambda function, selecting the `Blank Function` blueprint.
    You can skip the triggers step for now. 
   
3.  Select the _Python 3.6_ runtime.

4.  Set the name of the function to be one of the following forms:
   
    `plugin` or `plugin-xxx`

    This tells **evlambda** to load the `plugin` event handler. The
   trailing `-xxx` is optional and arbitrary. It allows multiple Lambda
   functions to use the same plugin without interfering with one another.

    The Lambda function name is also referred to as the _worker name_ and will be
   supplied as an argument to **evworker**.

    If using the **demo_w** plugin, the Lambda function name could be `demo_w` or
   something like `demo_w-MyFunction`.

    Don't worry about entering any Python code - that will get uploaded in a
   later step.

    The Lambda handler name is `evlambda.lambda_handler`. You can set this now
   or it will get set later if the  upload script provided is used.

5. You may need to change the Lambda function configuration to select
   appropriate IAM roles, timeouts etc.

### 4. Create the SQS queue in AWS

If the **evlambda** plugin has the option of event handoff to a remote
**evworker**, it does this by placing messages into an SQS queue which must be
created first.

1. If the Lambda function is named `funcName`, create an SQS queue named `evw-funcName`.

    i.e. If the Lambda function is called `demo_w-MyFunction`, the SQS queue
    will be named `evw-demo_w-MyFunction`.

2. Configure the queue to allow enough time for remote workers to handle events.

   AWS currently allows this to be up to 12 hours. Note that if the **evworker**
   does not indicate to SQS within this time that the event has been handled, it
   will be automatically placed back on the queue for it to be reprocessed. This
   means that in the event of an **evworker** crash, events can be picked up by
   another worker.  It also means there is a possibility of an event being
   processed more than once.

2. Make sure the the IAM role assigned to the the Lambda function allows it to send
   messages to the queue.
   
3. If the Lambda function is running in a customer VPC, make sure it has access to
   the relevant AWS API endpoints for the region.
   
3. Make sure the IAM profile that will be used by the remote **evworker** allows it
   to read messages from the queue.
   
In the situation where event messages are placed directly on an SQS queue by
another AWS service (e.g. S3), you can name the queue whatever you like and tell
**evworker** what queue to monitor but its just as well to stick to the same
convention based on the plugin name.

### 5. Set up messenger channels

Event handler plugins have access to a `send_msg()` method that can, funnily enough,
send a message to someone or something.

[More information about messenger plugins](messengers/README.md)

Some plugins may use this mechanism and some setup may be required to enable
specific channels (including IAM configuration).

If messenger channels required by a plugin are not available, it will log an error
when trying to use the channel and soldier on.

### 6. Upload the evlambda bundle to AWS

It can be painful to build the zip file of source files, custom modules etc.
that need to be uploaded to Lambda for the function to operate.

This process is automated by `etc/pkg-evlambda.sh`. It builds the zip file and,
optionally, uploads it to Lambda. The second step relies on having the AWS
CLI correctly configured with permissions to upload and modify Lambda functions.

To create and upload the zip bundle for worker `plugin-xxx` run the following command:

```
etc/pkg-evlambda.sh -u plugin-xxx
```

If this worked you should see two small JSON blobs printed. The contents don't
matter.

### 7. Install the evworker

If you wish to install **evworker** somewhere, rather than running it from the
source directory, run the following command:

```
#!sh
etc/evw-install.sh
```

You will be prompted for install location(s) - basically _bin_ and _lib_
directories.  You can safely use the same directory for both although the
defaults are `/usr/local/bin` and `/usr/local/lib`.

The _bin_ directory should be in your _PATH_ to avoid the need to specify the full
path when running it.

The installation process will create and populate a private virtual environment
and cause it to be activated when required. This ensures your normal Python
environment is not polluted.

To uninstall, simply remove the **evworker** executable from the _bin_ directory
and delete the **evworker** or **evworker.d** directory from the _lib_
directory.

### 8. Create the event trigger


Configure an event trigger source in AWS. You can generally do this from the AWS
Lambda console for the relevant Lambda function.

For initial testing, an S3 object creation event with one of the _demo_ plugins
is a simple option. You will need to create an S3 bucket if you don't already
have one for testing purposes.

### 9. Run the evworker

At this point, the Lambda function should be setup, the SQS handoff queue should
be in place and your Lambda function should be ready to receive events from your
selected event source.

Assuming the event handling plugin selected performs event handoff, its time to
run the event worker.

For testing purposes, you can run the **evworker** directly from the source
directory as follows:

```
#!sh
source venv/bin/activate  # If virtualenv not already active
./evworker.py --level debug worker-name
```

To run it from an installed location that's in your _PATH_, use:

```
#!sh
evworker --level debug worker-name
```

In the scenario where the worker is receiving handoff events from a Lambda
function, the _worker-name_ argument is the same as the corresponding Lambda function
name.

You should see some debug messages indicated worker threads starting and the
main thread polling the SQS handoff queue. It will then sit there, polling the
queue occasionally and waiting for event messages to arrive.

## Other Bits and Pieces

There are few support scripts available in the `etc` directory.

[More information](etc/README.md)

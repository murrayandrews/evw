# Common Components and Utilities

This package contains common support modules used by both **evworker** event workers and
**evlambda** Lambda event handlers.

The `event` module contains the `EventProto` abstract base class used
by **evw** event worker plugins.

As well as the standard Python modules and any plugin specific custom modules, the **evw**
framework also makes a number of other utility functions available as part of the `common`
package. Some of the more 
useful ones are listed below. Check the source for the others.

|Module|Function|Description|
|-|-|-|
|aws_utils|kms_decrypt|Decrypt a ciphertext blob that was encrypted using KMS. This is useful for sensitive data such as passwords that need to be made available to plugins. The IAM role used by the Lambda function must have usage rights for the KMS key(s) associated with any encrypted data.|
|aws_utils|aws_account_id| This is done with a trick by retrieving the VPC default security group and extracting the owner ID. Appropriate IAM permissions are required for this to work.|
|aws_utils|s3_load_yaml|Load a YAML file from S3 and return the decoded object.|
|aws_utils|s3_load_json|Load a JSON file from S3 and return the decoded object.|
|aws_utils|s3_search_yaml|Load a YAML file from S3, recursing up the prefix hierarchy, if necessary, to find the file.|
|utils|size_to_bytes|Convert a string specifying a data size to a number of bytes. e.g. '1K' and '1KB will convert to 1000 and 1KiB will convert to 1024).|
|utils|timedelta_to_hms|Convert a timedelta to hours, minutes, seconds (rounded to the nearest second). The results may not be quite what you expect if the timedelta is negative.|
|utils|dict_check|Check that the given dictionary has the required keys and only has keys in the required + optional sets provided. This is very useful, for example, for validating the contents of JSON or YAML configuration files read from S3.|
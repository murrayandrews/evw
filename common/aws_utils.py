"""
AWS related utilities.

Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
import json
from base64 import b64decode

import boto3
import yaml
from .utils import sepjoin

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
def kms_decrypt(ciphertext, b64=True, aws_session=None):
    """
    Decrypt a ciphertext blob that was encrypted using KMS. The following
    command generates the ciphertext:

        aws kms encrypt --key-id alias/mykey --plaintext fileb://plaintext \
            --output text --query CiphertextBlob

    :param ciphertext:  The ciphertext blob which must have been encrypted with
                        AWS KMS. Any whitespace in the ciphertext will be removed
                        as KMS encrypted text never has whitespace.
    :param b64:         If True, the ciphertext blob is base64 encoded.
                        Default True.
    :param aws_session: A boto3 Session.

    :type ciphertext:   str
    :type b64:          bool

    :return:            The plaintext.
    :rtype:             str
    """

    if not aws_session:
        aws_session = boto3.Session()

    # Squeeze out any whitespace and form a single ciphertext.
    ciphertext = ''.join(ciphertext.split())

    kms_client = aws_session.client('kms')

    if b64:
        ciphertext = b64decode(ciphertext)

    return kms_client.decrypt(CiphertextBlob=ciphertext)['Plaintext'].decode('utf-8')


# ------------------------------------------------------------------------------
def aws_account_id(aws_session=None):
    """
    Get the AWS account ID. This is done with a trick by retrieving the VPC
    default security group and extracting the owner ID. The result is cached
    internally.

    :param aws_session: A boto3 Session. If None a default is creaated.

    :return:            The AWS account ID
    :rtype:             str
    """

    try:
        return aws_account_id.account_id
    except AttributeError:
        pass

    if not aws_session:
        aws_session = boto3.Session()

    sg = aws_session.client('ec2').describe_security_groups(GroupNames=['default'])['SecurityGroups']
    aws_account_id.account_id = sg[0]['OwnerId']
    return aws_account_id.account_id


# ------------------------------------------------------------------------------
def s3_load_yaml(bucket_name, key, aws_session=None):
    """
    Load a YAML file from S3.

    :param bucket_name: Name of the S3 bucket.
    :param key:         Object key (file name) for the YAML file
    :param aws_session: A boto3 Session. If None a default is creaated.
    :type bucket_name:  str
    :type key:          str
    :return:            The object decoded from the YAML file.
    :rtype:             T

    :raise IOError:     If file doesn't exist or can't be retrieved.
    :raise ValueError:  If the file was retrieved but is not valid YAML.
    """

    if not aws_session:
        aws_session = boto3.Session()

    try:
        response = aws_session.resource('s3').Object(bucket_name, key).get()
    except Exception as e:
        raise IOError(str(e))

    try:
        return yaml.safe_load(response['Body'])
    except Exception as e:
        raise ValueError(str(e))


# ------------------------------------------------------------------------------
def s3_search_yaml(bucket_name, prefix, filename, aws_session=None):
    """
    Load a YAML file from S3, recursing up the prefix hierarchy if necessary to
    find the file.

    i.e. If the prefix is specified as a/b/c and the filename is z.yaml, then
    the first of the following to exist will be loaded:
        - a/b/c/z.yaml
        - a/b/z.yaml
        - a/z.yaml
        - z.yaml

    :param bucket_name: Name of the S3 bucket.
    :param prefix:      The search path. Do NOT end with /
    :param filename:    The YAML file basename.
    :param aws_session: A boto3 Session. If None a default is creaated.
    :type bucket_name:  str
    :type prefix:       str
    :type filename:     str
    :return:            A tuple (loaded object key, loaded object).
    :rtype:             (str, T)

    :raise IOError:     If file doesn't exist or can't be retrieved.
    :raise ValueError:  If the file was retrieved but is not valid YAML.
    """

    pfx_list = prefix.split('/')

    # Recurse up the tree
    for n in range(len(pfx_list), -1, -1):
        key = sepjoin('/', pfx_list[:n], filename)
        try:
            return key, s3_load_yaml(bucket_name, key, aws_session)
        except IOError:
            pass

    raise IOError('Could not find {} in {}/{}'.format(filename, bucket_name, prefix))


# ------------------------------------------------------------------------------
def s3_load_json(bucket_name, key, aws_session=None):
    """
    Load a JSON file from S3.

    :param bucket_name: Name of the S3 bucket.
    :param key:         Object key (file name) for the JSON file
    :param aws_session: A boto3 Session. If None a default is creaated.
    :type bucket_name:  str
    :type key:          str
    :return:            The object decoded from the JSON file.
    :rtype:             T

    :raise IOError:     If file doesn't exist or can't be retrieved.
    :raise ValueError:  If the file was retrieved but is not valid JSON.
    """

    if not aws_session:
        aws_session = boto3.Session()

    try:
        response = aws_session.resource('s3').Object(bucket_name, key).get()
    except Exception as e:
        raise IOError(str(e))

    try:
        return json.load(response['Body'])
    except Exception as e:
        raise ValueError('Bad JSON: {}'.format(str(e)))


# ------------------------------------------------------------------------------
def s3_split(s):
    """
    Split an S3 object name into bucket and prefix components.

    :param s:       The object name. Typically bucket/prefix but the following
                    are also accepted:
                        s3:bucket/prefix
                        s3://bucket/prefix
                        /bucket/prefix

    :type s:        str
    :return:        A tuple (bucket, prefix)
    :rtype:         tuple(str, str)
    """

    # Clean off any s3:// type prefix
    for p in 's3://', 's3:':
        if s.startswith(p):
            s = s[len(p):]
            break

    t = s.strip('/').split('/', 1)

    if not t[0]:
        raise ValueError('Invalid S3 object name: {}'.format(s))
    return t[0], t[1].strip('/') if len(t) > 1 else ''


# ------------------------------------------------------------------------------
def redshift_table_exists(cursor, schema, table):
    """
    Check if a Redshift table exists (as far as the user owning the connection
    can tell).

    :param cursor:      Database cursor
    :param schema:      Schema name
    :param table:       Table name

    :type schema:       str
    :type table:        str

    :return:            True if table exists, False otherwise
    :rtype:             bool
    """

    cursor.execute(
        """
        SELECT EXISTS (
            SELECT 1 FROM information_schema.tables
            WHERE  table_schema = '{}' AND table_name = '{}'
        );
        """.format(schema, table)
    )
    return cursor.fetchone()[0]


# ------------------------------------------------------------------------------
def sns_get_topic_arn_by_name(topic, aws_session=None):
    """
    Find a topic ARN with the given name in the current account.
    
    :param topic:       The topic name
    :param aws_session: A boto3 Session. If None a default is creaated.
    
    :return:            The topic ARN or None if not found.
    :rtype:             str
    
    """

    if not aws_session:
        aws_session = boto3.Session()

    sns = aws_session.client('sns')

    for page in sns.get_paginator('list_topics').paginate():
        for t in page['Topics']:  # type: dict
            if t['TopicArn'].endswith(':' + topic):
                return t['TopicArn']

    return None


# ------------------------------------------------------------------------------
def get_ssm_param(name, decrypt=True, aws_session=None):
    """
    This function reads a secure parameter from AWS SSM service.

    Warning: Does not handle list of string parameters sensibly.

    :param name:        Valid SSM parameter name
    :param decrypt:     If True, decrypt parameter values. Default True.
    :param aws_session: A boto3 Session. If None a default is creaated.

    :type name:         str
    :type decrypt:      bool

    :return:            The parameter value.
    :rtype:             str

    :raise Exception:   If the parameter doesn't exist or cannot be accessed.

    """

    if not aws_session:
        aws_session = boto3.Session()

    ssm = aws_session.client('ssm')
    response = ssm.get_parameter(Name=name, WithDecryption=decrypt)
    return response['Parameter']['Value']

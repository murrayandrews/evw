"""

Date / time related utilties.

All rights reserved (Murray Andrews 2015)

"""

import re
from datetime import datetime, time, timedelta

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
def datetime_to_iso(d=None):
    """
    Convert a datetime object to an ISO 8601 format string. Every datetime in
    GAE is UTC. Note that we truncate the last 3 digits of the microsends rather
    than rounding. Close enough.

    :param d:       A datetime object. If None use current time
    :type d:        datetime

    :return:        An ISO 8601 string e.g. "2015-03-07T06:16:04.633Z"
    :rtype:         str

    :raise ValueError: If the object is not a datetime object.
    """

    if not d:
        d = datetime.utcnow()

    if not isinstance(d, datetime):
        raise ValueError('Expected datetime not {}'.format(type(d)))

    return d.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'  # ISO date format


ISO8601_FORMATS = [
    '%Y-%m-%dT%H:%M:%S.%fZ',
    '%Y-%m-%dT%H:%M:%SZ',
]


# ------------------------------------------------------------------------------
def iso_to_datetime(s):
    """
    Convert an ISO 8601 formatted date to a datetime object. Can't guarantee this
    handles all variants. It is intended to handle "2015-03-07T06:16:04.633Z" or
    ""2015-03-07T06:16:04Z"

    :param s:       An ISO 8601 date string.
    :type s:        str

    :return:        A timezone naive datetime object.
    :rtype:         datetime

    :raise ValueError: If the string cannot be parsed.
    """

    for iso_format in ISO8601_FORMATS:
        try:
            d = datetime.strptime(s, iso_format)
        except ValueError:
            continue

        # Success!
        return d

    raise ValueError('Cannot parse ISO 8601 date')


# ------------------------------------------------------------------------------
def time_to_str(t):
    """
    Convert a time object to a string suitable for use in API responses.

    :param t:       A time object
    :type t:        datetime.time

    :return:        A string representation of the time
    :rtype:         str

    :raise ValueError: If the object is not a time object.
    """

    if not isinstance(t, time):
        raise ValueError('Expected time not {}'.format(type(t)))

    return t.strftime('%H:%M:%S.%f')


# ------------------------------------------------------------------------------
def timedelta_to_str(delta):
    """
    Convert a timedelta instance to a string.

    :param delta:   A timedelta object.
    :type delta:    timedelta

    :return:        A string representation of the timedelta.
    :rtype:         str

    """

    if not isinstance(delta, timedelta):
        raise ValueError('Expected timedelta not {}'.format(type(delta)))

    return str(delta.total_seconds())


# ------------------------------------------------------------------------------
def timedelta_to_hms(td):
    """
    Convert a timedelta to hours, minutes, seconds (rounded to nearest second).
    Results may not be quite what you expect if td is negative.

    :param td:      A timedelta
    :type td:       datetime.timedelta

    :return:        A triple (hours, minutes, seconds)
    :rtype:         tuple(int)
    """

    totalsecs = int(round(td.total_seconds(), 0))
    hours = totalsecs // 3600
    minutes = (totalsecs - 3600 * hours) // 60
    seconds = totalsecs - 3600 * hours - 60 * minutes

    return hours, minutes, seconds


# ------------------------------------------------------------------------------
def roundup_time(dt=None, round_to=60):
    """
    Round the given time up to the next multiple of round_to seconds.

    :param dt:          A datetime. Default current UTC time.
    :param round_to:    Number of seconds. Default 60.
    :type dt:           datetime
    :type round_to:     int

    :return:            Rounded datetime.
    :rtype:             datetime
    """

    if not dt:
        dt = datetime.utcnow()

    seconds = (dt - dt.min).seconds + (dt - dt.min).microseconds / 1e06
    rounding = (seconds + round_to - 1e-06) // round_to * round_to
    return dt + timedelta(0, rounding - seconds)


# ------------------------------------------------------------------------------
TIME_UNITS = {
    'w': 60 * 60 * 24 * 7,
    'd': 60 * 60 * 24,
    'h': 60 * 60,
    'm': 60,
    's': 1,
    '': 1  # Default is seconds
}

DURATION_REGEX = r'\s*((?P<value>[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?)\s*(?P<units>[{units}]?))\s*$'.format(
    units=''.join(TIME_UNITS.keys())
)


def duration_to_seconds(duration):
    """
    Convert a string specifying a time duration to a number of seconds.

    :param duration:    String in the form nnnX where nnn is an integer or float
                        and X is one of (case sensitive):
                        'w':    weeks
                        'd':    days
                        'h':    hours
                        'm':    minutes
                        's':    seconds.

                        If X is missing then seconds are assumed. Whitespace is ignored.
                        Can also be a float or integer. Note a leading + or - will be
                        handled correctly as will exponentials.

    :type duration:     str | int | float
    :return:            The duration in seconds.
    :rtype:             float

    :raise ValueError:  If the duration is malformed.
    """

    if isinstance(duration, (int, float)):
        return float(duration)

    if not isinstance(duration, str):
        raise ValueError('Invalid duration type: {}'.format(type(duration)))

    m = re.match(DURATION_REGEX, duration)

    if not m:
        raise ValueError('Invalid duration: ' + duration)

    return float(m.group('value')) * TIME_UNITS[m.group('units')]

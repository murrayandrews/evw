"""
This is the "event worker plugin" superclass EventProto(). Actual plugins
subclass this and must implement the process() method and may optionally
implement the handoff() method.

Concrete subclasses must be agnostic about whether they are running in AWS
Lambda (via the evlambda wrapper) or in a remote machine (via the evworker
daemon).

When running in Lambda, the evlambda wrapper calls the handoff() method first.
If it returns True, then the event is placed in an SQS queue (using the sqs()
method) rather than being processes within Lambda. A remote worker (the evworker
daemon) will then get the event from the queue and call the process() method to
process the event. If handoff() returns False when call by evlambda, the
process() method is called within Lambda by evlambda.

The SQS queue name is derived from the Lambda function name.

The process() method must be threadsafe as evworker us multi-threaded. Take care
with boto3 sessions in this regard. (Each thread is given its own Session()
object to avoid problems.)

................................................................................
Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
................................................................................

"""

import json
import logging
import uuid

import boto3
from .utils import import_by_name
from six import string_types
# noinspection PyUnresolvedReferences
from six.moves.urllib.parse import urlparse, parse_qsl

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
class EventRetryException(Exception):
    """
    If the process() method of an event worker plugin throws this exception, it
    is assumed that the a temporary failure has occurred and processing can be
    retried at some later time. Any other type of exception is assumed to be a
    permanent failure and there will be no retry.
    """

    pass


# ------------------------------------------------------------------------------
class EventProto(object):
    """
    Represents an AWS event object (e.g. as received by Lambda) with the ability
    to process it locally (wherever "local" happens to be) or put it in an SQS
    queue for a remote event worker to deal with.
    """

    # --------------------------------------------------------------------------
    def __init__(self, event, event_id=None, aws_session=None, logger=None):
        """
        This must be threadsafe.

        :param event:       An event object.
        :param event_id:    An event identifier. For a lambda invocation this
                            would typically be the aws_request_id from the
                            context. If None, a random UUID is generated.
        :param aws_session: A boto3 Session object. If not specified a default
                            session will be used. This is not wise in a multi-
                            threading context as boto Session objects are not
                            threadsafe.
        :param logger:      A logger. If not specified the root logger will be
                            used. Note that using the root logger will capture
                            boto events as well (which may or may not be
                            desirable). Default None.

        :type event:        dict
        :type event_id:     str
        """

        self.event = event
        self.event_id = event_id if event_id else 'xxx/{}'.format(uuid.uuid4())
        self.aws_session = aws_session if aws_session else boto3.Session()
        self.logger = logger if logger else logging.getLogger()

    # --------------------------------------------------------------------------
    # noinspection PyMethodMayBeStatic
    def handoff(self):
        """
        Determine if an event should be handed off to an event worker instead of
        being handled locally in lambda. The base class implementation always
        returns False.

        This method is only ever run in a lambda context so it must not cause
        any side effects depended on by process() method.

        :return:    True if the event should be handed off or False otherwise.
        :rtype:     bool
        """

        return False

    # --------------------------------------------------------------------------
    def process(self):
        """
        Process the event.

        """

        raise NotImplementedError('Event.process()')

    # --------------------------------------------------------------------------
    # noinspection PyMethodMayBeStatic
    def finish(self):
        """
        Called after processing to allow any final cleanup.

        """

        pass

    # --------------------------------------------------------------------------
    def sqs_handoff(self, queue_name):
        """
        Put the event on the specified SQS queue. This is done by serialising
        the event data in JSON. It is expected that an event worker will pull it
        off the queue. The event ID is added as message attribute EventId.

        :param queue_name:  SQS queue name.
        :type queue_name:   str

        :return:            The SQS message ID
        :rtype:             str
        """

        sqs_queue = self.aws_session.resource('sqs').get_queue_by_name(QueueName=queue_name)
        result = sqs_queue.send_message(
            MessageBody=json.dumps(self.event),
            MessageAttributes={
                'EventId': {
                    'DataType': 'String',
                    'StringValue': self.event_id
                }
            }
        )
        return result['MessageId']

    # --------------------------------------------------------------------------
    def send_msg(self, to, subject=None, message=None):
        """
        Send a message to someone or something. The actual processsing is
        handled by a messenger plugin. This is a helper function available to
        Event() plugins. Up to them whether they use it or not.

        :param to:          Destination(s). A string or list of strings
                            specifying destinations. Each one must be in the
                            form of a pseudo URL... dest-type://dest-address. The
                            dest-type (URL scheme) is used to load an
                            appropriate plugin. The dest-address can,
                            optionally, be followed with a URL query string
                            providing plugin specific parameters.  e.g.
                                ?x=10&p=Hello%20World

                            e.g.    sms://61499123456
                                    ses://fred@wherever.com
                                    s3:bucket/object?kmskey=k1
        :param subject:     Message subject. Not all destination types may use
                            this. Some destination types will truncate it.
                            Default None.
        :param message:     Message body. Not all destination types may use this.
                            Default None. At least one of subject/message must
                            be provided.

        :type to:           str | list[str]
        :type subject:      str
        :type message:      str

        :raise ValueError:  If subject and message are None or empty.
        :raise Exception:   If sending fails. If there are multiple To addresses
                            then the first failure cancels all.

        """

        if not to:
            return

        if not subject and not message:
            raise ValueError('At least one of subject or message required')

        if isinstance(to, string_types):
            to = [to]

        for dest in to:
            # Look for a URL style query string with custom params
            try:
                dest_url = urlparse(dest)
            except Exception as e:
                raise Exception('Invalid destination {}: {}'.format(dest, e))

            if not dest_url.scheme:
                raise Exception('Invalid destination {}'.format(dest))

            try:
                messenger = import_by_name(dest_url.scheme.lower(), 'messengers')
            except ImportError:
                raise Exception('Unsupported destination type: {}'.format(dest_url.scheme))

            try:
                custom_args = dict(parse_qsl(dest_url.query))
                # Rejoin netloc and path. Either may be ''
                dest_address = '/'.join(filter(None, [dest_url.netloc, dest_url.path.lstrip('/')]))
                # noinspection PyUnresolvedReferences
                messenger.send(dest_address, subject, message, self.aws_session, **custom_args)
            except Exception as e:
                raise Exception('Send to {} failed: {}'.format(dest, e))
            else:
                self.logger.debug('send_msg: sent to %s: %s', dest, subject)

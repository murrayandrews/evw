"""
General utilities.

Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import importlib
import json
import logging
import os
import re
import stat
import sys
from base64 import b64decode
from datetime import datetime, time, timedelta
from gzip import GzipFile
from .date import datetime_to_iso, time_to_str, timedelta_to_str

PY3 = sys.version_info[0] == 3

if PY3:
    # noinspection PyCompatibility, PyUnresolvedReferences,PyProtectedMember
    from urllib.request import urlopen, HTTPError
    # noinspection PyCompatibility, PyUnresolvedReferences
    from urllib.error import URLError
    # noinspection PyCompatibility, PyUnresolvedReferences
    from io import StringIO
else:
    # noinspection PyCompatibility, PyUnresolvedReferences
    from urllib2 import urlopen, HTTPError, URLError
    # noinspection PyCompatibility, PyUnresolvedReferences
    from StringIO import StringIO

__author__ = 'Murray Andrews'

STRING_TYPES = (type('x'), type(u'x'))


# ..............................................................................
# region string utils
# ..............................................................................


# ------------------------------------------------------------------------------
def sepjoin(sep, *args):
    """
    Join all the non-empty args with the specified separator,  expanding any list args.
    ie. if sep is / and with args of: 'a', [], ['b', 'c']' will return a/b/c

    :param sep:         A separator string used as a joiner.
    :param args:        A string or list of strings.
    :type sep:          str
    :type args:         str | list[str]

    :return:            Joined up args separated by /
    :rtype:             str
    """

    result = []
    for s in filter(None, args):
        result.extend(s if isinstance(s, list) else [s])

    return sep.join(result)


# ------------------------------------------------------------------------------
def splitext2(path, pathsep=os.sep, extsep=os.extsep):
    """
    This is a variation on os.path.splitext() except that a suffix is defined as
    everything from the first dot in the basename onwards, unlike splitext()
    which uses the last dot in the basename..

    Also splitext() always uses os.sep and os.extsep whereas splitext2 allows
    these to be overridden.

    :param path:        The path.
    :param pathsep:     Path separator. Defaults to os.sep.
    :param extsep:      Suffix separator. Defaults to os.extsep.

    :type path:         str
    :type pathsep:      str
    :type extsep:       str
    :return:            A tuple (root, ext) such that root + ext == path

    :rtype:             tuple(str)
    """

    try:
        dirpart, basepart = path.rsplit(pathsep, 1)
    except ValueError:
        dirpart, basepart = '', path

    preprefix = ''
    while basepart.startswith(extsep):
        preprefix += extsep
        basepart = basepart[1:]

    try:
        prefix, suffix = basepart.split(extsep, 1)
        suffix = '.' + suffix

    except ValueError:
        prefix, suffix = basepart, ''

    return sepjoin(pathsep, dirpart, preprefix+prefix), suffix


# ------------------------------------------------------------------------------
def split2(s, sep):
    """
    Convenience function to split a string into 2 pieces based on the first
    occurance of a separator. The first piece will always exist but the second
    (i.e. the bit after the separator) will only exist if the separator is
    present.

    :param s:               The string to split.
    :param sep:             The single character separator

    :type s                 str
    :type sep:              str

    :return:                A tuple of (pre-sep segment, post-sep segment).
                            If the separator is not present then the first
                            element will be the same as the supplied string and
                            the second element will be None.
    :rtype:                 tuple(str, str)
    """

    t = s.split(sep, 1)

    return (t[0], t[1]) if len(t) > 1 else (s, None)


# ------------------------------------------------------------------------------
def shorten_string(s, length, more):
    """
    Shorten the string so that it fits in the specified length. If the string
    must be cropped to fit then the string is cropped and the 'more' string is
    appended, making sure to fit inside the length.  If the 'more' string itself
    will not fit in the length then the string is just cropped at the specified
    length.

    :param s:           The string to shorten
    :param length:      The maximum length of the final string.
    :param more:        The string indicating something was chopped off s
                        e.g. '...' or ' (more)'
    :type s:            str
    :type length:       int
    :type more:         str

    """

    assert length > 0, 'Bad length in shorten_string'

    ls = len(s)
    if ls <= length:
        return s

    lm = len(more)
    if lm >= length:
        # Even the more string won't fit
        return s[0:length]

    return s[:length - lm] + more


# ------------------------------------------------------------------------------
def alphanum(s):
    """
    Remove all non-alphanumerics from a string.

    :param s:       The string.
    :type s:        str

    :return:        The string with all non-alphanumerics removed.
    :rtype:         str
    """

    return re.sub(r'[^\da-zA-Z]', '', s)


# ------------------------------------------------------------------------------
def splitstrip(s, sep, maxsplit=-1, chars=None):
    """
    Split a string and strip each of the components.
    
    :param s:           The string.
    :param sep:         Same as the arg to str.split().
    :param maxsplit:    Same as the arg to str.split().
    :param chars:       Same as the arg to str.strip().
    :return:            A list of strings.
    :rtype:             list[str]
    """

    return [t.strip(chars) for t in s.split(sep, maxsplit)]


# ------------------------------------------------------------------------------
def dashcap(s):
    """
    Capitalise the first letter in each component of a string separated with
    dashes.

    i.e. X-hello-WORLD becomes X-Hello-World

    :param s:       The string
    :type s:        str
    :return:        The initcapped string.
    :rtype:         str
    """

    return '-'.join([x.capitalize() for x in s.split('-')])

# ..............................................................................
# endregion string utils
# ..............................................................................


# ..............................................................................
# region logging utils
# ..............................................................................


# -------------------------------------------------------------------------------
def syslog_address():
    """
    Try to work out the syslog address.

    :return:    A value suitable for use as the address arg for SysLogHandler.
    :rtype:     tuple | str
    """

    for f in ('/dev/log', '/var/run/syslog'):
        try:
            mode = os.stat(f).st_mode
        except OSError:
            continue

        if stat.S_ISSOCK(mode):
            return f

    return 'localhost', 514


# -------------------------------------------------------------------------------
def get_log_level(s):
    """
    Convert the string version of a log level defined in the logging module to the
    corresponding log level. Raises ValueError if a bad string is provided.

    :param s:       A string version of a log level (e.g. 'error', 'info').
                    Case is not significant.
    :type s:        str

    :return:        The numeric logLevel equivalent.
    :rtype:         int

    :raises:        ValueError if the supplied string cannot be converted.
    """

    if not s or not isinstance(s, str):
        raise ValueError('Bad log level:' + str(s))

    t = s.upper()

    if not hasattr(logging, t):
        raise ValueError('Bad log level: ' + s)

    return getattr(logging, t)


# ..............................................................................
# endregion logging utils
# ..............................................................................


# ------------------------------------------------------------------------------
SIZE_UNITS = {
    'B': 1,
    'K': 1000,
    'KB': 1000,
    'M': 1000 ** 2,
    'MB': 1000 ** 2,
    'G': 1000 ** 3,
    'GB': 1000 ** 3,
    'T': 1000 ** 4,
    'TB': 1000 ** 4,
    'P': 1000 ** 5,
    'PB': 1000 ** 5,
    'KiB': 1024,
    'MiB': 1024 ** 2,
    'GiB': 1024 ** 3,
    'TiB': 1024 ** 4,
    'PiB': 1024 ** 5
}

SIZE_REGEX = r'\s*((?P<value>[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?)\s*(?P<units>{units}))\s*$'.format(
    units='|'.join(SIZE_UNITS.keys())
)


def size_to_bytes(size):
    """
    Convert a string specifying a data size to a number of bytes.

    :param size:        String in the form nnnX where nnn is an integer or float
                        and X is one of (case sensitive):
                        'B'         Bytes
                        'K', 'KB':  Kilobytes (1000)
                        'M', 'MB':  Megabytes
                        'G', 'GB':  Gigabytes
                        'T', 'TB':  Terabytes
                        'P', 'PB':  Petabytes.
                        'KiB':      Kibibytes (1024)
                        'MiB':      Mebibytes
                        'GiB':      Gibibytes
                        'TiB':      Tebibytes
                        'PiB':      Pebibytes

                        Whitespace is ignored.  Note a leading + or - will be
                        handled correctly as will exponentials. If no multiplier
                        suffix is provided, bytes are assumed.

    :type size:         str | int
    :return:            The size in bytes.
    :rtype:             int

    :raise ValueError:  If the input is malformed.
    """

    try:
        return int(size)
    except ValueError:
        pass

    if not isinstance(size, STRING_TYPES):
        raise ValueError('Invalid size type: {}'.format(type(size)))

    m = re.match(SIZE_REGEX, size)

    if not m:
        raise ValueError('Invalid size: ' + size)

    return int(round(float(m.group('value')) * SIZE_UNITS[m.group('units')], 0))


# ------------------------------------------------------------------------------
TIME_UNITS = {
    'w': 60 * 60 * 24 * 7,
    'd': 60 * 60 * 24,
    'h': 60 * 60,
    'm': 60,
    's': 1,
    '': 1  # Default is seconds
}

DURATION_REGEX = r'\s*((?P<value>[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?)\s*(?P<units>[{units}]?))\s*$'.format(
    units=''.join(TIME_UNITS.keys())
)


# ------------------------------------------------------------------------------
def dict_check(d, required=None, optional=None):
    """
    Check that the given dictionary has the required keys and only has keys in
    the required + optional sets.

    :param d:           The dict to check.
    :param required:    An iterable of mandatory keys. Can be None indicating
                        required keys should not be checked.
                        no required keys.
    :param optional:    An iterable of optional keys. Can be None indicating
                        optional keys should not be checked.

    :type d:            dict
    :type required:     list | tuple | set
    :type optional:     list | tuple | set
    :raise ValueError:  If the dict doesn't contain all required keys or does
                        contain disallowed keys.
    """

    if required is not None and not isinstance(required, set):
        required = set(required)
    if optional is not None and not isinstance(optional, set):
        optional = set(optional)

    actual_keys = set(d)
    if required is not None and not required <= actual_keys:
        raise ValueError('Missing keys: {}'.format(', '.join(sorted(required - actual_keys))))

    if optional is not None:
        bad_keys = actual_keys - (required if required is not None else set()) - optional
        if bad_keys:
            raise ValueError('Unexpected keys: {}'.format(', '.join(sorted(bad_keys))))


# ------------------------------------------------------------------------------
def decode_data(data):
    """
    Decode a base64, gzipped JSON string into a Python object.

    :param data:    The data to decode.
    :type data:     str
    :return:        A Python object.

    :raise Exception:   If there is a decoding problem.

    """

    return json.loads(GzipFile(fileobj=StringIO(b64decode(data))).read())


# -------------------------------------------------------------------------------
def import_by_name(name, parent=None):
    """
    Import a named module from within the named parent.

    :param name:            The name of the sender module.
    :param parent:          Name of parent module. Default None.

    :type name:             str
    :type parent:           str

    :return:                The sender module.
    :rtype:                 module

    :raise ImportError:     If the import fails.
    """

    if parent:
        name = parent + '.' + name

    return importlib.import_module(name)


# ------------------------------------------------------------------------------
def json_default(obj):
    """
    This is a helper function for JSON serialisation with json.dumps() to allow
    (UTC) datetime and time objects to be serialised. It should be used thus ...
        json_string = json.dumps(object_of_some_kind, default=json_default)
    It is primarily used in API responses.

    :param obj:             An object.
    :return:                A serialisable version. For datetime objects we just
                            convert them to a string that strptime() could handle.

    :raise TypeError:       If obj cannot be serialised.
    """

    if isinstance(obj, datetime):
        return datetime_to_iso(obj)

    if isinstance(obj, time):
        return time_to_str(obj)

    if isinstance(obj, timedelta):
        return timedelta_to_str(obj)

    try:
        return str(obj)
    except Exception:
        raise TypeError('Cannot serialize {}'.format(type(obj)))

# Support Tools

The `etc` directory contains a number of support tools.

Those intended for direct use are list below. Those marker with * must be
run from the **evw** source directory, not `etc`.

|Name|Description|
|-|-|
|[evw-install.sh](evw-install.md)*|Install evworker.|
|[kmspasswd.sh](kmspasswd.md)|Encrypt a password with AWS KMS.|
|[kmspasswd.py](kmspasswd.md)|Encrypt a password with AWS KMS. Functionally identical to `kmspasswd.sh` but in Python. Should run on Windows (untested). Requires boto3.|
|[pkg-evlambda.sh](pkg-evlambda.md)*|Create an evlambda package for uploading to AWS Lambda.|
|[pkg-evworker.sh](pkg-evworker.md)*|Create a deployment bundle for **evworker**.|
|[s3event.py](s3event.md)|Generate an artificial S3 event notification and, optionally, sends it to an SQS queue.|


The following components are not intended for direct use. They are used by one
of the tools described above.

|Name|Description|
|-|-|
|install-common.sh|Used by `install.sh`.|
|pyrunner.sh|Used by `install.sh`.|

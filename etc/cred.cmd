@echo off
REM - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
REM Make cluster credential files suitable for use with rsDropLoader2.
REM 
REM Crude but works.
REM - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
REM 
REM Copyright (c) 2016, Murray Andrews
REM All rights reserved.
REM 
REM Redistribution and use in source and binary forms, with or without modification,
REM are permitted provided that the following conditions are met:
REM 
REM 1.  Redistributions of source code must retain the above copyright notice, this
REM     list of conditions and the following disclaimer.
REM 
REM 2.  Redistributions in binary form must reproduce the above copyright notice,
REM     this list of conditions and the following disclaimer in the documentation
REM     and/or other materials provided with the distribution.
REM 
REM 3.  Neither the name of the copyright holder nor the names of its contributors
REM     may be used to endorse or promote products derived from this software
REM     without specific prior written permission.
REM 
REM THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
REM ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
REM WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
REM DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
REM ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
REM (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
REM LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
REM ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
REM (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
REM SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
REM - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

setlocal ENABLEEXTENSIONS
set PATH=%PATH%;C:\Program Files\Amazon\AWSCLI\
set AWSENCRYPT=aws kms encrypt --output text --query CiphertextBlob
set interactive=1
echo %CMDCMDLINE% | find /i "%~0" >NUL
if %ERRORLEVEL% == 0 set interactive=0

REM - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
REM User input

echo.
set /p key_id="KMS Key Id: "
echo.
set /p cluster="Cluster alias: "
echo.
set /p user="Cluster user name: "
echo.

echo Make sure no-one is looking over your shoulder
echo.
pause

cls
set /p secret="Password: "
cls
set /p verify="Verify: "
cls

if not "%secret%" == "%verify%" (
	echo No match
	exit /B 1
)

REM - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
REM KMS encrypt the password

for /f %%i in ('%AWSENCRYPT% --key-id alias/%key_id% --plaintext "%secret%"') do set epass=%%i


REM - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
REM Create the cluster credentials file.

if %ERRORLEVEL% == 0 (
	set cred_file="%cluster%.cred"
	echo # Credentials file for cluster {cluster} > %cred_file%
	echo # Created; %DATE% %TIME% >> %cred_file%
	echo >> %cred_file%
	echo user: %user% >> %cred_file%
	echo password: %epass% >> %cred_file%
	echo Created %cred_file%
)


REM - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
REM Pause

if %interactive% == 0 pause
exit /B 0

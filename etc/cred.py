#!/usr/bin/env python3

"""

Make credential files suitable for use with rsDropLoader2.

................................................................................

Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function

import argparse
import sys
from base64 import b64encode
from datetime import datetime
from getpass import getpass
from os.path import basename

import boto3

__author__ = 'Murray Andrews'

PROG = basename(sys.argv[0])
LINELEN = 60  # 72

CRED_CLUSTER_FILE = '{cluster}.cred'
CRED_CLUSTER_TEMPLATE = '''# Credentials file for cluster {cluster}
# Created: {when}

# ex: ft=yaml

user: {user}
password:
  {password}
'''

CRED_S3_FILE = 's3.cred'
CRED_S3_TEMPLATE = '''# Credentials file for S3
# Created: {when}

# ex: ft=yaml

access-key-id: {access_key}
access-secret-key:
  {secret_key}

'''


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Prepare credentials files for rsDropLoader'
    )

    argp.add_argument('-p', '--profile', action='store', help='As for AWS CLI.')

    argp.add_argument('-k', '---key', metavar='kms-key-name',
                      help='KMS key name (don\'t include alias/ prefix). If not'
                           ' specified and stdin is connected to a tty then the user'
                           ' will be prompted. Command line option is required otherwise.')

    argp.add_argument('--s3', action='store_true',
                      help='Make an s3 credential file instead of a cluster'
                           ' credential file')

    return argp.parse_args()


# ------------------------------------------------------------------------------
def get_input(prompt, allow_empty=True, ignore_whitespace=True):
    """
    Prmopt the user for input.

    :param prompt:          Prompt to print (to stderr)
    :param allow_empty:     If True allow empty response. Default True.
    :param ignore_whitespace: If True strip leading and trailing whitespace in
                            the response. Default True.
    :type prompt:           str
    :type allow_empty:      bool
    :type ignore_whitespace: bool
    :return:                The response from the user.
    :rtype:                 str
    """

    while True:
        print(prompt, file=sys.stderr, end='')
        try:
            # noinspection PyCompatibility,PyUnresolvedReferences
            response = raw_input()  # type: str
        except NameError:
            # Python 3
            response = input()  # type: str
        if ignore_whitespace:
            response = response.strip()
        if response or allow_empty:
            return response


# ------------------------------------------------------------------------------
def kms_encrypt(plaintext, key_alias, b64=True, aws_session=None):
    """
    Encrypt a string with AWS KMS.

    :param plaintext:       String to encrypt.
    :param key_alias:       A KMS key alias (with or without alias/ prefix).
    :param b64:             If True, encode the ciphertext in base64.
                            Default True
    :param aws_session:     A boto3 session.

    :type plaintext:        str
    :type key_alias:        str
    :type b64:              bool
    :return:                The ciphertext.
    :rtype:                 bytes
    """

    if not aws_session:
        aws_session = boto3.Session()

    if not key_alias.startswith('alias/'):
        key_alias = 'alias/' + key_alias

    kms_client = aws_session.client('kms')

    ciphertext = kms_client.encrypt(
        KeyId=key_alias,
        Plaintext=plaintext
    )['CiphertextBlob']

    return b64encode(ciphertext) if b64 else ciphertext


# ------------------------------------------------------------------------------
def chunks(s, n):
    """
    Break a string into fixed length chunks (except possibly for final chunk
    which may be shorter).

    :param s:       The string to chunk.
    :param n:       Chunk lemgth. Must be >0
    :return:        A list of chunks
    :rtype:         list[str]
    """

    return [s[i:i + n] for i in range(0, len(s), n)]


# ------------------------------------------------------------------------------
def cred_s3(kms_key, aws_session=None):
    """
    Create the S3 credentials file s3.cred.

    :param kms_key:     KMS key alias.
    :param aws_session: A boto3 Session.

    :type kms_key:      str
    """

    access_key = get_input('Access key: ', allow_empty=False)
    access_secret_key = None
    while not access_secret_key:
        access_secret_key = getpass('Access secret key: ').strip()

    ciphertext = kms_encrypt(access_secret_key, kms_key, aws_session=aws_session)
    with open(CRED_S3_FILE, 'w') as fp:
        print(
            CRED_S3_TEMPLATE.format(
                when=datetime.now().isoformat(),
                access_key=access_key,
                secret_key='\n  '.join(chunks(ciphertext, LINELEN))
            ),
            file=fp
        )

    print('Created: {}'.format(CRED_S3_FILE))


# ------------------------------------------------------------------------------
def cred_cluster(kms_key, aws_session=None):
    """
    Create the cluster credentials file. For cluster xxx it is named xxx.cred.

    :param kms_key:     KMSkey alias.
    :param aws_session: A boto3 Session.

    :type kms_key:      str

    """

    cluster = get_input('Cluster alias: ', allow_empty=False)
    user = get_input('Cluster user name: ', allow_empty=False)
    secret = getpass('Password: ')
    verify = getpass('Verify: ')

    if secret != verify:
        raise ValueError('Mismatch')

    cred_file = CRED_CLUSTER_FILE.format(cluster=cluster)
    with open(cred_file, 'w') as fp:
        print(
            CRED_CLUSTER_TEMPLATE.format(
                when=datetime.now().isoformat(),
                cluster=cluster,
                user=user,
                password='\n  '.join(
                    chunks(
                        kms_encrypt(secret, kms_key, aws_session=aws_session).decode('utf-8'),
                        LINELEN)
                )
            ),
            file=fp
        )

    print('Created {}'.format(cred_file))


# ------------------------------------------------------------------------------
def main():
    """
    Do the business.

    :return:    0
    :raise Exception: If anything goes wrong.
    """

    args = process_cli_args()

    kms_key = args.key if args.key else get_input('Key name: ', allow_empty=False)
    aws_session = boto3.Session(profile_name=args.profile)

    if args.s3:
        cred_s3(kms_key, aws_session)
    else:
        cred_cluster(kms_key, aws_session)

    return 0


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    exit(main())  # Uncomment for debugging
    try:
        exit(main())
    except Exception as ex:
        print('{}: {}'.format(PROG, ex), file=sys.stderr)
        exit(1)
    except KeyboardInterrupt:
        print('Interrupt', file=sys.stderr)
        exit(2)

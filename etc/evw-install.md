# install.sh(1) -- Install evworker

## SYNOPSIS

`etc/install.sh` \[install_dir\]

## DESCRIPTION

While **evworker** can be run directly from the **evw** source directory,
this is not always convenient, particularly in daemon mode.

This script installs **evworker** somewhere else. If the _install-dir_ option
is not specified, the user will be prompted for install location(s) - basically _bin_
and _lib_ directories.  It is safe to use the same directory for both although
the defaults are `/usr/local/bin` and `/usr/local/lib`.

The _bin_ directory should be in the user's _PATH_ to avoid the need to specify the
full path when running it.

The installation process will create and populate a private virtual environment
and cause it to be activated when required. This ensures the normal Python
environment is not polluted.

To uninstall, simply remove the **evworker** executable from the _bin_ directory
and delete the **evworker** or **evworker.d** directory from the _lib_
directory.

## OPTIONS

*   _install-dir_:
    The base installation directory. If not specified, the user will be prompted.
    The default is `/usr/local`, resulting in components being placed in
    `/usr/local/bin`, `/usr/local/lib` and `/usr/local/share/man/man1`.

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).

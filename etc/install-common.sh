#!/bin/bash
# This is common boilerplate for the simple Python program installer.

# If stdin is connected to a terminal the user is prompted to confirm install
# locations, otherwise defaults are used.

# Following ## lines go in the main installer. Uncomment them there, obviously.

##	TARGET=xxx
##	MANFILES=xxx.1  # Comment out if no man page.
##	PYMODULES="mymodule1 mymodule2"  # The application's local modules
##	python=python3  # Prefer python or python3.
##	force_python_version=yes  # Set to non-empty value to prevent choice of version
##	. install-common.sh
##	# Custom install commands ...

# This script does the following:
# 1.	Installs a generic Python script runner in the bin dir (shell script)
#	This points to the actual Python script in the lib dir
# 2.	Creates a lib dir for the script and installs the main Python script
# 3.	If a xxx-requirements.txt or requirements.txt is available, a virtualenv
#	is created in the lib dir and pip is used to install the requirements.
# 4.	Installs a manual entry for the new install in the man dir
# 5.	If a licen[cs]e.txt exists, that is installed in the lib dir.

# If any other files need to be installed, the calling installer program has to
# do that for itself. It should use "saydo ..." for that.

# -------------------------------------------------------------------------------
# Copyright (c) 2016, Murray Andrews
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.
# 
# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
# 
# 3.  Neither the name of the copyright holder nor the names of its contributors
#     may be used to endorse or promote products derived from this software without
#     specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -------------------------------------------------------------------------------

# ##############################################################################
# region BOILERPLATE

umask 022

PROG=`basename $0`
BASE=/usr/local

echo="echo -e"
install="install -Sv"

ANSI_GREY='\033[2m'
ANSI_RESET='\033[0m'
ANSI_BLUE='\033[34m'
ANSI_RED='\033[31m'
ANSI_YELLOW='\033[33m'
ANSI_MAGENTA='\033[35m'

# ------------------------------------------------------------------------------
# Print an error message
function error() {
	$echo "${ANSI_RESET}${ANSI_RED}$*${ANSI_RESET}"
}

# ------------------------------------------------------------------------------
# Confirm a value with the user if stdin is a tty otherwise use defaults.
# Usage: checkval prompt default
function checkval() {
	if [ ! -t 0 ]
	then
		echo "$2"
		return
	fi
	$echo "${ANSI_BLUE}$1 ${ANSI_RESET}${ANSI_GREY}($2)${ANSI_RESET}" \\c >/dev/tty
	read val
	if [ "$val" == "" ]
	then
		$echo $2
	else
		$echo "$val"
	fi
}

# ------------------------------------------------------------------------------
# Print a message and run a command. Exits on fail.
# Usage: saydo message command [args...]
function saydo() {
	$echo "${ANSI_BLUE}$1${ANSI_RESET}" ... \\c 
	shift
	result=`$* 2>&1`
	if [ $? -eq 0 ]
	then
		$echo ${ANSI_BLUE}Done${ANSI_RESET}
		[ "$result" != "" ] && $echo "${ANSI_GREY}$result${ANSI_RESET}"
	else
		error Failed
		[ "$result" != "" ] && $echo "${ANSI_YELLOW}$result${ANSI_RESET}"
		error Abort && exit 2
	fi
}

# ------------------------------------------------------------------------------
# Convert relative to absolute path. Usage: abspath path
function abspath() {
	$python -c "from __future__ import print_function; import os.path;print(os.path.abspath('$1'))"
}

# ------------------------------------------------------------------------------
# Check that required programs are avalable
# Usage: require prog ...
function require() {
	missing=0
	for p
	do
		[[ `which $p` == "" ]] && error "$p is required but cannot be found" && missing=1
	done
	[ $missing -ne 0 ] && exit 2
}
	
# ------------------------------------------------------------------------------
# Arguments check.

[ "$TARGET" == "" ] && error "Don't run this directly. It is used by other installers!" && exit 4

[ $# -gt 1 ] && $echo Usage: $PROG [basedir] && exit 1
case "$1"
in
	-*)	$echo Usage: $PROG [basedir]; exit 0;;
	"")	;;
	*)	BASE="$1";;
esac

# ------------------------------------------------------------------------------
# Warn if root.
if  [ `id -u` -eq 0 ]
then
	$echo "${ANSI_MAGENTA}WARNING: You are root. Proceed? (Type yes to to continue)${ANSI_RESET}" \\c
	read x
	[ "$x" != "yes" ] && exit
fi

# ------------------------------------------------------------------------------
# Confirm python version
[ "$python" == "" ] && python=python3
[ "$force_python_version" == "" ] && python=`checkval "python3 or python" $python`

# ------------------------------------------------------------------------------
# Check program prerequisites
require $python virtualenv

# ------------------------------------------------------------------------------
# Validate and create primary directories
BINDIR=`checkval BINDIR $BASE/bin`
LIBDIR=`checkval LIBDIR $BASE/lib`
MANDIR=`checkval MANDIR $BASE/share/man/man1`
BINTARGET=$BINDIR/$TARGET
LIBTARGET=$LIBDIR/$TARGET
RUNNER=etc/pyrunner.sh

# Prevent name clash between main script and library directory
[ `abspath $BINTARGET` == `abspath $LIBTARGET` ] && LIBTARGET=${LIBTARGET}.d

saydo "Creating primary directories" mkdir -p  $BINDIR $LIBTARGET $MANDIR

# ------------------------------------------------------------------------------
# Prepare the main runner - generic shell script that links to the python TARGET
TMPFILE=/tmp/$PROG.$$
ABSLIBDIR=`abspath $LIBDIR`
trap "/bin/rm -f $TMPFILE; exit 3" 0
sed "s|^LIBDIR=.*|LIBDIR=${ABSLIBDIR}|" $RUNNER > $TMPFILE

# Install the main runner and the main Python script
saydo "Installing main runner" $install -m755 $TMPFILE $BINTARGET
/bin/rm -f $TMPFILE
trap '' 0
saydo "Installing ${TARGET}.py" $install -m755 ${TARGET}.py $LIBTARGET

# Install the man files
[ "$MANFILES" != "" ] && saydo "Installing man page(s)" $install -m644 $MANFILES $MANDIR

# Install the local modules
if [ "$PYMODULES" != "" ]
then
	saydo "Installing local Python modules" cp -r $PYMODULES $LIBTARGET
	saydo "Cleaning pycaches from $LIBTARGET" find $LIBTARGET -depth -name '__pycache__' -exec /bin/rm -r '{}' \;
	saydo "Cleaning *.pyc files from $LIBTARGET" find $LIBTARGET -name '*.pyc' -exec /bin/rm -f '{}' \;
	saydo "Fixing directory permissions" find $LIBTARGET -type d -exec chmod go+rx '{}' \;
fi

# Install licence file
[ -f licence.txt ] && saydo "Installing licence.txt" $install -m644 licence.txt $LIBTARGET
[ -f license.txt ] && saydo "Installing license.txt" $install -m644 license.txt $LIBTARGET

# ------------------------------------------------------------------------------
# Presence of a requirements.txt file will force creation of a virtualenv.
# Can be either target-requirements.txt or just requirements.txt.
for f in ${TARGET}-requirements.txt requirements.txt
do
	[ -f $f ] && requirements_txt=$f && break
done
if [ "$requirements_txt" != "" ]
then
	saydo "Installing $requirements_txt" $install -m644 $requirements_txt $LIBTARGET/requirements.txt
	saydo "Creating virtualenv in $LIBTARGET" virtualenv --python=$python $LIBTARGET/venv
	. $LIBTARGET/venv/bin/activate
	saydo "Configuring virtualenv" pip install -r $LIBTARGET/requirements.txt
fi

# endregion BOILERPLATE
# ##############################################################################

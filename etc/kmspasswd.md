# kmspasswd(1) -- Encrypt a password with AWS KMS

Encrypt a password or other secret data with AWS KMS. The output format is suitable
for use in **evw** configuration files.

## SYNOPSIS

`etc/kmspasswd.sh` \[options\]

`etc/kmspasswd.py` \[options\]

## DESCRIPTION
,
**Evw** event handler plugins may sometimes need access to secret configuration
data (e.g. access keys, passwords etc). It is not safe for these to be in
cleartext.

**kmspasswd** provides a simple means to encrypt these strings.

Whatever is doing the decryption needs usage rights on whatever KMS key was
used for the encryption.

The event handler plugin can then use the `common.aws_utils.kmsdecrypt()`
function to decrypt the data and use it as required.

The shell version uses the AWS CLI and requires Linux or OSX.
The Python version uses boto3. It should run on Windows (untested). Otherwise,
they are functionally identical.

## OPTIONS

*   `-h`:
    Print help and exit.
    
*   `-k` _kms-key-name_

    KMS key name. Don't include the `alias/` prefix. If not specifed, and stdin
    is connected to a terminal, the user is prompted for the key name. Command
    line option is required otherwise.

*   `-p` _profile_
    As for the AWS CLI **--profile** option.


## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).

#!/usr/bin/env python3

"""

KMS encrypt a password or other secret data

................................................................................

Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function

import argparse
import sys
from base64 import b64encode, b64decode
from getpass import getpass
from os.path import basename

import boto3

PROG = basename(sys.argv[0])

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Remote event worker for AWS.'
    )

    argp.add_argument('-p', '--profile', action='store', help='As for AWS CLI.')

    argp.add_argument('-k', '---key', metavar='kms-key-name',
                      help='KMS key name (don\'t include alias/ prefix). If not'
                           ' specified and stdin is connected to a tty then the user'
                           ' will be prompted. Command line option is required otherwise.')

    return argp.parse_args()


# ------------------------------------------------------------------------------
def get_input(prompt, allow_empty=True, ignore_whitespace=True):
    """
    Prmopt the user for input.

    :param prompt:          Prompt to print (to stderr)
    :param allow_empty:     If True allow empty response. Default True.
    :param ignore_whitespace: If True strip leading and trailing whitespace in
                            the response. Default True.
    :type prompt:           str
    :type allow_empty:      bool
    :type ignore_whitespace: bool
    :return:                The response from the user.
    :rtype:                 str
    """

    while True:
        print(prompt, file=sys.stderr, end='')
        try:
            # noinspection PyCompatibility, PyUnresolvedReferences
            response = raw_input()  # type: str
        except NameError:
            # Python 3
            response = input()  # type: str
        if ignore_whitespace:
            response = response.strip()
        if response or allow_empty:
            return response


# ------------------------------------------------------------------------------
def kms_encrypt(plaintext, key_alias, b64=True, aws_session=None):
    """
    Encrypt a string with AWS KMS.

    :param plaintext:       String to encrypt.
    :param key_alias:       A KMS key alias (with or without alias/ prefix).
    :param b64:             If True, encode the ciphertext in base64.
                            Default True
    :param aws_session:     A boto3 session.

    :type plaintext:        str
    :type key_alias:        str
    :type b64:              bool
    :return:                The ciphertext.
    :rtype:                 bytes
    """

    if not aws_session:
        aws_session = boto3.Session()

    if not key_alias.startswith('alias/'):
        key_alias = 'alias/' + key_alias

    kms_client = aws_session.client('kms')

    ciphertext = kms_client.encrypt(
        KeyId=key_alias,
        Plaintext=plaintext
    )['CiphertextBlob']

    return b64encode(ciphertext) if b64 else ciphertext


# ------------------------------------------------------------------------------
def kms_decrypt(ciphertext, b64=True, aws_session=None):
    """
    Decrypt a ciphertext blob that was encrypted using KMS. The following
    command generates the ciphertext:

        aws kms encrypt --key-id alias/mykey --plaintext fileb://plaintext \
            --output text --query CiphertextBlob

    :param ciphertext:  The ciphertext blob which must have been encrypted with
                        AWS KMS. Any whitespace in the ciphertext will be removed
                        as KMS encrypted text never has whitespace.
    :param b64:         If True, the ciphertext blob is base64 encoded.
                        Default True.
    :param aws_session: A boto3 Session.

    :type ciphertext:   str
    :type b64:          bool

    :return:            The plaintext.
    :rtype:             str
    """

    if not aws_session:
        aws_session = boto3.Session()

    # Squeeze out any whitespace and form a single ciphertext.
    ciphertext = ''.join(ciphertext.split())

    kms_client = aws_session.client('kms')

    if b64:
        ciphertext = b64decode(ciphertext)

    return kms_client.decrypt(CiphertextBlob=ciphertext)['Plaintext'].decode('utf-8')


# ------------------------------------------------------------------------------
def main():
    """
    Do the business.

    :return:    0
    :raise Exception: If anything goes wrong.
    """

    args = process_cli_args()

    kms_key = args.key if args.key else get_input('Key name: ', allow_empty=False)

    secret = getpass('Password: ')
    verify = getpass('Verify: ')

    if secret != verify:
        raise ValueError('Mismatch')

    aws_session = boto3.Session(profile_name=args.profile)
    ciphertext = kms_encrypt(secret, kms_key, aws_session=aws_session).decode('utf-8')
    print(ciphertext)

    # Check decryption
    verify = kms_decrypt(ciphertext, aws_session=aws_session)
    if secret != verify:
        raise Exception('Decryption check failed!')
    return 0


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    # exit(main())  # Uncomment for debugging
    try:
        exit(main())
    except Exception as ex:
        print('{}: {}'.format(PROG, ex), file=sys.stderr)
        exit(1)
    except KeyboardInterrupt:
        print('Interrupt', file=sys.stderr)
        exit(2)

#!/bin/bash

# KMS encrypt a password or other secret data

# -------------------------------------------------------------------------------
# Copyright (c) 2016, Murray Andrews
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.
# 
# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
# 
# 3.  Neither the name of the copyright holder nor the names of its contributors
#     may be used to endorse or promote products derived from this software without
#     specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -------------------------------------------------------------------------------

PROG=`basename $0`
key=
aws="aws"

# -------------------------------------------------------------------------------
function usage {
	echo "Usage: $PROG [-h] [-k kms-key-name] [-p profile]" >&2
}

function help {
	usage
	cat >&2 <<-!

	Encrypt a password or secret string using AWS KMS. The output format is suitable
	for use in evworker/evlambda configuration files.

	Options:
	  -h			Print help and exit
	  -k kms-key-name	KMS key name (don't include alias/ prefix). If not
	  			specified and stdin is connected to a tty then the user
	  			will be prompted. Command line option is required otherwise.
	  -p profile		As for AWS CLI --profile option.

	!
}
# -------------------------------------------------------------------------------

args=`getopt hk:p: $*`
[ $? != 0 ] && usage >&2 && exit 1
for i
do
	case "$i"
	in
		-h)	help; exit;;
		-k)	key="$2"; shift 2;;
		-p)	aws="$aws --profile $2"; shift 2;;
		--)	shift; break
	esac
done

[ $# -ne 0 ] && usage && exit 1

# -------------------------------------------------------------------------------

if [ -t 0 ]
then
	# Prompt for key name if not provided on the command line
	if [ "$key" = "" ]
	then
		echo -n "Key name: " >/dev/tty
		read key
		[ "$key" = "" ] && echo "Key name must be specified" >&2 && exit 2
	fi
	# Get the password/secret data from the user
	status=2
	trap 'stty echo; exit $status' 0
	stty -echo

	echo -n "Password: " >/dev/tty
	read p1
	echo >/dev/tty

	[ "$p1" = "" ] && echo "Say what?" >/dev/tty && exit

	echo -n "Verify: " >/dev/tty
	read p2
	echo >/dev/tty
	stty echo
	trap '' 0
	[ "$p1" != "$p2" ] && echo Mismatch >/dev/tty && exit 2
else
	# Read from stdin
	[ "$key" = "" ] && echo Key name must be specified with -k argument >&2 && exit 1

	read p1
fi

# Encrypt. Take care not to have the secret as a command line arg anywhere.
(tr -d '\n' | $aws kms encrypt --key-id alias/$key --plaintext fileb:///dev/stdin --output text --query CiphertextBlob) <<<$p1

# pkg-evlambda.sh(1) -- Create an evlambda package for AWS Lambda

## SYNOPSIS

`etc/pkg-evlambda.sh` \[options\] function-name \[extra-components...\]

## DESCRIPTION

For anything other than simple, single source file functions, Lambda requires
a zip file containing all of the source components, including any non-standard
modules to be uploaded.

It can be painful to build the zip file, particularly the process of including
any non-standard Python modules, which requires them to be located in the
development virtual environment and included in the zip file at the root level.
Lambda is also very particular about file permissions of the zipped components.

This process is automated by **pkg-evlambda.sh**. It builds the zip file and,
optionally, uploads it to Lambda. The second step relies on having the AWS
CLI correctly configured with permissions to upload and modify Lambda functions.

If the **-u** option is used, the package created will be uploaded to Lambda and
the correct entry point for the Lambda event handler set. If upload is
successful, two small JSON blobs will be printed. The contents don't matter.

## OPTIONS

*   **-h**:
    Print help and exit.
    
*   **-p** profile:
    As for the AWS CLI **--profile** option.

*   **-r** region:
    As for the AWS CLI **--region** option.

*   **-u**:
    Upload the package to Lambda.
    If not specified, the package is created but not uploaded.
    
*   _function-name_:
    Name of the Lambda function. This option is required. There must be an event
    handler name matching the function name up to the first `-`. i.e. If
    function name is `xxx-123`, there must be an event handler named `xxx`. The
    created package is `xxx-123.zip`.

*   _extra-components_:
    If specified, these are added to the zip file. A better approach is to
    specify any additional files required in the plugin manifest.
    
## EVENT HANDLER PLUGIN MANIFESTS

Some event handler plugins make use of non-standard Python modules. These must
be included in the zip package uploaded to Lambda. The _plugin manifest_ is a
JSON formatted file that identifies any non-standard Python modules or other
files that need to be included in the zip package.

For a plugin named `myPlugin`, the manifest must be named `handlers/myPlugin.mf`.

If no additional modules or files are required, the manifest is not required.

**pkg-evlambda.sh** reads the plugin manifest when it creates the zip package
and includes the specified components.

The manifest can contain the following keys:

*   **modules**:
    A list of module names. These modules must be present in the virtual
    environment named `venv` in the **evw** source directory. It is not
    necessary to include either `boto3` or `yaml` if the default versions are
    acceptable, as these are either provided by Lambda or included automatically
    by `etc/pkg-lambda.sh`.

*   **files**:
    A list of files or directories that should be included. File names are
    relative to the **evw** source directory.

Other keys are ignored. It's a good idea to add some descriptive keys, such as
`author`, `description`, `version` etc.

As an example, this is the manifest for the **rsDropLoader** plugin:


    {
        "description": "Manifest for rsDropLoader event handler plugin",
        "author": "Murray Andrews",
        "files": ["licence.txt"],
        "modules": ["pg8000"]
    }

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).

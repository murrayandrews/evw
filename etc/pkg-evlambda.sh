#!/bin/sh

# Package the code bundle for a Lambda event worker. Python 3.6 runtime.

# -------------------------------------------------------------------------------
# Copyright (c) 2016, Murray Andrews
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.
# 
# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
# 
# 3.  Neither the name of the copyright holder nor the names of its contributors
#     may be used to endorse or promote products derived from this software without
#     specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -------------------------------------------------------------------------------

PROG=`basename $0`

AWS="aws"
EVWRAPPER=evlambda
LOCAL_MODULES="common messengers"  # Local dirs that must be zipped in.
EVW_MODULES="six yaml requests"  # Non-standard modules required by evlambda

LAMBDA_HANDLER=${EVWRAPPER}.lambda_handler
TMPDIR=/tmp/${EVWRAPPER}.$$
PKGTMP=${TMPDIR}/__pkg__.zip
VENV=venv  # Virtual environment


# -------------------------------------------------------------------------------
function usage {
	echo "Usage: $PROG [-h] [-p profile] [-r region] [-u] function-name [extra-components...]" >&2
}

function help {
	usage
	cat >&2 <<-!

	Create an evlambda package for uploading to AWS Lambda. Python 3.6+ Lambda
	runtime is assumed.

	Options:
	  -h			Print help and exit
	  -p profile		As for AWS CLI --profile option.
	  -r region		As for AWS CLI --region option
	  -u			Upload the package to Lambda. If not specified the package
	  			is created as (function-name.zip) but is not uploaded.
	  function-name		Name of the Lambda function. There must be an event
	  			handler name matching the function name up to the
	  			first -. i.e. If function name is xxx-123, there must be
	  			an event handler named xxx.
	  extra-components	If specified, these are added to the zip file.
	  			Typically additional local Python modules if required.


	!
}

function info {
	if [ -t 2 ]
	then
		echo "[34m$*[0m" >&2
	else
		echo $PROG: INFO: $* >&2
	fi
}

function error {
	if [ -t 2 ]
	then
		echo "[31m$*[0m" >&2
	else
		echo $PROG: ERROR: $* >&2
	fi
}

# -------------------------------------------------------------------------------

args=`getopt hp:r:u $*`
[ $? != 0 ] && usage >&2 && exit 1
for i
do
	case "$i"
	in
		-h)	help; exit;;
		-p)	AWS="$AWS --profile $2"; shift 2;;
		-r)	AWS="$AWS --region $2"; shift 2;;
		-u)	upload=yes; shift;;
		--)	shift; break
	esac
done

[ $# -eq 0 ] && usage && exit 1
FUNCTION="$1"; shift
PACKAGE=evl-${FUNCTION}.zip
HANDLER=`expr "$FUNCTION" : '\(^[^-][^-]*\)'`
MANIFEST=handlers/${HANDLER}.mf
[ "$HANDLER" == "" ] && error "Bad function name: $FUNCTION" && exit 1

[ ! -f handlers/${HANDLER}.py ] && error "No event handler for $HANDLER" && exit 1


# -------------------------------------------------------------------------------
# Prepare to create the code bundle. Copy everything to temp dir so we can play
# with file permissions.

status=1
trap '/bin/rm -rf $TMPDIR; exit $status' 0
mkdir $TMPDIR

# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
# copy the wrapper and add in the standard local modules.
info Adding evw components
cp ${EVWRAPPER}.py $TMPDIR
cp -r $LOCAL_MODULES $TMPDIR

# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
# Add the event handler
mkdir -p $TMPDIR/handlers
cp handlers/${HANDLER}.py handlers/__init__.py $TMPDIR/handlers

# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
# Copy any extra components specified on command line
[ $# -ne 0 ] && info "Adding extras" && cp -r $* $TMPDIR

# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
# Install any non-standard modules needed
info Adding non-standard modules
python3 -m pip install --target $TMPDIR --upgrade --quiet -r requirements.txt
[ $? -ne 0 ] && status=1 && exit

# -------------------------------------------------------------------------------
# Read the manifest (if it exists) for any custom modules or other files to
# include. Note Lambda provides boto3 automatically. No need to include it.
# The manifest is a JSON file containing optional "modules" and "files" keys
if [ -f "$MANIFEST" ]
then
	info Adding handler specific components from $MANIFEST

	modules=$(
		python3 -c 'import sys, json;print(" ".join(json.load(sys.stdin).get("modules",[])))' < $MANIFEST
	)

	[ "$modules" != "" ] && python3 -m pip install --target $TMPDIR --upgrade --quiet $modules

	files=$(
		python3 -c 'import sys, json;print(" ".join(json.load(sys.stdin).get("files",[])))' < $MANIFEST
	)
	if [ "$files" != "" ]
	then
		# cpio makes sure we preserve hierarchies.
		# The --insecure seems to be required for FreeBSD and derivatives,
		# but is unsupported for GNU/Linux.
		cpio_opts='-pdum --quiet'
		cpio --version | grep -q 'GNU cpio' || cpio_opts="$cpio_opts --insecure"
		find $files -type f | cpio $cpio_opts $TMPDIR
	fi
fi

# -------------------------------------------------------------------------------
# Temp dir should have everything needed. Fix permissions.
find $TMPDIR -type d -exec chmod go=rx '{}' \;
find $TMPDIR -type f -exec chmod go=r '{}' \;


# ----------------------------------------
# Create the main zip file for Lambda upload
info Creating code bundle
(
	cd $TMPDIR
	zip -9 --quiet -r -o $PKGTMP * --exclude \
			'*.pyc' \
			'*__pycache__/*' \
			'*.swp' \
			'*.zip' \
			'*.tar.*' \
			'*-info/*'
)

# ----------------------------------------
if [ -f $PKGTMP ]
then
	mv $PKGTMP $PACKAGE
	echo Created $PACKAGE
else
	error Failed to create $PACKAGE
	exit
fi

# ----------------------------------------
# Upload the zip file to Lambda
if [ "$upload" != "" ]
then
	info Uploading to AWS Lambda
	$AWS lambda update-function-code --function-name $FUNCTION --zip-file fileb://$PACKAGE
	$AWS lambda update-function-configuration --function-name $FUNCTION --handler $LAMBDA_HANDLER
fi

status=0

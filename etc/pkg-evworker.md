# pkg-evworker.sh(1) -- Create a deployment bundle for evworker

## SYNOPSIS

`etc/pkg-evworker.sh`

## DESCRIPTION

Create a bzipped tar file containing all of the base components required to
run **evworker**.

Note that in order to run any particular plugin, additional non-standard modules
may need to be installed. The best way to do this is to add them to the
`requirements.txt` file and then use `etc/evw-install.md`. This will create a
private virtual environment for **evworker** and populate it with the required
non-standard modules. Alternatively, create a Python _virtualenv_ named `venv`
in the same directory in which the tar file is extracted, activate the virtual
environment and use **pip** to manually install the required modules (including
those in `requirements.txt`).

## FILES

The output file is `evworker.tar.bz2`.

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).

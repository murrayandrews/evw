#!/bin/sh

# Package the code bundle for a evworker event worker. This is the analog of
# pkg-evlambda. It creates a complete code bundle containing all the necessary
# components to run the plugin specified on the command line.

# -------------------------------------------------------------------------------
# Copyright (c) 2016, Murray Andrews
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.
# 
# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
# 
# 3.  Neither the name of the copyright holder nor the names of its contributors
#     may be used to endorse or promote products derived from this software without
#     specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -------------------------------------------------------------------------------

PROG=$(basename $0)

EVWRAPPER=evworker
LOCAL_MODULES="common evw messengers"  # Local dirs that must be zipped in.

TMPDIR=tmp_${EVWRAPPER}.$$
PKGTMP=__pkg__.tar.bz2


# -------------------------------------------------------------------------------
function usage {
	echo "Usage: $PROG [-h] worker-name [extra-components...]" >&2
}

function help {
	usage
	cat >&2 <<-!

	Create an evworker package for deploying to a Linux (like) run environment

	Options:
	  -h			Print help and exit
	  worker-name		Name of the worker. There must be an event
	  			handler name matching the function name up to the
	  			first -. i.e. If function name is xxx-123, there must be
	  			an event handler named xxx.
	  extra-components	If specified, these are added to the zip file.
	  			Typically additional local Python modules if required.


	!
}

function info {
	if [ -t 2 ]
	then
		echo "[34m$*[0m" >&2
	else
		echo $PROG: INFO: $* >&2
	fi
}

function error {
	if [ -t 2 ]
	then
		echo "[31m$*[0m" >&2
	else
		echo $PROG: ERROR: $* >&2
	fi
}


# -------------------------------------------------------------------------------

args=`getopt hp:u $*`
[ $? != 0 ] && usage >&2 && exit 1
for i
do
	case "$i"
	in
		-h)	help; exit;;
		--)	shift; break
	esac
done

[ $# -eq 0 ] && usage && exit 1
FUNCTION="$1"; shift
PACKAGE=evw-${FUNCTION}.tar.bz2
HANDLER=`expr "$FUNCTION" : '\(^[^-][^-]*\)'`
MANIFEST=handlers/${HANDLER}.mf
[ "$HANDLER" == "" ] && error "Bad function name: $FUNCTION" && exit 1

[ ! -f handlers/${HANDLER}.py ] && error "No event handler for $HANDLER" && exit 1


# -------------------------------------------------------------------------------
# Prepare to create the code bundle. Copy everything to temp dir so we can play
# with file permissions.

status=1
trap '/bin/rm -rf $TMPDIR; exit $status' 0
mkdir $TMPDIR

# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
# copy the wrapper and add in the standard local modules.
info Adding evw components
cp ${EVWRAPPER}.py $TMPDIR
cp -r $LOCAL_MODULES $TMPDIR

# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
# Add the event handler
mkdir -p $TMPDIR/handlers
cp handlers/${HANDLER}.py handlers/__init__.py $TMPDIR/handlers

# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
# Copy any extra components specified on command line
[ $# -ne 0 ] && info "Adding extras" && cp -r $* $TMPDIR

# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
# Install any non-standard modules needed
info Adding non-standard modules
python3 -m pip install --target $TMPDIR --upgrade --quiet -r requirements.txt
[ $? -ne 0 ] && status=1 && exit


# -------------------------------------------------------------------------------
# Read the manifest (if it exists) for any custom modules or other files to
# include. Note Lambda provides boto3 automatically. No need to include it.
# The manifest is a JSON file containing optional "modules" and "files" keys
if [ -f "$MANIFEST" ]
then
	info Adding handler specific components from $MANIFEST

	modules=$(
		python3 -c 'import sys, json;print(" ".join(json.load(sys.stdin).get("modules",[])))' < $MANIFEST
	)

	if [ "$modules" != "" ]
	then
		python3 -m pip install --target $TMPDIR --upgrade --quiet $modules || exit
	fi

	files=$(
		python3 -c 'import sys, json;print(" ".join(json.load(sys.stdin).get("files",[])))' < $MANIFEST
	)
	if [ "$files" != "" ]
	then
		# cpio makes sure we preserve hierarchies.
		# cpio makes sure we preserve hierarchies.
		# The --insecure seems to be required for FreeBSD and derivatives,
		# but is unsupported for GNU/Linux.
		cpio_opts='-pdum --quiet'
		cpio --version | grep -q 'GNU cpio' || cpio_opts="$cpio_opts --insecure"
		find $files -type f | cpio $cpio_opts $TMPDIR
	fi
fi

# -------------------------------------------------------------------------------
# Temp dir should have everything needed. Fix permissions.
find $TMPDIR -type d -exec chmod go=rx '{}' \;
find $TMPDIR -type f -exec chmod go=r '{}' \;


# ----------------------------------------
# Create the code bundle
info Creating code bundle
(
	cd $TMPDIR
	tar -cyf $PKGTMP \
		--exclude '*.pyc' \
		--exclude '__pycache__' \
		--exclude '*.swp' \
		--exclude '*.zip' \
		--exclude '*.tar.*' \
		--exclude '*-info' \
		*
)

# ----------------------------------------
mv $TMPDIR/$PKGTMP $PACKAGE
info Created $PACKAGE
status=0

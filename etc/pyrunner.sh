#!/bin/sh

# General runner for Python programs. If the basename of this script is xxx,
# then look for a runnable Python script in one of the following (with and
# without the .py extension):
# 	LIBDIR/xxx.d/xxx.py
#	LIBDIR/xxx/xxx.py
#	LIBDIR/venv*/bin/xxx.py
#	LIBDIR/xxx.py
#
# A typical usage would be to have this script reside in /usr/local/bin and
# there would be a corresponding directory in /usr/local/lib with the same name
# containing the main python script and whatever supporting modules or files it
# requires.
#
# If there is a virtual environment in the LIBDIR (named venv*),
# then activate that before running the script.
#
# This is all a bit clunky but it allows a Python based program with its own
# local modules and other module dependences to be installed as an isolated
# bundle that can be easily uninstalled.

# -------------------------------------------------------------------------------
# Copyright (c) 2016, Murray Andrews
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.
# 
# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
# 
# 3.  Neither the name of the copyright holder nor the names of its contributors
#     may be used to endorse or promote products derived from this software without
#     specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -------------------------------------------------------------------------------

PROG=`basename $0`
LIBDIR=/usr/local/lib

#-------------------------------------------------------------------------------
# Find the runnable script
for d in $LIBDIR/${PROG}.d $LIBDIR/$PROG $LIBDIR/$PROG/venv*/bin $LIBDIR
do
	for r in $d/$PROG.py $d/$PROG
	do
		if [ -f "$r" -a -x "$r" ] 
		then
			RUNNABLE="$r"
			LIBDIR="$d"
			break 2
		fi
	done
done

[ "$RUNNABLE" = "" ] && echo "$PROG: Cannot locate run module in $LIBDIR" >&2 && exit 1

#-------------------------------------------------------------------------------
# See if there is virtualenv for it - must be in same dir as main script
for v in $LIBDIR/venv*
do
	[ -d $v ] && VENV=$v && break
done

[ "$VENV" != "" ] && . $VENV/bin/activate

#-------------------------------------------------------------------------------

exec $RUNNABLE $*

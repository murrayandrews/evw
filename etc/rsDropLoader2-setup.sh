#!/bin/sh

# -------------------------------------------------------------------------------
# Copyright (c) 2016, Murray Andrews
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
# 1.  Redistributions of source code must retain the above copyright notice, this
#     list of conditions and the following disclaimer.
# 
# 2.  Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
# 
# 3.  Neither the name of the copyright holder nor the names of its contributors
#     may be used to endorse or promote products derived from this software without
#     specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# -------------------------------------------------------------------------------

AWS=aws
TABLESPACE=evw
WORKER=rsDropLoader2

READ_CAPACITY=2  # Provisioned read capacity
WRITE_CAPACITY=1  # Provisioned write capacity

# -------------------------------------------------------------------------------
# Check if dynamoDB table exists. Only works if <= 100 tables exist
function dynamodb_table_exists {
	[ "$_dytables" = "" ] && _dytables=`$AWS dynamodb list-tables --output text | cut -f2`
	for i in $_dytables
	do
		[ "$1" == "$i" ] && return 0
	done
	return 1
}

# -------------------------------------------------------------------------------
# Create a simple dynamo table
# Usage: dynamodb_table_create table primary-key
function dynamodb_table_create {
	dynamodb_table_exists $1 && echo Table $1 exists && return

	aws dynamodb create-table --table-name $1 \
		--attribute-definitions AttributeName=$2,AttributeType=S \
		--key-schema AttributeName=$2,KeyType=HASH \
		--provisioned-throughput ReadCapacityUnits=${READ_CAPACITY},WriteCapacityUnits=${WRITE_CAPACITY}
}

# -------------------------------------------------------------------------------

dynamodb_table_create ${TABLESPACE}.${WORKER}.clusters cluster

#!/usr/bin/env python3

"""

Simulate an S3 event.

See https://docs.aws.amazon.com/AmazonS3/latest/dev/notification-content-structure.html

This is a full event.

    {
        'Records': [
            {
                'eventSource': 'aws:s3',
                'eventName': 'ObjectCreated:Put',
                'eventVersion': '2.0',
                'userIdentity': {'principalId': 'AWS:AIDAJ3YYBPAYP22AQ3BNU'},
                'requestParameters': {'sourceIPAddress': '120.147.217.15'},
                'eventTime': '2016-09-24T06:02:55.886Z',
                's3': {
                    'configurationId': 'f48af588-2542-4a98-941d-eb828e291b28',
                    'object': {
                        'eTag': '6686853da3491a56c98917cc5c4ddea2',
                        'size': 5,
                        'sequencer': '0057E6170FC7ACE87B',
                        'key': 'dropbox/r.dat'
                    },
                    's3SchemaVersion': '1.0',
                    'bucket': {
                        'ownerIdentity': {'principalId': 'AVRD2C17Z6GLL'},
                        'arn': 'arn:aws:s3:::dev.canis2.com',
                        'name': 'dev.canis2.com'
                    }
                },
                'awsRegion': 'ap-southeast-2',
                'responseElements': {
                    'x-amz-request-id': '0C4C63F66A7E79E3',
                    'x-amz-id-2': 'XfW80wKuyFuv26mrre2uIh7NPxw/Zcl5P3+7Pl4E3ioMPHtOExUvmIgpoensUNbGLqsqmVrDbEw='
                }
            }
        ]
    }

This is the simulated event. Its usually close enough.

    {
        'Records': [
            {
                'eventSource': 's3event',
                'eventName': 'ObjectCreated:Put',
                'eventVersion': '2.0',
                'userIdentity': {'principalId': 'AWS:AIDAJ3YYBPAYP22AQ3BNU'},
                'requestParameters': {'sourceIPAddress': '120.147.217.15'},
                'eventTime': '2016-09-24T06:02:55.886Z',
                's3': {
                    'configurationId': 's3event-simulator',
                    'object': {
                        'eTag': '6686853da3491a56c98917cc5c4ddea2',
                        'size': 5,
                        'sequencer': '0057E6170FC7ACE87B',
                        'key': 'dropbox/r.dat'
                    },
                    's3SchemaVersion': '1.0',
                    'bucket': {
                        'ownerIdentity': {'principalId': 'AIDAJ3YYBPAYP22AQ3BNU'},
                        'arn': 'arn:aws:s3:::dev.canis2.com',
                        'name': 'dev.canis2.com'
                    }
                },
                'awsRegion': 'ap-southeast-2'
            }
        ]
    }

................................................................................

Copyright (c) 2018, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

# ..............................................................................
# region imports

import argparse
import json
import os
import sys
from datetime import datetime
from getpass import getuser
from socket import gethostname
from time import time

import boto3

try:
    # noinspection PyCompatibility, PyUnresolvedReferences
    from urllib2 import urlopen
except ImportError:
    # noinspection PyCompatibility, PyUnresolvedReferences
    from urllib.request import urlopen

# endregion imports
# ..............................................................................


# ..............................................................................
# region constants

__author__ = 'Murray Andrews'
__version__ = '1.0.0'

PROG = os.path.splitext(os.path.basename(sys.argv[0]))[0]

EVENT_TYPE = 'ObjectCreated:Put'  # Default event type
EVENT_SOURCE = PROG  # Real events use 'aws:s3'


# endregion constants
# ..............................................................................


# ------------------------------------------------------------------------------
class S3event(dict):
    """
    Construct a simulated S3 event. All the fields are populated by f_* methods.

    """

    # --------------------------------------------------------------------------
    def __init__(self, bucket, key, event_source=PROG, event_type=EVENT_TYPE, aws_session=None):
        """

        :param bucket:      S3 bucket name
        :param key:         S3 object key
        :param event_source: Event source. Default is the program name. For
                            real S3 events, this is 'aws:s3'.
        :param event_type:  Event type. Default ObjectCreated:Put
        :param aws_session: A boto3 Session object. If not specified a default
                            session is used.

        :type bucket:       str
        :type key:          str
        :type event_source: str
        :type event_type:   str
        :type aws_session:  boto3.Session
        """

        super(S3event, self).__init__()
        self.bucket_name = bucket
        self.key = key
        self.aws_session = aws_session if aws_session else boto3.Session()
        self.event_source = event_source
        self.event_type = event_type

        s3 = self.aws_session.resource('s3')

        self.bucket = s3.Bucket(self.bucket_name)
        self.object = self.bucket.Object(self.key)

        self.iam = self.aws_session.client('iam')
        # noinspection PyBroadException
        try:
            self.principal_id = self.iam.get_user()['User']['UserId']
        except Exception:
            self.principal_id = getuser() + '@' + gethostname()

        field_methods = [getattr(self, m) for m in dir(self) if m.startswith('f_')]

        # Populate all the fields.
        for m in field_methods:
            m()

    # --------------------------------------------------------------------------
    @property
    def json(self):
        """
        Encode the event as JSON.

        :return:        JSON encoded event.
        :rtype:         str
        """

        return json.dumps(self, indent=4, sort_keys=True)

    # --------------------------------------------------------------------------
    def f_event_source(self):
        """
        Set eventSource
        """

        self['eventSource'] = self.event_source

    # --------------------------------------------------------------------------
    def f_event_name(self):
        """
        Set eventName
        """

        self['eventName'] = self.event_type

    # --------------------------------------------------------------------------
    def f_event_version(self):
        """
        Set eventVersion
        """

        self['eventVersion'] = '2.0'

    # --------------------------------------------------------------------------
    def f_user_identity(self):
        """
        Set userIdentity. Kludge.
        """

        self['userIdentity'] = {'principalId': 'AWS:' + self.principal_id}

    # --------------------------------------------------------------------------
    def f_request_params(self):
        """
        Set requestParameters. Primarily this is source IP. Kludge.
        """

        self['requestParameters'] = {
            'sourceIPAddress': json.load(urlopen('https://api.ipify.org/?format=json'))['ip']
        }

    # --------------------------------------------------------------------------
    def f_event_time(self):
        """
        Set eventTime
        """

        self['eventTime'] = datetime.utcnow().isoformat()[:-3] + 'Z'

    # --------------------------------------------------------------------------
    def f_s3(self):
        """
        Set s3 key
        """

        self['s3'] = {
            'configurationId': PROG + '-simulator',
            'object': {
                'eTag': str(self.object.e_tag).strip('"'),
                'size': self.object.content_length,
                'sequencer': '{:X}'.format(int(time() * 1e06)),
                'key': self.object.key
            },
            's3SchemaVersion': '1.0',
            'bucket': {
                # This ownerIdentity is wrong -- its just an approximation
                'ownerIdentity': {'principalId': self.principal_id},
                'arn': 'arn:aws:s3:::' + self.bucket.name,
                'name': self.bucket_name,
            },
        }

        v = self.object.version_id
        if v:
            self['s3']['object']['versionId'] = v

    # --------------------------------------------------------------------------
    def f_region(self):
        """
        Set awsRegion to the region of the bucket (may not be default region).
        """

        # This is ridiculously obscure. This actually returns None if the region
        # is us-east-1. Why would anyone in their right mind do that?

        r = self.bucket.meta.client.get_bucket_location(Bucket=self.bucket.name)["LocationConstraint"]

        self['awsRegion'] = r if r else 'us-east-1'


# ------------------------------------------------------------------------------
def s3_split(s):
    """
    Split an S3 object name into bucket and prefix components.

    :param s:       The object name. Typically bucket/prefix but the following
                    are also accepted:
                        s3:bucket/prefix
                        s3://bucket/prefix
                        /bucket/prefix

    :type s:        str
    :return:        A tuple (bucket, prefix)
    :rtype:         tuple(str, str)
    """

    # Clean off any s3:// type prefix
    for p in 's3://', 's3:':
        if s.startswith(p):
            s = s[len(p):]
            break

    t = s.strip('/').split('/', 1)

    if not t[0]:
        raise ValueError('Invalid S3 object name: {}'.format(s))
    return t[0], t[1].strip('/') if len(t) > 1 else ''


# ------------------------------------------------------------------------------
def sqs_send(queue, msg, aws_session=None):
    """
    Send a message to the specified SQS queue.

    :param queue:       SQS queue name or URL (without https:// prefix).
    :param msg:         The message to send.
    :param aws_session: A boto3 Session object. If not specified a default
                        session is used.

    :type queue:        str
    :type msg:          str
    :type aws_session:  boto3.Session
    """

    if not aws_session:
        aws_session = boto3.Session()

    if '.amazonaws.com/' in queue:
        # Assume its a URL
        sqs_queue = aws_session.resource('sqs').Queue('https://' + queue)
    else:
        sqs_queue = aws_session.resource('sqs').get_queue_by_name(QueueName=queue)

    sqs_queue.send_message(MessageBody=msg)


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='S3 event simulator'
    )

    argp.add_argument('-e', '--event-type', dest='event_type', action='store',
                      default=EVENT_TYPE,
                      help='Event type. Default is {}.'.format(EVENT_TYPE))

    argp.add_argument('--profile', action='store', help='As for AWS CLI.')

    argp.add_argument('-q', '--queue', action='store',
                      help='AWS SQS queue name. If not specified the message is'
                           ' generated but not sent anywhere.')

    argp.add_argument('-s', '--event-source', dest='event_source', action='store',
                      default=EVENT_TYPE,
                      help='Event source. Default is {}.'
                           ' Real S3 events use aws:s3.'.format(EVENT_SOURCE))

    argp.add_argument('-v', '--version', action='version', version=__version__,
                      help='Print version and exit.')

    argp.add_argument('s3_object', metavar='s3-object', action='store',
                      help='S3 object name.')

    return argp.parse_args()


# ------------------------------------------------------------------------------
def main():
    """
    Do the business.

    :return:        Status
    :rtype:         int
    """

    args = process_cli_args()

    aws_session = boto3.Session(profile_name=args.profile)
    bucket, key = s3_split(args.s3_object)
    if not key:
        raise Exception('Object key required')

    ev = json.dumps(
        {
            'Records': [
                S3event(bucket, key, event_type=args.event_type, aws_session=aws_session)
            ]
        },
        indent=4,
        sort_keys=True
    )

    print(ev)

    if args.queue:
        sqs_send(args.queue, ev, aws_session=aws_session)
        print('Message sent to {}'.format(args.queue))


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    # exit(main())  # Uncomment for debugging
    try:
        exit(main())
    except Exception as ex:
        print('{}: {}'.format(PROG, ex))
        exit(1)
    except KeyboardInterrupt:
        print('{}: Interrupt')
        exit(2)

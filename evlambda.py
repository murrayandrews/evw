"""
Wrapper for event worker plugins that allows them to be run in AWS Lambda.

................................................................................
Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
................................................................................

"""

import json
import logging
import os

from common.const import SQS_QUEUE_NAME, LOGLEVEL
from common.utils import import_by_name, get_log_level
from common.event import EventRetryException

__author__ = 'Murray Andrews'

# If WORKER_NAME is None, we get the worker name from the Lambda function
# name up to the first '-' and use that to load a worker plugin.

# If EVW_WORKER is None, worker is determined from Lambda function name
EVW_WORKER = os.environ.get('EVW_WORKER')

# In Lambda, just use the root logger.
LOGNAME = None
# noinspection PyTypeChecker
LOG = logging.getLogger(LOGNAME)


# ------------------------------------------------------------------------------
def setup_logging(level, name=None):
    """
    Setup logging.

    :param level:   Logging level. The string format of a level (eg 'debug').
    :param name:    Logger name. Default None implies root logger.

    :type level:    str
    :type name:     str | None

    """

    logger = logging.getLogger(name)
    logger.setLevel(get_log_level(level))
    logger.debug('Log level set to %s (%d)', level, logger.getEffectiveLevel())


# ------------------------------------------------------------------------------
def lambda_handler(event, context):
    """
    This is the AWS lambda entry point.

    It can process the event locally or put it in an SQS queue for an event
    worker running elsewhere (e.g. in an EC2 instance).

    If the event is sent to SQS, the queue name is
        evw-function_name
    i.e. the lambda function name prefixed by evw-

    This function is generic and should not need modification. It relies on
    having access to an Event class which is a subclass of EventProto.

    :param event:       Lambda event data.
    :param context:     Lambda context. This will be populated if running in
                        lambda but None if running in an eventworker.
    :type event:        dict

    """

    setup_logging(os.environ.get('EVW_LOGLEVEL', LOGLEVEL), name=LOGNAME)
    LOG.debug("Received event: " + json.dumps(event, indent=2))

    # ----------------------------------------
    # Load the worker plugin and get the event handler class.

    # If WORKER_NAME is not specified, we get the Lambda function name
    # up to the first '-' and use that to load a worker plugin.
    worker_name = EVW_WORKER if EVW_WORKER else context.function_name.split('-', 1)[0]
    try:
        worker = import_by_name(worker_name, 'handlers')
    except ImportError as e:
        LOG.critical('Event %s: Can\'t import worker plugin %s: %s',
                     context.aws_request_id, worker_name, str(e))
        return

    try:
        event_class = getattr(worker, 'Event')
    except AttributeError:
        LOG.critical('Event %s: %s does not appear to be a worker plugin - no Event() class',
                     context.aws_request_id, worker_name)
        return

    ev = event_class(event, event_id='evl/' + context.aws_request_id)

    # We allow handoff override with an environment variable. Good for debugging.
    handoff_control = os.environ.get('EVW_HANDOFF', 'normal').lower()
    LOG.debug('Handoff control: %s', handoff_control)
    if handoff_control != 'never' and SQS_QUEUE_NAME is not None \
            and (handoff_control == 'always' or ev.handoff()):
        queue_name = SQS_QUEUE_NAME.format(worker_name=worker_name)
        msg_id = ev.sqs_handoff(queue_name)
        LOG.info('Event %s: Sent to SQS queue %s, message ID: %s',
                 context.aws_request_id, queue_name, msg_id)
        return

    # Process the event locally
    LOG.info('Event %s: Processing locally', context.aws_request_id)
    try:
        ev.process()
    except EventRetryException as e:
        LOG.warning('Event %s: Retry: %s', context.aws_request_id, str(e))
        # Allow this exception to propagate to force a retry
        raise
    except Exception as e:
        LOG.error('Event %s: Failed: %s', context.aws_request_id, str(e))
        # Don't allow a retry
        return
    finally:
        ev.finish()

    LOG.info('Event %s: Completed', context.aws_request_id)

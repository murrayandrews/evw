"""
OS related utilities


Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import signal
import os
import subprocess
import sys

__author__ = 'Murray Andrews'

# map signal numbers to names
signal_names = {
    getattr(signal, name): name for name in dir(signal)
    if name.startswith('SIG') and not name.startswith('SIG_')
    }


# ------------------------------------------------------------------------------
def signum(sig):
    """
    Convert a signal name (eg 'SIGINT' or 'INT') to its corresponding signal
    number.

    :param sig:         Either a signal name or a signal number. If the latter
                        it is checked for validity and returned unchanged if
                        valid.
    :type sig:          str | int

    :return:            The signal number.
    :rtype:             int

    :raise ValueError:  If the signal is not known.
    """

    if isinstance(sig, int):

        if sig not in signal_names:
            raise ValueError('Unknown signal: {}'.format(sig))
        return sig

    sig_num = getattr(signal, sig.upper(), getattr(signal, 'SIG' + sig.upper(), None))
    if not sig_num:
        raise ValueError('Unknown signal: {}'.format(sig))

    return sig_num


# ------------------------------------------------------------------------------
def signame(sig):
    """
    Convert a signal to its corresponding name.

    :param sig:         Either a signal number of name. If the latter it is
                        converted to its cannonical form (ie. SIGINT not INT).
    :type sig:          int | str
    :return:            The signal name.
    :rtype:             str

    :raise ValueError:  If the signal is not known.
    """

    if isinstance(sig, int):
        if sig in signal_names:
            return signal_names[sig]
        raise ValueError('Unknown signal: {}'.format(sig))

    # String signal name.
    return signal_names[signum(sig)]


# ------------------------------------------------------------------------------
def makedirs(path, mode=0o777, exist_ok=False):
    """
    Repackaging of os.makedirs() to ignore file exists error. This is required
    due to a bug in os.makedirs() in Python 3.4.0 which is fixed in 3.4.1.

    See https://bugs.python.org/issue13498
    and  https://bugs.python.org/issue21082

    :param path:        As for os.makedirs()
    :param mode:        As for os.makedirs()
    :param exist_ok:    As for os.makedirs()

    :return:
    """

    try:
        os.makedirs(path, mode, exist_ok)
    except OSError as e:
        # Ignore file exists error.
        if not exist_ok or e.errno != 17:
            raise


# ------------------------------------------------------------------------------

PS_COMMAND = {
    'darwin': "ps -caxo pid,command"
}


def getpids_for_command(command):
    """
    Return a list of the PIDs of processes running the specified
    command.  The command argument should be the basename of the
    command. i.e. the basename of the first word in the "CMD/command"
    column returned by ps(1). Any leading "-" in the running process
    names will be ignored.  i.e a command arg of "bash" will match
    processes listed as "-bash". If no processes match then an empty
    list is returned.

    WARNING: OS dependent hack. Probably better using psutil.

    :param command: Name of process (e.g. "bash")
    :type command:  str

    :return:        A list of process IDs which may be empty.
    :rtype:         list[int]

    """

    try:
        ps_command = PS_COMMAND[sys.platform]
    except KeyError:
        raise NotImplementedError("getpids_for_command doesn't support {}".format(sys.platform))

    procs = list()

    ps = subprocess.Popen(ps_command, shell=True, stdout=subprocess.PIPE)

    # Read and discard column header line
    ps.stdout.readline()

    for line in ps.stdout:

        pid, ps_name = line.decode('utf-8').strip().split(None, 1)

        if command == ps_name or (ps_name[0] == '-' and ps_name[1:] == command):
            procs.append(int(pid))

    return procs


# ------------------------------------------------------------------------------
def process_is_running(command):
    """
    Returns True if it appears that a process with the specified
    command is running and False otherwise.

    WARNING: OS dependent

    :param command: Name of process (e.g. "bash")

    """

    return len(getpids_for_command(command)) > 0

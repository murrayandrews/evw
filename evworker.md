# evworker(1) -- Remote event worker for the evw framework.


## SYNOPSIS

`evworker` \[options\] worker-name

## DESCRIPTION

**Evworker** is the wrapper for **evw** event handler plugins that allows
them to run on Linux-like nodes. It complements the **evlambda** wrapper
that allows the same plugin to run within AWS Lambda.


## OPTIONS
*   _worker_:
    Worker name. This must be of the form `plugin` or `plugin-xxx` where
    `plugin` is the name of an available **evw** event worker plugin.  This
    option controls both the event worker plugin that gets loaded, as well as the
    name of the SQS that will be monitored for events (unless the **-q**,
    **--queue** option is used).

*   `-b` _HEARTBEAT_, `--heartbeat` _HEARTBEAT_:
    Emit a heartbeat log message (log level _info_) every this many seconds.  A
    value of 0 (the default) disables heartbeats. If specified, a minimum of 30
    seconds is imposed. The heartbeat message contains the current, estimated,
    SQS queue length, the number of non-visible messages and the length of the
    internal event processing queue.
    
*   `--batch`:
    Process messages until the SQS queue is empty and then exit. Mutually
    exclusive with the `-d`, `--daemon` option. If neither option is used,
    **evworker** runs in the foreground.

*   `-c`, `--no-colour`, `--no-color`:
    Don't use colour in information messages.

*   `-d`, `--daemon`:
    Run as a daemon. The process ID will be written to _worker_.pid in the
    current directory.  This pid file also acts as a lock file to prevent two
    daemons for the same worker running at the same time. If it is necessary to
    run two at once, run them in different directories. Mutually exclusive with
    the `--batch` option. If neither option is used, **evworker** runs in the
    foreground.

*   `-E` _ENVFILE_, `--environ-file` _ENVFILE_:
    Name of a JSON formatted file containing variables that will be added to the
    environment. This is processed before, and in addition to, any
    `-e`/`--environ` arguments.


*    `-e` _name_=_value_, `--environ` _name_=_value_:
    Add the specified variable to the environment. Can be used more than once.
    Some plugins may require certain environment variables to be set. While
    environment variables can always be set in the conventional way, this is a
    convenience option.

*   `-h`:
    Print help and exit.

*   `-l` _LEVEL_, `--level` _LEVEL_:
    Print messages of a given severity level or above. The standard logging
    level names are available but _debug_, _info_, _warning_ and _error_ are
    most useful.  The Default is _info_.

*   `--log` _LOG_:
    Log to the specified target. This can be either a file name or a syslog
    facility with an @ prefix (e.g. _@local0_).

*   `--profile` _profile_:
    As for the AWS CLI **--profile** option.

*   `-q` _QUEUE_, `--queue` _QUEUE_:
    AWS SQS queue name. If not specified, the queue name is derived from the
    worker name.

*   `-r` _RETRIES_, `--retries` _RETRIES_:
    Maximum number of retry attempts to process an event. Default 2. If an event
    is not processed after this many retries, it is discarded. If a negative
    number is specified, retry limiting is disabled. A retry can occur either
    because **evworker** could not tell AWS SQS that it had finished processing
    it, or if the event processing experienced an error that might be overcome
    by retrying. In either case, the retry will occur after the SQS queue
    visibility timeout has expired. If the event processing experiences a
    permanent failure it will not be retried. It is up to individual worker
    plugins to decide if a processing failure is temporary or permanent.  It is
    possible for an event to be processed more than once if the processing was
    completed but **evworker** could not signal completion to SQS.

*   `-s` _SLEEP_, `--sleep` _SLEEP_:
    Sleep for this many seconds between SQS poll attempts when no messages are
    available.  Default 10.  Note that the `-w`/`--wait` time is outside this
    sleep time.

*   `-t` _THREADS_, `--threads` _THREADS_:
    Run this many threads. Default 5.

*   `-w` _WAIT_, `--wait` _WAIT_:
    Wait this many seconds for the SQS queue to provide messages (long polling).
    Must be in the range 0 .. 20.  Default is 10 seconds.

## SEE ALSO

[evw](https://bitbucket.org/murrayandrews/evw) on Bitbucket.

## AUTHOR

Murray Andrews

## LICENCE

[BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause).

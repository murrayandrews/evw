#!/usr/bin/env python3

"""
Multi-thrreaded event processing worker for AWS events. Events are received on
an SQS queue. The events are put on the queue by a companion Lambda function or
directly by AWS itself.

The event worker can be run in the foreground or as a daemon. Because of the SQS
queueing mechanism, its possible to run multiple workers if desired without any
requirement for coordination between them. Event workers can run on AWS EC2
Linux nodes or on other Linux-like nodes.

This file is not event type specific. It uses an "event worker plugin" to handle
the specifics of processing. The plugin supplies an Event() class which should
be a subclass of EventProto. Exactly the same plugin can also be run in AWS
Lambda using the evlambdaProto wrapper. When running in Lambda, it can hand-off
events to remote event workers (i.e. this script) if required (e.g. for long
running jobs).

................................................................................
Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
................................................................................

"""

# ..............................................................................
# region imports
# ..............................................................................

from __future__ import print_function

import argparse
import json
import logging
import os
import platform
import sys
import time
from datetime import datetime, timedelta
from logging.handlers import SysLogHandler
from threading import Thread

from common.const import LOGLEVEL, SQS_QUEUE_NAME

try:
    # Python 2
    # noinspection PyUnresolvedReferences, PyCompatibility
    from Queue import Queue
except ImportError:
    # Python 3
    # noinspection PyUnresolvedReferences, PyCompatibility
    from queue import Queue

import boto3

from evw.mu_daemon import daemonify
from common.utils import import_by_name, syslog_address, get_log_level
from common.event import EventRetryException

# ..............................................................................
# endregion imports
# ..............................................................................


# ..............................................................................
# region constants
# ..............................................................................

__author__ = 'Murray Andrews'

PROG = os.path.basename(sys.argv[0])
LOGNAME = 'evw'  # Set to None to include boto which uses root logger.
LOG = logging.getLogger(name=LOGNAME)
LONG_POLL_DEFAULT = 10  # Wait 10 seconds for SQS messages to arrive
LONG_POLL_MAX = 20  # Max wait time
THREADS = 5
RETRIES = 2  # No. retries to process an event before giving up on it.
SQS_POLL_SLEEP = 10  # Sleep between SQS polls when no messages available.
BUSY_SLEEP = 5  # Sleep for this many seconds if all threads busy.
HEARTBEAT_MIN = 30  # Minimum time between heartbeat messages

# ..............................................................................
# endregion constants
# ..............................................................................

# ..............................................................................
# region colour

# ------------------------------------------------------------------------------
# Clunky support for colour output if colorama is not installed.

try:
    # noinspection PyUnresolvedReferences
    import colorama
    # noinspection PyUnresolvedReferences
    from colorama import Fore, Style

    colorama.init()

except ImportError:
    class Fore(object):
        """
        Basic alternative to colorama colours using ANSI sequences
        """
        RESET = '\033[0m'
        BLACK = '\033[30m'
        RED = '\033[31m'
        GREEN = '\033[32m'
        YELLOW = '\033[33m'
        BLUE = '\033[34m'
        MAGENTA = '\033[35m'
        CYAN = '\033[36m'


    class Style(object):
        """
        Basic alternative to colorama styles using ANSI sequences
        """
        RESET_ALL = '\033[0m'
        BRIGHT = '\033[1m'
        DIM = '\033[2m'
        NORMAL = '\033[22m'


# endregion colour
# ..............................................................................


# ..............................................................................
# region logging

# ------------------------------------------------------------------------------
class ColourLogHandler(logging.Handler):
    """
    Basic stream handler that writes to stderr but with different colours for
    different message levels.

    """

    # --------------------------------------------------------------------------
    def __init__(self, colour=True):
        """
        Allow colour to be enabled or disabled.

        :param colour:      If True colour is enabled for log messages.
                            Default True.
        :type colour:       bool

        """

        super(ColourLogHandler, self).__init__()
        self.colour = colour

    # --------------------------------------------------------------------------
    def emit(self, record):
        """
        Print the record to stderr with some colour enhancement.

        :param record:  Log record
        :type record:   logging.LogRecord
        :return:
        """

        if self.colour:
            if record.levelno >= logging.ERROR:
                colour = Style.BRIGHT + Fore.RED
            elif record.levelno >= logging.WARNING:
                colour = Fore.MAGENTA
            elif record.levelno >= logging.INFO:
                colour = Fore.BLUE
            else:
                colour = Style.DIM + Fore.BLACK

            print(colour + self.format(record) + Fore.RESET + Style.RESET_ALL, file=sys.stderr)
        else:
            print(self.format(record), file=sys.stderr)


# ------------------------------------------------------------------------------
def setup_logging(level, target=None, colour=True, name=None, prefix=None):
    """
    Setup logging.

    :param level:   Logging level. The string format of a level (eg 'debug').
    :param target:  Logging target. Either a file name or a syslog facility name
                    starting with @ or None. If None, log to stderr.
    :param colour:  If True and logging to the terminal, colourise messages for
                    different logging levels. Default True.
    :param name     The name of the logger to configure. If None, configure the
                    root logger.
    :param prefix:  Messages a prefixed by this string (with colon+space
                    appended). Default None.
    :type level:    str
    :type target    str | None
    :type colour:   bool
    :type prefix:   str | None
    :type name:     str | None

    :raise ValueError: If an invalid log level or syslog facility is specified.
    """

    logger = logging.getLogger(name)
    logger.propagate = False
    logger.setLevel(get_log_level(level))

    # Get rid of unwanted handlers.
    for h in logger.handlers:
        logger.removeHandler(h)

    if target:
        if target.startswith('@'):
            # Syslog to specified facility

            if target[1:] not in SysLogHandler.facility_names:
                raise ValueError('Bad syslog facility: {}'.format(target[1:]))
            h = SysLogHandler(address=syslog_address(), facility=target[1:])
            h.setFormatter(
                logging.Formatter(
                    (prefix if prefix else '') + '[%(process)d]: %(levelname)s: %(threadName)s: %(message)s'
                )
            )
        else:
            # Log to a file
            h = logging.FileHandler(target)
            h.setFormatter(logging.Formatter('%(asctime)s: %(levelname)s: %(threadName)s: %(message)s'))
        logger.addHandler(h)
        logger.debug('%s', ' '.join(sys.argv))
        logger.debug('Logfile set to %s', target)
    else:
        # Just log to stderr.
        h = ColourLogHandler(colour=colour)
        h.setFormatter(logging.Formatter(
            (prefix + ': ' if prefix else '') + '%(threadName)s: %(message)s'))
        logger.addHandler(h)
    logger.debug('Log level set to %s (%d)', level, logger.getEffectiveLevel())


# endregion logging
# ..............................................................................


# ------------------------------------------------------------------------------
class StoreNameValuePair(argparse.Action):
    """
    Used with argparse to store values from options of the form:
        --option name=value

    The destination (self.dest) will be created as a dict {name: value}. This
    allows multiple name-value pairs to be set for the same option.

    Usage is:

        argparser.add_argument('-x', metavar='key=value', action=StoreNameValuePair)

    or
        argparser.add_argument('-x', metavar='key=value ...', action=StoreNameValuePair,
                               nargs='+')

    """

    # --------------------------------------------------------------------------
    # noinspection PyUnresolvedReferences
    def __call__(self, parser, namespace, values, option_string=None):

        if not hasattr(namespace, self.dest) or not getattr(namespace, self.dest):
            setattr(namespace, self.dest, dict())
        argdict = getattr(namespace, self.dest)

        if not isinstance(values, list):
            values = [values]
        for val in values:
            try:
                n, v = val.split('=', 1)
            except ValueError as e:
                raise argparse.ArgumentError(self, str(e))
            argdict[n] = v


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Remote event worker for AWS.'
    )

    # noinspection PyTypeChecker
    argp.add_argument('-b', '--heartbeat', action='store', type=int, default=0,
                      help='Emit a heartbeat log message every this many seconds.'
                           ' A value of 0 (the default) disables heartbeats. If'
                           ' specified, a minimum of {} seconds is imposed.'.format(HEARTBEAT_MIN))

    run_mode_group = argp.add_argument_group(
        'run mode arguments',
        description='If none of the following are specified, run in the foreground.'
    )
    run_mode_args = run_mode_group.add_mutually_exclusive_group()

    run_mode_args.add_argument('--batch', action='store_true',
                               help='Run a single batch and exit when queue is empty.')

    argp.add_argument('-c', '--no-colour', '--no-color', dest='colour', action='store_false',
                      default=True, help='Don\'t use colour in information messages.')

    run_mode_args.add_argument('-d', '--daemon', action='store_true',
                               help='Run as a daemon.')

    argp.add_argument('-E', '--environ-file', action='store', dest='envfile',
                      help='Name of a JSON formatted file containing variables'
                           ' that will be added to the environment. This is processed'
                           ' before, and in addition to, any -e/--environm arguments.')

    argp.add_argument('-e', '--environ', action=StoreNameValuePair, metavar='name=value',
                      help='Add the specified variable to the environment. Can be used'
                           ' more than once. Some plugins'
                           ' may require certain environment variables to be set.')

    argp.add_argument('-l', '--level', metavar='LEVEL', default=LOGLEVEL,
                      help='Print messages of a given severity level or above.'
                           ' The standard logging level names are available but info,'
                           ' warning and error are most useful. The Default is {}.'.format(LOGLEVEL))

    argp.add_argument('--log', action='store',
                      help='Log to the specified target. This can be either a file'
                           ' name or a syslog facility with an @ prefix (e.g. @local0).')

    argp.add_argument('--profile', action='store', help='As for AWS CLI.')

    # noinspection PyTypeChecker
    argp.add_argument('-s', '--sleep', action='store',
                      type=int, default=SQS_POLL_SLEEP,
                      help='Sleep for this many seconds between SQS poll attempts'
                           ' when no messages are available. Note that the -w/--wait'
                           ' time is outside this sleep time. Default {}.'.format(SQS_POLL_SLEEP))

    argp.add_argument('-q', '--queue', action='store',
                      help='AWS SQS queue name. If not specified, the queue name'
                           ' is derived from the worker name.')

    # noinspection PyTypeChecker
    argp.add_argument('-r', '--retries', action='store', type=int, default=RETRIES,
                      help='Maximum number of retry attempts to process an event.'
                           ' If an event is not processed after this many retries,'
                           ' it is discarded. Note that it is possible for an event'
                           ' to be processed more than once. If set to a negative'
                           ' number, retry limiting is disabled. Default {}.'.format(RETRIES))

    # noinspection PyTypeChecker
    argp.add_argument('-t', '--threads', action='store', type=int, default=THREADS,
                      help='Run this many threads. Default {}.'.format(THREADS))

    argp.add_argument('--tag', action='store', default=PROG,
                      help='Tag log entries with the specified value. The default is {}.'.format(PROG))

    # noinspection PyTypeChecker
    argp.add_argument('-w', '--wait', action='store', type=int, default=LONG_POLL_DEFAULT,
                      help='Wait this many seconds for the SQS queue to provide messages'
                           ' (long polling). Must be in the range 0 .. {}. Default is'
                           ' {} seconds.'.format(LONG_POLL_MAX, LONG_POLL_DEFAULT)
                      )

    argp.add_argument('worker', action='store',
                      help='Worker name. This must be of the form "plugin" or'
                           ' "plugin-xxx" where plugin is the name of an available'
                           ' event worker plugin.'
                      )

    return argp.parse_args()


# ------------------------------------------------------------------------------
def heartbeat(event_queue, sqs_queue, hb_period):
    """
    Thread worker to issue heartbeat messages.

    :param event_queue: An internal (Python) queue containing messages extracted
                        from SQS by the main thread. Each one represents a AWS
                        event that needs to be processed.

    :param sqs_queue:   SQS queue from which events are being loaded.
    :param hb_period:   How often heartbeat messages are issued.

    :type event_queue:  Queue
    :type hb_period:    int

    :return:
    """

    LOG.debug('Starting')
    assert hb_period > 0, 'Bad heartbeat period: {}'.format(hb_period)

    wake_time = datetime.utcnow()
    sqs_q_arn = sqs_queue.attributes['QueueArn']

    while True:
        wake_time += timedelta(seconds=hb_period)
        try:
            sqs_queue.load()
        except Exception as e:
            LOG.warning('Could not get attributes on %s: %s', sqs_q_arn, e)
            sqs_q_len = '??'
            sqs_q_nvis = '??'
        else:
            sqs_q_len = sqs_queue.attributes['ApproximateNumberOfMessages']
            sqs_q_nvis = sqs_queue.attributes['ApproximateNumberOfMessagesNotVisible']

        now = datetime.utcnow()

        LOG.info(
            '{date} {host} {sqs_q} len={sqs_q_len} notvisibile={sqs_q_nvis} Internal len={int_q_len}'.format(
                date=now.isoformat(),
                host=platform.node(),
                sqs_q=sqs_q_arn,
                sqs_q_len=sqs_q_len,
                sqs_q_nvis=sqs_q_nvis,
                int_q_len=event_queue.qsize()
            )
        )

        try:
            time.sleep((wake_time - now).total_seconds())
        except ValueError as e:
            LOG.warning(str(e))


# ------------------------------------------------------------------------------
def process_messages(event_queue, event_class, profile=None):
    """
    Thread worker to process event messages extracted from SQS. If the messages
    are processed successfully, or can never be processed successfully, they are
    removed from the SQS queue. If they raise a EventRetryException, they are
    not removed from the queue so they can be retried once the SQS message
    visibility timeout expires.

    :param event_queue: An internal (Python) queue containing messages extracted
                        from SQS by the main thread. Each one represents a AWS
                        event that needs to be processed.

    :param event_class: The EventProto() subclass that will process events.
    :param profile:     AWS profile name (for credentials selection)
    :type event_queue:  Queue
    :type profile:      str
    """

    LOG.debug('Starting')

    # Boto sessions are not thread safe so need one per thread.
    aws_session = boto3.Session(profile_name=profile)

    while True:

        event_msg = event_queue.get()

        # ----------------------------------------
        # Get event ID. If not present as message attribute use the message ID.
        # evlambda will provide an event id as a message attribute.
        event_id = None
        if event_msg.message_attributes:
            event_id = event_msg.message_attributes.get('EventId', {}).get('StringValue')
        if not event_id:
            event_id = 'sqs/' + event_msg.message_id

        LOG.info('Event %s: Received as message %s', event_id, event_msg.message_id)

        # ----------------------------------------
        # Extract the message body which contains the event data
        try:
            event = event_class(
                json.loads(event_msg.body),
                aws_session=aws_session,
                event_id=event_id,
                logger=LOG
            )
        except Exception as e:
            # No point leaving this for another worker - it can never run
            LOG.error('Event %s: Discarding - %s', event_id, str(e))
            event_msg.delete()
            event_queue.task_done()
            continue

        LOG.debug('Event %s: %s', event_id, str(event.event))

        try:
            event.process()
        except EventRetryException as e:
            # Can retry this one so don't delete it from the SQS queue.
            # It will pop back into the SQS queue once the visibility
            # time-out expires.
            LOG.warning('Event %s: Will retry - %s', event_id, str(e))
            continue
        except Exception as e:
            LOG.error('Event %s: Discarding - %s', event_id, str(e))
            event_msg.delete()
        else:
            LOG.info('Event %s: Complete', event_id)
            event_msg.delete()
        finally:
            event.finish()
            event_queue.task_done()


# ------------------------------------------------------------------------------
def main():
    """
    Kicks off a bunch of handler threads then receives messages from the SQS
    queue and feeds them to the processing threads. Loops endlessly if not in
    batch mode. In batch mode, exits once the SQS queue is empty.

    :raise Exception: If anything goes wrong.
    """

    setup_logging('info', name=LOGNAME, prefix=PROG)
    args = process_cli_args()

    if not 0 <= args.wait <= LONG_POLL_MAX:
        raise ValueError('Argument -w/--wait: value must be in the range 0 .. {}'.format(LONG_POLL_MAX))

    if args.heartbeat and args.heartbeat < HEARTBEAT_MIN:
        raise ValueError('Argument -b/--heartbeat: value must be 0 or greater than {}'.format(HEARTBEAT_MIN))

    # ----------------------------------------
    # Setup the environment vars

    if args.envfile:
        with open(args.envfile, 'r') as jfp:
            env = json.load(jfp)

        if not isinstance(env, dict):
            raise Exception('{}: must be a dictionary object'.format(args.envfile))
        os.environ.update(env)

    if args.environ:
        os.environ.update(args.environ)

    # ----------------------------------------
    # Load the event handler plugin and get the event handler class.
    event_handler_module = import_by_name(args.worker.split('-')[0], 'handlers')
    try:
        event_handler_class = getattr(event_handler_module, 'Event')
    except AttributeError:
        raise Exception(
            '{} does not appear to be a worker plugin - no Event() class'.format(args.worker)
        )

    if args.daemon:
        daemonify(pidfile=args.worker + '.pid')

    setup_logging(args.level, name=LOGNAME, target=args.log, colour=args.colour, prefix=args.tag)

    # ----------------------------------------
    # Get the SQS queue. Lambda job or another AWS event source will have put
    # messages on this. In the latter case there won't be an EventId attribute.
    aws_session = boto3.Session(profile_name=args.profile)
    sqs_queue_name = args.queue if args.queue else SQS_QUEUE_NAME.format(worker_name=args.worker)
    LOG.debug('Getting SQS queue %s', sqs_queue_name)
    sqs_queue = aws_session.resource('sqs').get_queue_by_name(QueueName=sqs_queue_name)
    LOG.info('SQS queue %s', sqs_queue.url)

    # ----------------------------------------
    # Prepare for threading - create an internal event message queue and start
    # worker threads. The main thread pulls jobs of the SQS queue and puts them
    # on the internal queue for the worker threads.

    event_queue = Queue(maxsize=args.threads)

    for t in range(args.threads):
        worker = Thread(
            target=process_messages,
            name='thread-{:02}'.format(t),
            args=(event_queue, event_handler_class, args.profile),
        )
        worker.daemon = True
        worker.start()

    # ----------------------------------------
    # Start the heartbeat thread
    if args.heartbeat:
        worker = Thread(
            target=heartbeat,
            name='heartbeat',
            args=(event_queue, sqs_queue, args.heartbeat)
        )
        worker.daemon = True
        worker.start()

    # ----------------------------------------
    # Poll for messages on the SQS queue and place them on the internal worker queue
    sleep_time = max(0, args.sleep)
    while True:
        if event_queue.full():
            # Don't retrieve messages from SQS if our internal worker queue is full
            time.sleep(BUSY_SLEEP)
            continue

        LOG.debug('Polling SQS')
        event_messages = sqs_queue.receive_messages(
            AttributeNames=['All'],  # We don't really use this info
            MessageAttributeNames=['All'],
            WaitTimeSeconds=args.wait,
            MaxNumberOfMessages=1
        )

        if event_messages:
            for ev in event_messages:
                attempt_number = int(ev.attributes['ApproximateReceiveCount'])

                if args.retries >= 0 and attempt_number > args.retries + 1:
                    LOG.warning('Event message %s: Retry limit exceeded - deleted on cycle %d',
                                ev.message_id, attempt_number)
                    ev.delete()
                else:
                    event_queue.put(ev)
        elif args.batch:
            LOG.info('No more messages in batch -- finishing')
            break
        elif sleep_time:
            LOG.debug('SQS poll sleep %d', sleep_time)
            time.sleep(sleep_time)

    LOG.debug('Waiting for worker threads to complete their work before finishing')
    event_queue.join()

    # Allow the daemon threads to finish before destroying everything
    time.sleep(1)
    return 0


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    # exit(main())  # Uncomment for debugging
    try:
        exit(main())
    except Exception as ex:
        LOG.error('%s', ex)
        exit(1)
    except KeyboardInterrupt:
        LOG.warning('Interrupt')
        exit(2)

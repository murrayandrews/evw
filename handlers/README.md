# Evw Event Handler Plugins

**Evw** event handler plugins are located in the `handlers` package. These are
used by both event workers and Lambda event handlers. Some plugins have
additional components in the `misc` directory.

Note that **evw** currently does not support synchronous Lambda function
invocation.

## Existing Plugins

The following handlers are currently available.

|Handler|Description|
|-|-|
|demo_l|A demo handler that just logs the incoming event. It never performs handoff to a remote worker.|
|demo_w|A demo handler that just logs the incoming event. It always performs handoff to a remote worker.|
|[rsDropLoader](rsDropLoader.md)|S3 to Redshift loader. A file dropped into S3 is loaded into Redshift. Handoff between Lambda and a remote worker is configurable and conditional on source file size.|
|[rsDropLoader2](rsDropLoader2.md)|An alternative implementation to **rsDropLoader** that provides the end user with more autonomy in the loading process.|
|[mbot](mbot.md)|A messaging bot that can send messages to named endpoints (e.g. users) via various mechanisms (e.g. email, SMS, push messaging ...).|
|[ipranger](ipranger.md)|A utility that analyses changes to AWS IP address ranges when they occur and sends SNS and email based summaries of the changes to subscribed users.|
|[s3handler](s3handler.md)|Handle S3 event messages with arbitrary executables (e.g. shell scripts).|
|[slacker](slacker.md)|Forward AWS SNS messages to Slack channels.|
|[pager](pager.md)|Send alarm messages to paging services such as PagerDuty and VictorOps.|

## Plugin Components

Each plugin consists of a number of components. For a plugin named _myPlugin_,
these components will be:

|File|Required|Description|
|-|:-:|-|
|handlers/myPlugin.py|Yes|The plugin Python source module.|
|handlers/myPlugin.mf|Sometimes|A JSON formatted manifest that specifies additional non-standard Python modules and other files required by the plugin. If no additional modules or files are required, the manifest is not required.|
|handlers/myPlugin.md|Recommended|A markdown formatted file documenting the plugin. Not strictly required but plugins aren't much use without documentation.|
|misc/myPlugin/\*|No|Additional support components such as CloudFormation scripts, additional documentation etc.|

## Plugin Python Modules

The plugin Python module contains the actual code for the module.

For a plugin named `myPlugin`, the source file must be named
`handlers/myPlugin.py`.

Both the **evlambda** Lambda wrapper and the **evworker** remote worker daemon
dynamically load plugins at run-time. The Lambda wrapper derives the plugin
module name from the Lambda function name. The remote worker derives the plugin
module name from the _worker-name_ command argument (which is the same as the
Lambda function name in the situation where a Lambda function is sending events
to the worker).

### Plugin Rules

The following rules apply to the Python plugin source:

1.  Plugins must contain an `Event` class that inherits from `common.EventProto`.
1.  Plugins must implement the `process()` method within the `Event` class.
1.  Plugins must be agnostic about whether they are running in AWS Lambda or
    another Linux environment (typically with connectivity to AWS services).
1.  Plugins must be thread-safe. Boto3 is one to watch in this regard as `Session`
    objects are not thread-safe.
1.  Plugins must not have memory leaks, leave garbage on the file system or
    consume any other resource without releasing it before exiting.
1.  Plugins must not assume they have access to any part of the file system
    apart from the current directory and /tmp.
1.  Plugins must not rely on the AWS Lambda context object. This is not
    available outside Lambda.
1.  Plugins should not use `print()` to write to stdout/stderr except for
    debugging.  Use the logging system instead.

### The Event Class

Each plugin must contain an `Event` class that inherits from `common.EventProto`.

#### Methods

The following methods are available within the plugin's `Event` class for use
by, or requiring implementation within, the plugin.

|Method|Description|
|-|-|
|\_\_init\_\_()|If overriding the base class `__init__()` it is essential that the superclass `__init__()` is called. Have a look at the code for the [rsDropLoader](rsDropLoader.md) plugin to see how to do this safely.|
|handoff()|Determine if an event should be handed off to an event worker instead of being handled locally in Lambda. The base class implementation always returns False. This method is only ever run in a Lambda context, never by **evworker**, so it must not cause any side effects depended on by the `process()` method. When overriding the base class method, do not invoke the superclass `handoff()` method.|
|process()|Process the event (which is available as `self.event`). The plugin must implement this method and it must not call the superclass `process()` method. If `process()` raises a `common.event.EventRetryException`, the event is not removed from the event queue and will reappear after the visibility timeout has expired. This provides a basic retry mechanism for events.|
|finish()|Called after event processing is complete to allow any final cleanup or release of resources. The base class implementation does nothing. Implement this method if required.|
|send_msg(args)|Send a message to someone or something. The actual processing is handled by a [messenger plugin](#markdown-header-sending-messages-from-plugins).|

#### Instance Attributes

The following instance attributes are available within the plugin's `Event`
class. Plugins can add their own instance attributes by providing a custom
`__init__()` method that **must** invoke the superclass `__init__()`.

|Attribute|Description|
|-|-|
|event|The event as it was provided by AWS; either Lambda or another AWS service. This is a dictionary. The contents are event type specific.|
|event_id|An event identifier. For a Lambda sourced event this will be the `aws_request_id` from the context, prefixed with `evl/`, even if the event is handed off to a remote worker by **evlambda**. This allows correlation of Lambda event logs in CloudWatch with any logs the remote worker produces, provided the latter includes the event identifier in the log entries. If the event appeared on the remote worker's SQS queue from somewhere other than **evlambda**, the event identifier will be the SQS message id prefixed with `sqs/`. If the source of the event cannot be determined, the event identifier is a random UUID prefixed with `xxx/`.|
|aws_session|A boto3 `Session` object. Within the multi-threaded **evworker**, each thread has its own dedicated `Session` available to the plugin to avoid problems.| 
|logger|A logger object from the Python `logging` module. This should be used for all logging rather than the default/root logger or `print()` statements. The **evworker** takes care to separate the plugin logger from the boto3 loggers as the latter produces massive amount of generally useless information. This is still TBD for **evlambda**.|

### Sending Messages from Plugins

The `send_msg()` method available in the plugin `Event` class is available to
plugins to send messages to people or systems. It's up to the plugin to send
messages as no messages are sent unbidden by the **evw** framework itself.

The actual processing is handled by a messenger plugin.
[More information on messengers](../messengers/README.md).

The `send_msg()` method takes the following arguments:

|Argument|Type|Description|
|-|-|-|
|to|str|A string or list of strings specifying destinations. Each one must be in the format `dest-type:dest-address`. The `dest-type` is used to load an appropriate plugin and the `dest-address` is plugin type specific. For example, `ses:fredbloggs@gmail.com` would send an email to `fredbloggs@gmail.com` using the `ses` plugin which uses the AWS Simple Email Service.|
|subject|str|Message subject.|
|message|str|Message body.|

Note that messenger plugins may vary on whether they use the subject, message
body or both. For example the `sms` (SMS) plugin ignores the message body if a
subject is provided.

### Accessing AWS Resources / Services

Most event handler plugins will need to access AWS services or resources (S3
etc.)

This must be done using the
[boto3](https://boto3.readthedocs.io/en/latest/index.html) SDK for AWS.

Plugins must use the boto3 `Session` object provided as `self.aws_session`
rather than the default `Session` object provided by the boto3 module, both to
avoid multi-threading issues and to allow the **evworker** to use a different
AWS account profile specified on the command line.

So do this:

```python
s3service = self.aws_session.service('s3')
ec2client = self.aws_session.client('ec2')
```

Do **not** do this:

```python
s3service = boto3.service('s3')
ec2client = boto3.client('ec2')
```

### Logging

Event handler plugins should not use the default / root logger provided by the
Python `logging` module. Instead they should use the logger provided as
`self.logger`.

So do this:

```python
self.logger.info('Yes')
```

Do **not** do this:

```python
logging.getLogger().info('No')
```

### Handoff() vs Process()

Within **evlambda**, the processing flow is:

*   Call `handoff()` to make a handoff decision.
*   If `handoff()` returned `False`, call `process()` to process the
    event in Lambda otherwise send it to **evworker**.
*   If `handoff()` returned `False`, call `finish()` to cleanup.

Within **evworker**, the processing flow is:

*   Call `process()` to process the event.
*   Call `finish()` to cleanup.

The dilemma here is that `handoff()` may need to do some expensive operations
(e.g.  load configuration files from S3) in order to make its decision but the
`process()` method cannot know if `handoff()` has been called first.

Hence `process()` cannot rely on any pre-work `handoff()` may have done and may
have to repeat those expensive operations.

There are a couple of options to address this:

*   Do any expensive initialisation activities in the plugin's `__init__()`.

    This is ok provided the initialisation activities are guaranteed to be
    required in all circumstances. However, if they're only required in certain
    circumstances, its wasted effort. In Lambda, that may matter due to the AWS
    time-based charging model.

*   For operations such as loading configuration files from S3, wrap them in
    an access method that performs the operation once and caches the result.
    Subsequent calls then deliver the cached result.

    This is the approach taken by the [rsDropLoader](rsDropLoader.md) plugin to
    reading the configuration and control files. Whichever of `handoff()` or
    `process()` first needs the data populates the cache and subsequent calls by
    either simply re-use it.

### Other Utilities Available to Plugins

As well as the standard Python modules and any plugin specific custom modules,
the **evw** framework makes a number of other utility functions available. These
are located in the `common` package.

Check out the package itself for the specifics. Some of the more useful ones are
listed [here](../common/README.md).


## Plugin Manifests

The plugin manifest is a JSON formatted file that identifies any non-standard
Python modules or other files that need to be included:

1.  In the zip bundle that is uploaded to AWS Lambda in order for the plugin
    to work with **evlambda**. 
2.  In the tar.bz2 bundle used to create a self contained **evworker**
    installation.

Tools are provided in the [etc](../etc/README.md) directory for creating these
bundles from the manifest.

For a plugin named `myPlugin`, the manifest must be named `handlers/myPlugin.mf`.

If no additional modules or files are required, the manifest is not required.

The manifest file is not used by the **evlambda**/**evworker** wrappers. It is
used by the the support scripts that create the deployment bundles.

### Manifest Contents

The manifest can contain the following keys:

|Key name|Description|
|-|-|
|modules|A list of module names. These modules must be present in the virtual environment named `venv` in the **evw** source directory. It is not necessary to include either `boto3` or `yaml` if the default versions are acceptable, as these are either provided by Lambda or included automatically by `etc/pkg-lambda.sh`.|
|files|A list of files or directories that should be included. File names are relative to the **evw** source directory|

Other keys are ignored. It's a good idea to add some descriptive keys, such as
`author`, `description`, `version` etc.

### Sample Manifest

As an example, this is the manifest for the [rsDropLoader](rsDropLoader.md)
plugin:

```javascript
{
    "description": "Manifest for rsDropLoader event handler plugin",
    "author": "Murray Andrews",
    "files": ["licence.txt"],
    "modules": ["pg8000"]
}
```

## Plugin Documentation

The preferred method for documenting plugins is to create a (bitbucket
compatible) markdown file that describes how it is triggered, what it does and
how it is controlled or configured.

For a plugin named `myPlugin`, the plugin documentation should be be named
`handlers/myPlugin.md`.

## Development and Testing of Plugins

Development of Lambda functions is a pain. AWS development tools are virtually
non-existent and the CloudWatch based logging arrangement is awful.

Contrast this with Google App Engine which comes with a fully functional desktop
development SDK which is directly supported by powerful IDEs such as PyCharm as
well as an online logging facility that is, more or less, usable.

One way to make this process less painful is to use **evw** to bounce events
from Lambda back to your desktop environment where development tools are more
accessible.

To do this, install a skeleton plugin via **evlambda** that has a `handoff()`
method that always returns `True`. The `demo_w` plugin does this.

You also need to setup the appropriate SQS handoff queue and IAM permissions.

With these in place, events that hit the Lambda function will be deflected to
the SQS queue where they can be picked up by **evworker** running on your
desktop with your _under development_ plugin.





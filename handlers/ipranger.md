# Evw Event Handler Plugin - ipranger

## Overview

AWS occasionally makes changes to its
[IP address ranges](http://docs.aws.amazon.com/general/latest/gr/aws-ip-ranges.html).

These changes may require configuration changes in AWS based environments,
typically network ACLs and security groups.

While AWS does post a notification to a globally available SNS topic when
changes occur, this notification is pretty unhelpful in that it only tells you
something has changed rather than what has changed. To work that out, you need
to download their [current JSON file](https://ip-ranges.amazonaws.com/ip-ranges.json),
over 5000 lines of it, and compare it to whatever previous version you may have
to determine if the changes impact you. Painful.

**IPranger** automates this process, summarising the changes in human readable
form. It can distribute a summary of changes via SNS and also via AWS SES (email).
In the latter case, it allows users to subscribe to changes for particular AWS
regions of interest.


## How it Works

When AWS changes their IP ranges, a notification is posted message to a globally
available SNS topic `arn:aws:sns:us-east-1:806199016981:AmazonIpSpaceChanged`.
This sends a message that looks like this:

```json
{
  "create-time":"yyyy-mm-ddThh:mm:ss+00:00",
  "synctoken":"0123456789",
  "md5":"6a45316e926d5d37836de8bc9463c933",
  "url":"https://ip-ranges.amazonaws.com/ip-ranges.json"
}
```

The **ipranger** Lambda function is subscribed to the topic and receives
the message and does the following:

1.  Download the new IP address ranges JSON file.
2.  Load the cache of previous AWS IP ranges in S3. If there is no previous
    version then the latest version is placed in the cache and **ipranger**
    exits.
3.  Compare the previous IP address ranges with the latest ones and formulate
    a region-by-region summary of changes.
4.  Load the contents of the DynamoDB [subscriptions table](#markdown-header-the-subscriptions-table).
    This is basically a list of email addresses and AWS regions of interest
    for each email address.
5.  Send an email to every subscriber where there is in IP range change
    in one or more of their regions of interest.
6.  Publish a message to an **ipranger** owned SNS topic summarising
    the changes across all regions.
7.  Store the latest version of the IP ranges JSON file in the S3 cache
    for next time.

To be informed of changes, users can either subscribe to the **ipranger** SNS
topic or have their email address included in the
[subscriptions table](#markdown-header-the-subscriptions-table). The latter is
more fine grained and the messages are prettier but it requires that SES is
enabled for the environment.

## Installation

While **ipranger** can be installed in the same way as other **evw** modules,
it requires a number of AWS components to be present to allow it to work.
These are:

-   An SNS topic used to distribute formatted results.
-   An S3 cache bucket to hold new versions of the address ranges JSON file
    as they become available.
-   A DynamoDB table containing user subscription information.
-   The Lambda function itself with a number of environment variables.
-   An IAM role to allow the Lambda function to access the resources.
-   A subscription to the AWS provided SNS topic that involves the Lambda function.

All of this is automated with a CloudFormation template: `misc/ipranger/ipranger.cfn.json`.
Documentation for the can be found [here](misc/ipranger/ipranger.cfn.md).

> Installation must be performed in the **us-east-1** region.

Using this template, the process is:

1.  Clone the **evw** repository.

2.  Run the following command to generate the Lambda install bundle.

    ```bash
    etc/pkg-evlambda.sh -r us-east-1  ipranger
    ```

    This will produce a file `evl-ipranger.zip`.

3.  Load `evl-ipranger.zip` into an S3 bucket somewhere. You will need the
    location a bit later.

4.  Go to the AWS CloudFormation console and make sure you are looking at
    the `us-east-1` region.

5.  Create a new CloudFormation stack using the template `misc/ipranger/ipranger-cfn.json`.
    You will be asked to populate a number of parameters, described below.

### CloudFormation Template Parameters

|Parameter|Required|Description|
|-|-|-|
|IPrangerMessageFileHtml|No|**IPranger** uses [Jinja2](http://jinja.pocoo.org) to format the messages it sends. This parameter specifies an S3 location containing a Jinja2 template for HTML formatted emails. If not specified, an internal template is used.|
|IPrangerMessageFileTxt|No|An S3 location containing a Jinja2 template for plaintext formatted SNS messages and emails. If not specified, an internal template is used.|
|ModuleName|Yes|The Lambda module name. This also controls a number of other resource names. Best to leave it set to the default of `ipranger`.|
|S3CacheBucket|Yes|S3 bucket used to cache AWS IP ranges JSON files as they become available.|
|S3CacheBucketCreate|Yes|If set to `yes` (the default), it is assumed the `S3CacheBucket` does not exist and needs to be created.|
|S3CachePrefix|No|The prefix within the `S3CacheBucket` for cached JSON files. Can be empty. Do not include a trailing or leading `/`.|
|S3CodeBucket|Yes|The bucket in which the  `evl-ipranger.zip` code bundle was placed. This must exist and must contain the code bundle prior to creating the stack.|
|S3CodeKey|Yes|The full path name of the code bundle in the `S3CodeBucket` (no leading `/`).|
|SESsender|No|The sender address used by **ipranger** for sending SES emails to subscribers. It can be of the form `Sender name <sender-email>`. If not specified, email based subscriptions are disabled.|
|SubscriptionsTableCreate|Yes|If set to `yes` (the default), a DynamoDB table will be created for user subscriptions to IP ranges update information. This is described further below.|

### Testing

If you want to test it out, do the following:

1.  Copy the provided `misc/ipranger/ipranges-empty.json` file to `latest.json`
    in the S3 cache area. It has the same format as the standard AWS file but
    contains no IP range data.
2.  Add one or more subscriptions to the **ipranger** SNS topic.
3.  Add one or more entries to the DynamoDB
    [subscriptions table](#markdown-header-the-subscriptions-table).
4.  Go to the Lambda console and select the **ipranger** function that was
    created as part of the CloudFormation stack.
5.  Under the `Actions` menu, select `Configure test event`.
6.  Replace the contents of the test event with the contents of the file
    `misc/ipranger/test-event.json`.
7.  Click `Save and Test`.

If all is ok, the subscribers should receive some emails. Note that this
test will also update the `latest.json` file in the S3 cache with the
current AWS address range list. So if you click `Test` again, no new emails
should be sent. If you want to retest, repeat step 1.

If there are any problems, check the CloudWatch logs in the `/aws/lambda/ipranger`
log group.

## The Subscriptions Table

The subscriptions table is a DynamoDB table named `ipranger.subscriptions`. It is created by
the provided CloudFormation template. Entries must be added manually.

|Name|Required|Type|Description|
|-|-|-|-|
|email|Yes|string|Email address of a subscriber.|
|regions|No|string|A comma separated list of regions or region patterns in which the user is interested. Patterns are GLOB style (e.g. `ap-southeast-*` will match `ap-southeast-1` and `ap-southeast-2` while `*` matches every region). The special AWS region `GLOBAL` is always included and does not need to be explicitly listed. If no value is provided, then `*` (all regions) is assumed.|
|disabled|No|boolean|If `true`,  sending of email to this user is disabled. This will not prevent SNS sending notifications to this address if it is subscribed to the **ipranger** topic.|

## Environment Variables

In addition to the [standard environment variables](../README.md#markdown-header-lambda-environment-variables) supported by all **evw** handlers,
**ipranger** supports the following additional variables.

All of these variables are initially set by the CloudFormation template when **ipranger**
is installed so you should not need to fiddle with them to get started. However they
can be changed as required by either updating the CloudFormation stack (preferred)
or directly in the Lambda console.

|Name|Required|Description|
|-|-|-|
|IPRANGER_MSG_TEMPLATE_HTML|No|S3 location containing a Jinja2 template for HTML formatted SNS messages and emails. If not specified, an internal template is used.|
|IPRANGER_MSG_TEMPLATE_TXT|No|S3 location containing a Jinja2 template for plaintext formatted SNS messages and emails. If not specified, an internal template is used.|
|IPRANGER_S3_CACHE|Yes|S3 location (bucket and prefix) used to cache AWS IP ranges JSON files as they become available. **IPranger** saves the most recent file as `latest.json`. All received files are also stored by date but these are not used by **ipranger**.|
|IPRANGER_SES_SENDER|No|The sender address used by **ipranger** for sending SES emails to subscribers. It can be of the form `Sender name <sender-email>`. If not specified, email based subscriptions are disabled.|
|IPRANGER_SNS_TOPIC_ARN|No|The ARN of the topic to which **ipranger** publishes its updates. If not specified, **ipwranger** will look for an SNS topic named `ipranger`.|

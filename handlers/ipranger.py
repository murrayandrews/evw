"""
evlambda/evworker plugin to analyse changes to the AWS IP address ranges
and send change information to SNS queue(s).

The trigger events are messages published by AWS to this SNS topic:

    arn:aws:sns:us-east-1:806199016981:AmazonIpSpaceChanged

Each new file is compared to the previous one and changes are published in
human readable form to an SNS topic named after the plugin.

Previous versions are stored in S3.

The event message Records[0]['Sns']['Message'] looks like a JSON encoded
version of this:

{
  "create-time":"yyyy-mm-ddThh:mm:ss+00:00",
  "synctoken":"0123456789",
  "md5":"6a45316e8bc9463c9e926d5d37836d33",
  "url":"https://ip-ranges.amazonaws.com/ip-ranges.json"
}

Note that the SNS message has 'synctoken' while the ranges file has 'syncToken'.

................................................................................
Copyright (c) 2017, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
................................................................................

"""

from __future__ import print_function

import os
from collections import namedtuple
import json
import operator
from collections import OrderedDict
from fnmatch import fnmatch

from botocore.exceptions import ClientError
import jinja2
from common.aws_utils import s3_split, s3_load_json, sns_get_topic_arn_by_name
from common.event import EventProto
from common.utils import splitstrip
try:
    from urllib import urlopen
except ImportError:
    # noinspection PyCompatibility
    from urllib.request import urlopen

__author__ = 'Murray Andrews'

PLUGIN_NAME = __name__.split('.')[-1]

# ..............................................................................
# region templates
# ..............................................................................

# This is the default text message if the IPRANGER_MSG_TEMPLATE_TXT environment
# var is not set or the file cannot be loaded.
IPRANGER_MSG_TXT = '''
{#- This is a sample message template for ipranger -#}

{%- if regions_of_interest -%}
There have been changes to the AWS IP address ranges in your regions of interest ({{  regions_of_interest }}).
{%- else -%}
There have been changes to the AWS IP address ranges.
{%- endif %}

Previous Version: {{ previous.createDate }} ({{ previous.syncToken }})
Lastest Version:  {{ latest.createDate }} ({{ latest.syncToken }})

{% for region in changes_by_region %}
========================================
{{ region }}
========================================
    {% set changes = changes_by_region[region] %}
    {%- if changes.ip4_added %}
IP4 Additions
        {%- for chg in changes.ip4_added|sort_multi('region', 'service', 'ip_prefix') %}
    + {{ chg.region }} {{ chg.service }} {{ chg.ip_prefix }}
        {%- endfor %}
    {% endif -%}
    {%- if changes.ip4_deleted %}
IP4 Deletions
        {%- for chg in changes.ip4_deleted|sort_multi('region', 'service', 'ip_prefix') %}
    - {{ chg.region }} {{ chg.service }} {{ chg.ip_prefix }}
        {%- endfor %}
    {% endif -%}
    {%- if changes.ip6_added %}
IP6 Additions
        {%- for chg in changes.ip6_added|sort_multi('region', 'service', 'ipv6_prefix') %}
    + {{ chg.region }} {{ chg.service }} {{ chg.ipv6_prefix }}
        {%- endfor %}
    {% endif -%}
    {%- if changes.ip6_deleted %}
IP6 Deletions
        {%- for chg in changes.ip6_deleted|sort_multi('region', 'service', 'ipv6_prefix') %}
    - {{ chg.region }} {{ chg.service }} {{ chg.ipv6_prefix }}
        {%- endfor %}
    {% endif -%}

{% endfor %}
{# #}
Latest information was obtained from {{ url }}.

This message was produced by {{ plugin }}.
'''

# This is the default HTML message if the IPRANGER_MSG_TEMPLATE_HTML environment
# var is not set or the file cannot be loaded.
IPRANGER_MSG_HTML = '''
<html>
<head>
    <title>AWS IP Address Range Changes</title>
    <style type="text/css">
        .standout {
            font-weight: bold;
            color: steelblue;
        }

        .footer {
            color: darkgrey;
            text-align: center;
        }

        h1 {
            font-size: 120%;
            color: steelblue;
        }

        table.change_table {
            border-collapse: collapse;
            border: 1px solid lightgrey;
        }

        td, th {
            padding: 2px 5px;
        }

        td.added {
            border: 1px solid lightgrey;
            border-left-width: 0;
            border-right-width: 0;
            color: steelblue;
        }

        td.deleted {
            border: 1px solid lightgrey;
            border-left-width: 0;
            border-right-width: 0;
            color: indianred;
        }

        th.category {
            background-color: lightgrey;
            text-align: left;
            font-weight: bold;
        }

    </style>
</head>
<body>
<p>
    {% if regions_of_interest %}
        There have been changes to the AWS IP address ranges in your regions of
        interest ({{ regions_of_interest }}).
    {% else %}
        There have been changes to the AWS IP address ranges.
    {% endif %}
</p>
<table class="change_table">
    <tr>
        <td class="standout">Previous Version:</td>
        <td>{{ previous.createDate }}</td>
        <td>({{ previous.syncToken }})</td>
    </tr>
    <tr>
        <td class="standout">Lastest Version:</td>
        <td>{{ latest.createDate }}</td>
        <td>({{ latest.syncToken }})</td>
    </tr>
</table>
{% for region in changes_by_region %}
    {% set changes = changes_by_region[region] %}
    <h1>{{ region }}</h1>
    <div>
        <table class="change_table">
            {%- if changes.ip4_added %}
                <tr>
                    <th colspan="3" class="category">IP4 Additions</th>
                </tr>
                {%- for chg in changes.ip4_added|sort_multi('service', 'ip_prefix') %}
                    <tr>
                        <td class="added">+</td>
                        <td class="added">{{ chg.service }}</td>
                        <td class="added">{{ chg.ip_prefix }}</td>
                    </tr>
                {%- endfor %}
            {% endif -%}
            {%- if changes.ip4_deleted %}
                <tr>
                    <th colspan="3" class="category">IP4 Deletions</th>
                </tr>
                {%- for chg in changes.ip4_deleted|sort_multi('service', 'ip_prefix') %}
                    <tr>
                        <td class="deleted">-</td>
                        <td class="deleted">{{ chg.service }}</td>
                        <td class="deleted">{{ chg.ip_prefix }}</td>
                    </tr>
                {%- endfor %}
            {% endif -%}
            {%- if changes.ip6_added %}
                <tr>
                    <th colspan="3" class="category">IP6 Additions</th>
                </tr>
                {%- for chg in changes.ip6_added|sort_multi('service', 'ipv6_prefix') %}
                    <tr>
                        <td class="added">+</td>
                        <td class="added">{{ chg.service }}</td>
                        <td class="added">{{ chg.ipv6_prefix }}</td>
                    </tr>
                {%- endfor %}
            {% endif -%}
            {%- if changes.ip6_deleted %}
                <tr>
                    <th colspan="3" class="category">IP6 Deletions</th>
                </tr>
                {%- for chg in changes.ip6_deleted|sort_multi('service', 'ipv6_prefix') %}
                    <tr>
                        <td class="deleted">-</td>
                        <td class="deleted">{{ chg.service }}</td>
                        <td class="deleted">{{ chg.ipv6_prefix }}</td>
                    </tr>
                {%- endfor %}
            {% endif -%}
        </table>
    </div>
{% endfor %}
<p> Latest information was obtained from {{ url }}.</p>

<p class="footer">
    This message was produced by IPranger - an
    <a href="https://bitbucket.org/murrayandrews/evw">evw</a> plugin.
</p>
</body>
</html>
'''
# ..............................................................................
# endregion templates
# ..............................................................................

# ------------------------------------------------------------------------------
# Ip*Range types provide hashable versions of each IP range entry to allow set
# operations.

Ip4Range = namedtuple('Ip4Range', ['region', 'service', 'ip_prefix'])
Ip6Range = namedtuple('Ip6Range', ['region', 'service', 'ipv6_prefix'])


# ------------------------------------------------------------------------------
class Event(EventProto):
    """
    Analyse changes to the AWS IP address ranges.

    """

    # --------------------------------------------------------------------------
    def __init__(self, *args, **kwargs):

        super(Event, self).__init__(*args, **kwargs)

        self.sns_topic_arn = os.environ.get(
            'IPRANGER_SNS_TOPIC_ARN',
            sns_get_topic_arn_by_name(PLUGIN_NAME)
        )

        # ----------------------------------------
        # SNS sending preparation
        if not self.sns_topic_arn:
            raise Exception('IPRANGER_SNS_TOPIC_ARN environment variable'
                            ' not set and no queue named {}'.format(PLUGIN_NAME))

        # ----------------------------------------
        # S3 preparation
        try:
            s3_cache = os.environ['IPRANGER_S3_CACHE'].rstrip('/')
        except KeyError:
            raise Exception('IPRANGER_S3_CACHE environment variable not set')

        self.s3_cache_bucket, self.s3_cache_prefix = s3_split(s3_cache)
        self.s3_cache_key = (self.s3_cache_prefix + '/latest.json').lstrip('/')
        self.latest = {}  # Cache the latest IP ranges object keyed on URL

        # ----------------------------------------
        # DynamoDB subscriptions table handle
        dynamo_db = self.aws_session.resource('dynamodb')
        self.subscriptions_table = dynamo_db.Table(PLUGIN_NAME + '.subscriptions')

        # ----------------------------------------
        # Get ready to use AWS SES for email. We force us-east-1 region for SES.
        self.ses_sender = os.environ.get('IPRANGER_SES_SENDER')
        if self.ses_sender:
            self.ses = self.aws_session.client('ses', region_name='us-east-1')
        else:
            self.ses = None
            self.logger.warning('Cannot send email - no SES sender configured.')

    # --------------------------------------------------------------------------
    def process(self):
        """
        Process the event.
        """

        self.logger.debug('IPRANGER %s', str(self.event))

        for record in self.event['Records']:
            if record['EventSource'] != 'aws:sns':
                raise Exception('Unexpected event source: {}'.format(record['EventSource']))

            event_msg = record['Sns']['Message']  # type: dict
            self.logger.info('Change record data %s', event_msg)

            event_data = json.loads(event_msg)
            latest = self.get_latest_ranges_from_aws(event_data['url'])

            try:
                latest_synctoken = int(latest['syncToken'])
            except KeyError:
                raise Exception('Latest file from {} has no syncToken'.format(event_data['url']))
            except ValueError:
                raise Exception('Latest file from {} has bad syncToken'.format(event_data['url']))

            # ----------------------------------------
            # Check the cache for a previous file. If it doesn't exist or doesn't
            # have a syncToken we store this one and finish as no basis for a delta.

            previous = self.read_cache()  # type: dict

            if previous:
                previous_synctoken = int(previous['syncToken'])
                if previous_synctoken >= latest_synctoken:
                    raise Exception(
                        'Latest file from {} (syncToken {}) is not newer than previous (syncToken {})'.format(
                            event_data['url'], latest_synctoken, previous_synctoken)
                    )
                self.post_changes(previous, latest, event_data['url'])
            else:
                self.logger.warning('No previous IP range cache.')

            self.write_cache(latest)

    # --------------------------------------------------------------------------
    def get_latest_ranges_from_aws(self, url):
        """
        Fetch the latest ranges from AWS. Results are cached.
        
        :param url:     The URL to get it from
        :type url:      str
        
        :return:        The latest ranges object
        :rtype:         dict[str-->T]
        """

        if url not in self.latest:
            response = urlopen(url)
            self.latest[url] = json.loads(response.read())

        return self.latest[url]

    # --------------------------------------------------------------------------
    def read_cache(self):
        """
        Read current IP range file from the S3 cache. If there is no existing
        cache file then return None.
        
        :return:            The IP ranges object.
        :rtype:             dict
        """

        try:
            return s3_load_json(
                bucket_name=self.s3_cache_bucket,
                key=self.s3_cache_key,
                aws_session=self.aws_session
            )
        except (IOError, ValueError) as e:
            # File doesn't exist or is bad JSON
            self.logger.warning('Could not load %s/%s: %s',
                                self.s3_cache_bucket, self.s3_cache_key, str(e))
            return None

    # --------------------------------------------------------------------------
    def write_cache(self, ipranges):
        """
        Store the IP range object back to the cache.  This stores the object
        twice - once under a date structure and once under the value
        'latest.json'.
        
        :param ipranges:    The IP ranges object.
        :type ipranges:     dict
        
        """

        bucket = self.aws_session.resource('s3').Bucket(self.s3_cache_bucket)

        # ----------------------------------------
        # Save latest.json
        put_args = {
            'Key': self.s3_cache_key,
            'Body': json.dumps(ipranges, indent=4, sort_keys=True),
            'ContentType': 'application/json',
            'Metadata': {'Creator': PLUGIN_NAME}
        }
        bucket.put_object(**put_args)
        self.logger.info('Wrote %s', put_args['Key'])

        # ----------------------------------------
        # Save as YYYY/<date>.json (Reduced redundancy)
        put_args['Key'] = '{pfx}/{cdate:.4}/{cdate}.json'.format(
            pfx=self.s3_cache_prefix,
            cdate=ipranges['createDate']
        ).lstrip('/')  # lstrip in case prefix is empty
        put_args['StorageClass'] = 'REDUCED_REDUNDANCY'
        bucket.put_object(**put_args)
        self.logger.info('Wrote %s', put_args['Key'])

    # --------------------------------------------------------------------------
    def post_changes(self, previous, latest, url):
        """
        Compare the previous and latest IP ranges objects and generate
        difference sets. Then send a message with summary to an SNS topic.
        
        :param previous:    A previous IP ranges object.
        :param latest:      The latest IP ranges object.
        :param url:         The URL where latest was obtained.
        :type previous:     dict
        :type latest:       dict
        :type url:          str
        """

        # ----------------------------------------
        # Calculate the changes

        previous_ip4 = {Ip4Range(**pfx) for pfx in previous['prefixes']}
        latest_ip4 = {Ip4Range(**pfx) for pfx in latest['prefixes']}
        previous_ip6 = {Ip6Range(**pfx) for pfx in previous['ipv6_prefixes']}
        latest_ip6 = {Ip6Range(**pfx) for pfx in latest['ipv6_prefixes']}

        changes = {
            'ip4_deleted': previous_ip4 - latest_ip4,
            'ip4_added': latest_ip4 - previous_ip4,
            'ip6_deleted': previous_ip6 - latest_ip6,
            'ip6_added': latest_ip6 - previous_ip6
        }

        # ----------------------------------------
        # Slice the changes by region

        regions = set()
        for chg_set in changes.values():
            regions.update({chg.region for chg in chg_set})

        changes_by_region = OrderedDict()
        for r in sorted(regions):  # type: str
            changes_by_region[r] = {}
            for chg_type, chg_set in changes.items():
                # noinspection PyTypeChecker
                changes_by_region[r][chg_type] = {chg for chg in chg_set if chg.region == r}

        # ----------------------------------------
        # Prepare Jinja for template processing

        jenv = jinja2.Environment(autoescape=True)
        jenv.filters['sort_multi'] = j2_sort_multi_attributes
        template_txt = jenv.from_string(self.get_msg_template_txt())
        template_html = jenv.from_string(self.get_msg_template_html())

        # ----------------------------------------
        # Process each subscription

        for subscription in self.subscriptions():
            # Extract regions that match the glob style subscription patterns
            # 'GLOBAL' is a special case and is always added first.
            sb_changes_by_region = OrderedDict()
            if 'GLOBAL' in changes_by_region:
                sb_changes_by_region['GLOBAL'] = changes_by_region['GLOBAL']

            regions_of_interest = []
            for r in changes_by_region:
                if r == 'GLOBAL':
                    # Everyone gets GLOBAL
                    continue
                regions_of_interest = splitstrip(subscription.get('regions', '*'), sep=',')
                for region_glob in regions_of_interest:
                    if fnmatch(r, region_glob):
                        # This region is matched by one of the glob patterns
                        sb_changes_by_region[r] = changes_by_region[r]
                        break

            if not sb_changes_by_region:
                # Nothing to report for this subscription
                self.logger.info('No address space changes to report for %s',
                                 subscription['email'])
                continue

            # Render the template(s) for this subscription
            summary_txt = template_txt.render(
                previous=previous,
                latest=latest,
                changes=changes,  # All changes not split by region
                changes_by_region=sb_changes_by_region,
                plugin=PLUGIN_NAME,
                url=url,
                regions_of_interest=', '.join(sorted(regions_of_interest))
            )
            summary_html = template_html.render(
                previous=previous,
                latest=latest,
                changes=changes,  # All changes not split by region
                changes_by_region=sb_changes_by_region,
                plugin=PLUGIN_NAME,
                url=url,
                regions_of_interest=', '.join(sorted(regions_of_interest))
            )

            try:
                self.send_email(
                    to=subscription['email'],
                    subject='AWS IP address ranges have changed',
                    txtbody=summary_txt,
                    htmlbody=summary_html
                )
            except Exception as e:
                self.logger.error('Could not send email to %s: %s',
                                  subscription['email'], str(e))
            else:
                self.logger.info('Email sent to %s', subscription['email'])

        # ----------------------------------------
        # Produce a text summary of all changes and send it to SNS.
        summary_txt = template_txt.render(
            previous=previous,
            latest=latest,
            changes=changes,
            changes_by_region=changes_by_region,
            plugin=PLUGIN_NAME,
            url=url,
        )
        self.send_msg('sns://' + self.sns_topic_arn, 'AWS IP address ranges have changed', summary_txt)

    # ------------------------------------------------------------------------------
    def get_msg_template_txt(self):
        """
        Get the message text that wll be rendered with IP range changes. If the
        IPRANGER_MSG_TEMPLATE_TXT is set it is assumed to point to an S3 location
        containing the message text otherwise an internal default is used.
        
        :return:        The message text.
        :rtype:         str
        """

        msg_template = os.environ.get('IPRANGER_MSG_TEMPLATE_TXT')
        if not msg_template:
            self.logger.info('Using internal message text')
            return IPRANGER_MSG_TXT

        try:
            bucket, key = s3_split(msg_template)
            return self.aws_session.resource('s3').Object(bucket, key).get()['Body'].read()
        except Exception as e:
            self.logger.error('%s: %s', os.environ['IPRANGER_MSG_TEMPLATE_TXT'], str(e))
            self.logger.warning('Using internal message text')
            return IPRANGER_MSG_TXT

    # ------------------------------------------------------------------------------
    def get_msg_template_html(self):
        """
        Get the message HTML that wll be rendered with IP range changes. If the
        IPRANGER_MSG_TEMPLATE_HTML is set it is assumed to point to an S3 location
        containing the message HTML otherwise an internal default is used.
        
        :return:        The message text.
        :rtype:         str
        """

        msg_template = os.environ.get('IPRANGER_MSG_TEMPLATE_HTML')
        if not msg_template:
            self.logger.info('Using internal message text')
            return IPRANGER_MSG_HTML

        try:
            bucket, key = s3_split(msg_template)
            return self.aws_session.resource('s3').Object(bucket, key).get()['Body'].read()
        except Exception as e:
            self.logger.error('%s: %s', os.environ['IPRANGER_MSG_TEMPLATE_HTML'], str(e))
            self.logger.warning('Using internal message html')
            return IPRANGER_MSG_HTML

    # ------------------------------------------------------------------------------
    def subscriptions(self):
        """
        A generator returning subscriptions from the subscriptions table. If the
        table doesn't exist then there will be an empty sequence of subscriptions.
        
        """

        try:
            self.subscriptions_table.load()
        except ClientError:
            # Table doesn't exist
            self.logger.warning('DynamoDB table %s table does not exist.',
                                self.subscriptions_table.table_name)
            return

        start_key = None

        while True:
            self.logger.debug('Fetch batch from %s', self.subscriptions_table.table_name)
            scan_params = {'Limit': 100}
            if start_key:
                scan_params['ExclusiveStartKey'] = start_key
            result = self.subscriptions_table.scan(**scan_params)  # type: dict
            for subscription in result['Items']:
                if subscription.get('disabled'):
                    self.logger.info('Sending disabled for %s', subscription['email'])
                else:
                    yield subscription
            start_key = result.get('LastEvaluatedKey')
            if not start_key:
                break

    # ------------------------------------------------------------------------------
    def send_email(self, to, subject, txtbody, htmlbody):
        """
        Send an email via SES. Note that SES is not available in all regions so we
        force us-east-1.
        
        We don't use the messenger subsystem of evw because its mailer cannot
        handle HTML bodies.

        :param to:          Destination email address. Must be SES verified email.
        :param subject:     Message subject.
        :param txtbody:     Plain text message body.
        :param htmlbody:    HTML message body.
        
        :type to:           str
        :type subject:      str
        :type txtbody:      str
        :type htmlbody:     str
       
        :raise Exception:   If there are no verified email domains.
        
        """

        if not self.ses:
            return

        self.ses.send_email(
            Source=self.ses_sender,
            Destination={'ToAddresses': [to]},
            Message={
                'Subject': {'Data': subject, 'Charset': 'UTF-8'},
                'Body': {
                    'Text': {'Data': txtbody, 'Charset': 'UTF-8'},
                    'Html': {'Data': htmlbody, 'Charset': 'UTF-8'}
                }
            }
        )


# ------------------------------------------------------------------------------
def j2_sort_multi_attributes(l, *operators):
    """
    Sort by multiple attributes in Jinja template.

    See: http://stackoverflow.com/questions/16143053/stable-sorting-in-jinja2

    Use in template:

        {{item_list|sort_multi('attr1','attr2')}}


    :param l:           The list to sort.
    :param operators:
    :type l:            list
    :return:            A sorted copy of the list.
    """

    l2 = list(l)
    l2.sort(key=operator.attrgetter(*operators))
    return l2

# Evw Event Handler Plugin - mbot

## Overview

The **mbot** event handler plugin provides a general purpose messaging subsystem
for the AWS environment.  It allows messages to be sent to one or more named
destinations via a variety of pluggable mechanisms such as email, SMS and push
messaging.

Installation of **mbot** is described
[below](#markdown-header-installation-and-configuration).

![](../misc/mbot/mbot-1.png)


The sending process consists of 6 primary activities:

1. [Message Submission](#markdown-header-message-submission)

    A message is submitted to the **mbot** system with one or more recipient
    user or group IDs specified. Various message formats are supported.

    Optionally, messaging mechanisms (*modes*) may also be specified to
    constrain how the message may be sent (e.g. email, SMS etc).

2.  [Group Expansion](#markdown-header-group-expansion)


    Group IDs specified in the message `To` attribute are (recursively)
    expanded via the [groups table](#markdown-header-the-groups-table) into
    their constituent user IDs to generate a target user ID list.

    User IDs are arbitrary. One possible option is to use IAM user identifiers.

3.  [Mode Expansion](#markdown-header-mode-expansion)

    Values specified in the message `Mode` attribute are expanded to a list of
    sending modes via the [modes table](#markdown-header-the-modes-table).  This
    allows a mode alias to be expanded into a list of real modes that will be
    attempted for each target user.

4.  [User Lookup](#markdown-header-user-lookup)

    The target user IDs are looked up in the [users
    table](#markdown-header-the-users-table) to obtain a user info record for
    each user. This record contains descriptive information (e.g. user name) and
    the sending modes and corresponding addresses available for that user.

5.  [Message Rendering](#markdown-header-message-rendering)

    The message `Subject` and `Body` attributes are subject to per-user
    parameter substitution to allow some limited personalisation of outgoing
    messages.

6.  [Message Transmission](#markdown-header-message-transmission)

    The message is sent to each target user via the first available mode
    permitted for the message and supported by the target user. **Mbot** will
    continue to try alternate allowed modes in the event of failure.

    The [messengers table](#markdown-header-the-messengers-table) maps
    mode names (e.g. `email`) to an underlying messenger plugin that can
    handle that mode (e.g. `ses` for AWS Simple Email Service).

## Message Submission

Messages can be submitted to **mbot** via
[SNS](#markedown-header-message-submission-via-sns).

Other mechanisms may be supported in future (e.g. file drop in S3).

### Message Submission via SNS

**Mbot** will receive message submissions on the SNS topic `mbot`. For example,
if the message is contained in the file `msg.txt`, the following commands will
get the topic ARN and send the message to it:

```bash
# Get the topic ARN
MBOT_TOPIC_ARN=`aws sns list-topics --query "Topics[?ends_with(TopicArn,':mbot')]" --output text`
# Send the message
aws sns publish --topic $MBOT_TOPIC_ARN --message file://msg.txt
```

### Message Formats

Two message formats are supported:
[text](#markdown-header-text-formatted-messages) and
[JSON](#markdown-header-json-formatted-messages).

#### Text Formatted Messages

A text formatted message contains a set of header lines in (mostly) arbitrary
order, followed by a blank line and then the message body.

Each header line consists of the header name followed by a colon, optional
whitespace and the header value.

The following header fields are available:

|Name|Required|Description|
|-|-|-|
|To|Yes|A comma separated list of **mbot** user / group IDs. If multiple `To` lines are provided they will be aggregated into a single target list.|
|Subject|Yes|Message subject.|
|Mode|No|A comma separated list of allowed message sending modes. If multiple `Mode` lines are provided, they will be aggregated into a single mode list in the same order. If not specified a default list of modes is used.|
|X-Mbot-\*|No|Any header field starting with `X-Mbot-` will be silently discarded as these are reserved for internal use.|
|X-\*|No|Any other header field starting with `X-` is ignored in the sending process but will be included in message logs. These headers are also available for [parameter substitution](#markdown-header-message-rendering) in the message subject and body.|


Here is a sample message. The `{{param}}` substitution syntax is explained in
the section on [message rendering](#markdown-header-message-rendering).

```Jinja
To: harry.lime, holly.martins
Subject: Hello {{user.givenName}}
X-Sender: This-is-me

Dear {{user.givenName}}

Your fullname is {{user.fullName}}.
Your family name is {{user.familyName}}.

Regards
    Calloway
```

#### JSON Formatted Messages

If a message starts with a left curly bracket `{`, **mbot** will assume the
message is a JSON formatted object. The following elements are supported:

|Name|Required|Type|Description|
|-|-|-|-|
|To|Yes|string or list(string)|An **mbot** user / group ID or a list of IDs.|
|Subject|Yes|string|Message subject.|
|Body|Yes|String|Message body|
|Mode|No|string or list(string)|A message sending mode or a list of modes. If not specified a default list of modes is used.|
|X-Mbot-\*|No|any|Any header field starting with `X-Mbot-` will be silently discarded as these are reserved for internal use.|
|X-\*|No|any|Any other header field starting with `X-` is ignored in the sending process but will be included in message logs. These headers are also available for [parameter substitution](#markdown-header-message-rendering) in the message subject and body.|


## Group Expansion

The `To` attribute of an **mbot** message is a list of destination group IDs or
user IDs.


Group IDs are expanded via the [groups table](#markdown-header-the-groups-table)
into their constituent user IDs to generate a target user ID list.

**Mbot** will recursively expand group IDs until it reaches a final list
containing only user IDs. Any circularity in a group list will result in that
group being ignored. **Mbot** will not send a message to the same user ID twice,
even if it occurs in multiple groups.

If the groups table does not exist, **mbot** will skip the group expansion phase
and assume all destinations are user IDs.


### The Groups Table

The **groups table** is an optional DynamoDB table named `mbot.groups`. It maps
group IDs to a list of IDs, each of which can be another group ID or a user ID.

The table is created by the CloudFormation template provided.
It contains the following fields.

|Name|Required|Type|Description|
|-|-|-|-|
|groupId|Yes|string|A string identifier for the group. It must be lower case and must not contain commas. This is the primary partition key for the table.|
|value|Yes|string|A comma separated list of IDs, each of which may be a user ID or another group ID.|
|disabled|No|boolean|If `true`,  sending to this group is disabled. This will not prevent sending to users in this group if they occur elsewhere in the target user list for a message.|


## Mode Expansion

The `Modes` attribute of a message is a list of one or more sending modes that
are permitted for a message.  If a `Modes` attribute is not specified when the
message is submitted, **mbot** will use the value `default`.

Just as user groups are expanded into individual user IDs via the groups table,
the modes for a message are expanded via the
[modes table](#markdown-header-the-modes-table).
Only a single round of expansion is done (no recursion). The result is a final
list of base modes that are permitted for the message.

The purpose of this process is to allow site-specific control of which modes are
used for various types of message. For example, the `default` mode can be
redefined, modes can be redirected to other modes or convenience labels can be
defined for sequences of base modes.

If the table does not exist or a mode is not present in the table, **mbot** will
use its small list of internal mode expansions. These are:

```make
default: email, pushover, sms
fast: pushover, sms, email
```

Any mode not present in the table or the internal expansion list is assumed to
be a real base mode. In the message transmission phase, these base modes will be
used to find a messenger plugin to handle transmission.


### The Modes Table

The **modes table** is an optional DynamoDB table named `mbot.modes`. It maps
modes names to a list of base mode names.

The table is created by the CloudFormation template provided.
It contains the following fields.

|Name|Required|Type|Description|
|-|-|-|-|
|mode|Yes|string|The mode name. It must be lower case and alphanumeric.|
|value|Yes|string|A comma separated list of base mode names.|

## User Lookup

In order to send a message to a user ID via a sending mode, **mbot** must be
able to obtain the target address specific to the user for the given mode.
For email this will be an email address. For SMS it will be a
numeric MSISDN. Other modes are likely to have their own address formats.

The [users table](#markdown-header-the-users-table) provides this translation.
It also contains other arbitrary information about the user. The user info
record from the users table is provided to the
[message rendering](#markdown-header-message-rendering) process.

### The Users Table

The **users table** is a DynamoDB table named `mbot.users`. It maps **mbot**
user IDs to user information and messaging mode addresses (e.g. email
addresses).

As the user table is stored in DynamoDB, it is possible to add arbitrary
attributes for any and all users as well as target address for any or all
sending modes.

The table is created by the CloudFormation template provided.  It contains the
following fields.  Those marked with a * represent target addresses for various
messaging modes. The user may have one or more of these.

|Name|Required|Type|Description|
|-|-|-|-|
|userId|Yes|string|A string identifier for the user. It must be lower case and must not contain commas. This is the primary partition key for the table.|
|givenName|Recommended|string|The user's given name.|
|familyName|Recommended|string|The user's family name.|
|fullName|No|string|The user's full name. If not specified it will be formed by joining the givenName and familyName.|
|disabled|No|boolean|If `true`, all sending to this user is disabled.|
|email*|No|string|An email address.|
|pushover*|No|string|The [Pushover](https://pushover.net) system provides push messaging to iOS, Android and desktops. The value of the field is the user's pushover key.|
|sms*|No|string|A full MSISDN for SMS messaging. It must be entirely numeric and include the country prefix.|

All of these fields can be interpolated into the message text prior to
transmission to allow some limited personalisation of messages.

## Message Rendering


The message subject and body are subject to per-user parameter substitution
prior to transmission. This is done with [Jinja2](http://jinja.pocoo.org) so the
full power of the Jinja templating language is available.

The following objects are made available for use in messages.

*   `user`: A dictionary holding the contents of the user record from the
    [users table](#markdown-header-the-users-table).

    e.g. `{{user.fullName}}` would insert the value of the user's full name.

    Because of the schema-less nature of DynamoDB, you can add whatever
    additional attributes you like to the users table for parameter substitution
    purposes.

*   `mbot`: A dictionary holding a set of internally generated attributes.
    These also correspond to `X-Mbot-*` headers recorded with the message
    when it is [logged to S3](#markdown-header-message-logging).

*   `x`: A dictionary holding all of the `X-*` headers (excluding `X-Mbot-*`)
    supplied as part of the submitted message. The leading `X-` and all
    non-alphanumeric characters are removed from the header name when
    populating the `x` dictionary for message rendering.

    For example, if the message contained the following header:

    ```
    X-My-Header: Hello world
    ```

    ... then `{{x.MyHeader}}` in the message would be replaced with `Hello world`.

The following elements are present in the `mbot` dictionary.

|Name|Description|
|-|-|
|EventId|A unique identifier relating to the message submission event.|
|MessageId|A unique identifier relating to the submitted message. This is generally more useful than the event ID. All outbound messages based on the one submitted message have the same MessageId|
|Mode|The sending mode used.|
|SentTo|The actual mode address used for this user + mode.|
|Timestamp|An ISO 8601 formatted timestamp for the message.|
|UserId|The user ID to which the message is being sent.|


## Message Transmission

Once **mbot** has a final list of target user IDs and a list of permitted
sending modes, it will attempt to transmit the message to each target user
ID via each permitted mode in order,
until either it appears to have succeeded or it has exhausted the list
of permitted modes. Obviously, modes that are not defined for a given
user ID will be skipped for that user.

A given mode will never be tried twice for the same message / user ID
and a message will not be sent to a given user ID twice.

[Message rendering](#markdown-header-message-rendering) is performed for each
user ID/mode combination prior to transmission.

### Modes and Messengers

In order to transmit a message via a given mode, **mbot** must have an
appropriate handler for that mode. **Mbot** relies on the
[evw messenger plugins](../messengers/README.md) for this purpose.
Hence any of the **evw** messenger plugins can be used.

The [messengers table](#markdown-header-the-messengers-table) maps mode names
(e.g. `email`) to an underlying messenger plugin that can handle that mode (e.g.
`ses` for AWS Simple Email Service).

A given mode may be implemented by more than messenger plugin. For example,
different email services may use different messenger plugins however **mbot**
must be configured to choose one messenger plugin for each mode. Different modes
may have different address structures (e.g. email addresses are structurally
different from SMS numbers) but all messengers for a given mode must support the
address structure(s) for that mode.

The `disabled` messenger plugin is a special case. It is designed to accept
any address format and to always
fail. By mapping a mode to the `disabled` messenger plugin, a mode can be
disabled.

### The Messengers Table

The **messengers table** is an optional DynamoDB table named `mbot.messengers`.
It maps modes names to the name of messenger plugins capable of handling message
transmission for that mode.

The table is created by the CloudFormation template provided.
It contains the following fields.

|Name|Required|Type|Description|
|-|-|-|-|
|mode|Yes|string|A message sending mode (e.g. `email`). Must be lower case. This is the partition key.|
|messenger|Yes|string|The name of an [evw messenger plugin](../messengers/README.md). Must be lower case.|

If the table does not exist, **mbot** has a short internal list it uses:

```make
 email: ses     # Default messenger for email is AWS SES
 sms: smssns    # Default messenger for SMS is AWS SNS
```

If a mode is not listed in the messengers
table, it is assumed that the mode and its messenger plugin have the same name.
This is the case for the _pushover_ mode, for example.

## Message Logging

If the environment variable `MBOT_LOG` is specified, every message transmitted
to a user is logged in the specified location in S3. A date based structure is
created with the message ID and target user ID used in the final components of
the filename.

Note that all date/times in **mbot** are UTC.

The message is logged in a JSON format that is compatible with the **mbot** JSON
input format.

## Installation and Configuration

While **mbot** can be installed in the same way as other **evw** modules,
it requires a number of AWS components to be present to allow it to work.
These are:

-   An SNS topic with appropriate triggers to send events to the **mbot** Lambda
    function.
-   The various DynamoDB tables.
-   An IAM role for the Lambda function itself.
-   IAM policy/group to allow users to initiate messages.
-   The Lambda function itself.

All of this is automated with a CloudFormation template:
`misc/mbot/mbot.cfn.json`.  Documentation for the can be found
[here](misc/mbot/mbot.cfn.md).

Using this template, the process is:

1.  Clone the **evw** repository.

2.  Run the following command to generate the Lambda install bundle.

    ```bash
    etc/pkg-evlambda.sh mbot
    ```

    This will produce a file `evl-mbot.zip`.

3.  Load `evl-mbot.zip` into an S3 bucket somewhere. You will need the
    location a bit later.

4.  Create a new CloudFormation stack using the template
    `misc/mbot/mbot.cfn.json`.  You will be asked to populate a number of
    parameters, described below.

### CloudFormation Template Parameters

|Parameter|Required|Description|
|-|-|-|
|DynamoDbReadCapacity|Yes|Read capacity units for the DynamoDB tables. The default is 3, which is quite low. This will minimise costs but may need to be increased if a lot of messages are sent.|
|ModuleName|Yes|The Lambda module name. This also controls a number of other resource names. Best to leave it set to the default of `mbot`.|
|PushoverAppKey|No|[Pushover](http://pushover.net) API Token/Key for mbot. If not specified, the Pushover plugin is disabled.|
|S3CodeBucket|Yes|The bucket in which the  `evl-mbot.zip` code bundle was placed. This must exist and must contain the code bundle prior to creating the stack.|
|S3CodeKey|Yes|The full path name of the code bundle in the `S3CodeBucket` (no leading `/`).|
|S3LogBucket|No|S3 bucket for message logging.|
|S3LogKmsKey|No|A KMS key name to encrypt logged messages. If not specified AES256 is used.| |
|S3LogPrefix|No|Prefix within the log bucket for message logs (no trailing `/`).|

### Environment Variables

In addition to the [standard environment
variables](../README.md#markdown-header-lambda-environment-variables) supported
by all **evw** handlers, **mbot** supports the following additional variables.

All of these variables are initially set by the CloudFormation template when
**mbot** is installed so you should not need to fiddle with them to get started.
However they can be changed as required by either updating the CloudFormation
stack (preferred) or directly in the Lambda console.

|Name|Description|Default|
|-|-|-|
|MBOT_USERS_TABLE|Name of the [users table](#markdown-header-the-users-table).|mbot.users|
|MBOT_GROUPS_TABLE|Name of the [groups table](#markdown-header-the-groups-table).|mbot.groups|
|MBOT_MODES_TABLE|Name of the [modes table](#markdown-header-the-modes-table).|mbot.modes|
|MBOT_MESSENGERS_TABLE|Name of the [messengers table](#markdown-header-the-messengers-table).|mbot.messengers|
|MBOT_LOG|The S3 prefix for message logging. It must be of the form `bucket/prefix` or `s3://bucket/prefix`. If not specified, messages are not logged.| |
|MBOT_LOG_KEY|A KMS key name to encrypt logged messages. If not specified AES256 is used.| |

In some cases, the default value is derived from the name given to the **mbot**
event handler (which by default is `mbot`).

Note that some messenger plugins (e.g. pushover) require environment variables
of their own to be set.

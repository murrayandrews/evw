"""
evlambda/evworker plugin to provide a basic messaging subsystem in an AWS
environment. Essentially it just puts a wrapper around the evw messenger
subsystem (which is a bit basic).

................................................................................
Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
................................................................................

"""

# TODO: Add cloudformation setup

from __future__ import print_function

import json
import os

import dateutil.parser
import jinja2

from common.aws_utils import s3_split
from common.event import EventProto
from botocore.client import Config
from botocore.exceptions import ClientError
from common.utils import alphanum, splitstrip
from six import string_types

__author__ = 'Murray Andrews'

PLUGIN_NAME = __name__.split('.')[-1]
MBOT_USERS_TABLE = os.environ.get('MBOT_USERS_TABLE', PLUGIN_NAME + '.users')
MBOT_GROUPS_TABLE = os.environ.get('MBOT_GROUPS_TABLE', PLUGIN_NAME + '.groups')
MBOT_MODES_TABLE = os.environ.get('MBOT_MODES_TABLE', PLUGIN_NAME + '.modes')
MBOT_MESSENGERS_TABLE = os.environ.get('MBOT_MESSENGERS_TABLE', PLUGIN_NAME + '.messengers')

# The environment var MBOT_LOG specifies a bucket name and prefix for
# logging sent messages. Messages are logged under a YYYY/MM/DD substructure.

MBOT_LOG = os.environ.get('MBOT_LOG')
if MBOT_LOG:
    MBOT_LOG_BUCKET, MBOT_LOG_PREFIX = s3_split(MBOT_LOG)

# The environment var MBOT_LOG_KEY specifies a KMS key name to encrypt log files.
# If not specified, AES256 will be used.

MBOT_LOG_KEY = os.environ.get('MBOT_LOG_KEY')

MSG_HEADERS = {'To', 'Subject', 'Mode', 'Body'}
MSG_MODES_DEFAULT = 'default'

# Map mode aliases to base modes. Overridden by the DynamoDB modes table if
# present. Keys and values must be lower case. Values must be lists.
MSG_MODES = {
    MSG_MODES_DEFAULT: ['email', 'pushover', 'sms'],
    'fast': ['pushover', 'sms', 'email'],
}

# Map modes to messenger plugins that provide the service. If no entry then
# assume mode and plugin have the same name. Overridden by the DynamoDB
# messengers table.
MSG_MESSENGERS = {
    'email': 'ses',  # Default messenger for email is AWS SES
    'sms': 'smssns',  # Default messenger for SMS is AWS SNS
}


# ------------------------------------------------------------------------------
class Event(EventProto):
    """
    Send a message.

    """

    # --------------------------------------------------------------------------
    def __init__(self, *args, **kwargs):

        super(Event, self).__init__(*args, **kwargs)

        dynamo_db = self.aws_session.resource('dynamodb')
        self.user_table = dynamo_db.Table(MBOT_USERS_TABLE)
        self.group_table = dynamo_db.Table(MBOT_GROUPS_TABLE)
        self.mode_table = dynamo_db.Table(MBOT_MODES_TABLE)
        self.messenger_table = dynamo_db.Table(MBOT_MESSENGERS_TABLE)
        self.messengers = {}  # Maps modes to messenger plugin name. Built progressively.
        self.groups = {}  # Keeps track of expanded groups

    # --------------------------------------------------------------------------
    def process(self):
        """
        Process the event.
        """

        for record in self.event['Records']:
            if record['EventSource'] == 'aws:sns':
                msg = record['Sns']['Message']  # type: str
                msg_timestamp = record['Sns']['Timestamp']
                msg_id = record['Sns']['MessageId']
            else:
                raise Exception('Unexpected event source: {}'.format(record['EventSource']))

            # Handle different message formats
            if msg.startswith('{'):
                msg = self.parse_json_msg(msg)  # type: dict
            else:
                msg = self.parse_txt_msg(msg)  # type: dict

            self.check_msg(msg)  # This removes all user supplied X-Mbot headers
            msg['X-Mbot-EventId'] = self.event_id
            msg['X-Mbot-MessageId'] = msg_id
            msg['X-Mbot-Timestamp'] = msg_timestamp
            self.send(msg)

    # --------------------------------------------------------------------------
    @staticmethod
    def parse_txt_msg(msg_txt):
        """
        Parse a simple text formatted message. This looks like:
            To: recipient1, recipient2
            Subject: a subject
            Mode: email, sms

            Body text follows a blank line.

        Multiple To: and Mode: lines are allowed. Ordering of header lines is
        mostly arbitrary. For header lines that don't aggregate (e.g. Subject),
        the last occurence is the one that counts.

        Any header lines starting X- are ignored as well.

        :param msg_txt:     The message, including control info.
        :type msg_txt:      str

        :return:        The parsed message object.
        :rtype:         dict[str-->str]
        """

        msg = {
            'To': [],
            'Body': [],
            'Mode': []
        }
        header = True

        for line in msg_txt.splitlines():
            line = line.strip()

            if header:
                if not line:
                    # Blank line ends the header region
                    header = False
                    continue

                field = splitstrip(line, ':', 1)
                if len(field) < 2:
                    raise Exception('Bad header line: {}'.format(line))

                f = field[0].lower()

                # ------------------------------
                # X- lines are stored but not used.
                if field[0].startswith('X-'):
                    msg[field[0]] = field[1]
                    continue

                # ------------------------------
                # To lines are aggregated.
                if f == 'to':
                    msg['To'].extend(splitstrip(field[1], ','))
                    continue

                # ------------------------------
                # Mode lines are aggregated.
                if f == 'mode':
                    msg['Mode'].extend(splitstrip(field[1], ','))
                    continue

                # ------------------------------
                # All other headers are not aggregated -- last value rules.
                msg[field[0].capitalize()] = field[1]

            else:
                # Message body
                msg['Body'].append(line)

        msg['Body'] = '\n'.join(msg['Body'])
        return msg

    # --------------------------------------------------------------------------
    @staticmethod
    def parse_json_msg(msg_txt):
        """
        Parse a JSON formatted message. This looks like:
            {
                "To": ["recipient1", "recipient2"],
                "Subject": "a subject",
                "Body": "Body text"
            }

            The "To" attribute can be a string or a list but will always be a
            list in the returned value.

        :param msg_txt:     The message formatted as a JSON string.
        :type msg_txt:      str

        :return:        The parsed message object.
        :rtype:         dict[str-->str]
        """

        try:
            msg = json.loads(msg_txt)
        except Exception as e:
            raise Exception('Cannot decode JSON message: {}'.format(e))

        return msg

    # --------------------------------------------------------------------------
    @staticmethod
    def check_msg(msg):
        """
        Validate message structure. Ignore X- elements. Delete any X-Mbot items
        as the user is not allowed to add those.

        :param msg:         The message object.
        :type msg:          dict

        :raise ValueError:  If the message is malformed.
        """

        # ----------------------------------------
        # Supply defaults for optional fields
        msg.setdefault('Subject', None)
        if not msg.get('Mode'):
            msg['Mode'] = [MSG_MODES_DEFAULT]

        # ----------------------------------------
        # Remove user supplied X-Mbot headers - these are for internal use only.
        for h in [h for h in msg if h.startswith('X-Mbot')]:
            del msg[h]

        # ----------------------------------------
        # Only examine those headers that don't start with X-

        hdrs = {h for h in msg if not h.startswith('X-')}
        extras = hdrs - MSG_HEADERS
        if extras:
            raise ValueError('Unexpected message header(s): {}'.format(', '.join(sorted(extras))))

        missing = MSG_HEADERS - hdrs
        if missing:
            raise ValueError('Missing message header(s): {}'.format(', '.join(sorted(missing))))

        empty = [h for h in msg if not msg[h]]
        if empty:
            raise ValueError('Missing value for header(s): {}'.format(', '.join(sorted(empty))))

        # ----------------------------------------
        # To and Mode values must be lists
        for hdr in 'To', 'Mode':
            if not isinstance(msg[hdr], list):
                if not isinstance(msg[hdr], string_types):
                    raise Exception('Value of header {} must be string or list of strings'.format(hdr))
                msg[hdr] = [msg[hdr]]

    # --------------------------------------------------------------------------
    def send(self, msg):
        """
        Send the message

        :param msg:         The message object.
        :type msg:          dict
        """

        # ----------------------------------------
        # Need to expand the modes list to get to a base set of modes.
        # i.e. any mode aliaes need to be replaced.

        msg_modes = self.resolve_modes(msg['Mode'])
        msg_id = msg['X-Mbot-MessageId']
        already_sent = set()

        for to in msg['To']:

            # ----------------------------------------
            try:
                users = self.expand_group(to)
                self.logger.debug('Message %s: Expand %s --> %s',
                                  msg_id, to, ', '.join(users))
            except Exception as e:
                self.logger.warning('Message %s: Cannot send to %s: %s',
                                    msg_id, to, str(e))
                continue

            for uid in users:
                if uid in already_sent:
                    continue
                try:
                    mode, address = self.send_to_user(uid, msg, msg_modes)
                except Exception as e:
                    self.logger.warning('Message %s: Cannot send to user %s: %s',
                                        msg_id, uid, str(e))
                else:
                    self.logger.info('Message %s: Message sent to %s via %s:%s',
                                     msg_id, uid, mode, address)
                finally:
                    # Never try the same user twice
                    already_sent.add(uid)

    # --------------------------------------------------------------------------
    def send_to_user(self, uid, msg, modes):
        """
        Send a message to a given uid by the first available mode. Makes no
        attempt to prevent a user getting the same message twice.
        
        :param uid:         The user ID to which the message is sent.
        :param msg:         The message object.
        :param modes:       A list of sending modes. These will be tried in order
                            until one appears to succeed.
                            
        :type uid:          str
        :type msg:          dict
        :type modes:        list[str]
        
        :return:            A tuple: The (mode, address) actually used.
        :rtype:             tuple(str)
        
        :raise Exception:   If the message could not be sent for any reason.
        
        """

        uid = uid.lower()

        # ----------------------------------------
        # Get user details and contact addresses from DynamoDB.
        try:
            user_info = self.get_user(uid)
        except KeyError:
            raise Exception('Unknown user')

        # ----------------------------------------
        # User exists. Work out how to send.

        # First check sending is enabled for this user
        if user_info.get('disabled'):
            raise Exception('Sending disabled for user')

        for mode in modes:
            address = user_info.get(mode)

            if not address:
                # User doesn't support this mode
                continue

            # Have sending mode. Render message subject & body with user info.
            # We also supply any X- headers and X-Mbot headers as dicts x and mbot.

            lmsg = dict(msg)
            lmsg.update({
                'X-Mbot-Sent-To': address,
                'X-Mbot-Mode': mode,
                'X-Mbot-UserId': uid,
            })
            # Extract the X-Mbot- headers for rendering in the message
            xmbot = {alphanum(k[7:]): lmsg[k] for k in lmsg if k.startswith('X-Mbot-')}

            # Extract the X- headers for rendering in the message
            x = {
                alphanum(k[2:]): lmsg[k] for k in lmsg
                if k.startswith('X-') and not k.startswith('X-Mbot-')
            }

            lmsg['Body'] = jinja2.Template(msg['Body']).render(
                user=user_info, mbot=xmbot, x=x)
            lmsg['Subject'] = (jinja2.Template(msg['Subject']).render(
                user=user_info, mbot=xmbot, x=x))

            try:
                messenger_name = self.get_messenger_name(mode)
                self.send_msg(
                    to='{}://{}'.format(messenger_name, address),
                    subject=lmsg['Subject'],
                    message=lmsg['Body']
                )
            except Exception as e:
                # Send failed. Log it and continue to see if another mode might work.
                self.logger.warning('Message %s: Could not send to %s via %s:%s: %s',
                                    msg['X-Mbot-MessageId'], uid, mode, address, str(e))
                continue

            # ----------------------------------------
            # Store a copy of the message in S3.
            if MBOT_LOG:
                lmsg['X-Mbot-Handler'] = messenger_name
                self.s3_log_msg(lmsg)

            return mode, address

        # ----------------------------------------
        raise Exception('No available mode')

    # --------------------------------------------------------------------------
    def resolve_modes(self, modes):
        """
        Resolve a list of messaging modes into a canonical list of base modes.
        Basically this involves a single round of expansion of mode aliases via
        the mbot.modes table. There is also an internal list which is applied
        where there is no entry in the mbot.modes table. The same base mode will
        not occur in the output list twice.

        :param modes:       A list of modes. Any of these that occur in the
                            DynamoDB mode table will be replaced by their
                            value which is a comma separated list of base modes.
        :type modes:        list[str]
        :return:            A resolved list of modes.
        :rtype:             list[str]
        """

        resolved_modes = []

        for m in (m.lower() for m in modes):

            m_resolved = []

            # ------------------------------
            # First try the DynamoDB mode table

            if self.mode_table:
                try:
                    mode_tab_entry = self.mode_table.get_item(Key={'mode': m})['Item']
                    m_resolved = splitstrip(mode_tab_entry['value'], ',')
                except ClientError:
                    # The table does not exist. Prevent further table lookups
                    self.logger.debug('DynamoDB table %s table does not exist.', self.mode_table.table_name)
                    self.mode_table = None
                except KeyError:
                    # Not present in the table. Move on.
                    pass

            # ------------------------------
            # Next try the internal default table.

            if not m_resolved:
                m_resolved = MSG_MODES.get(m, [])

            # ------------------------------
            # Lastly just use the input mode as the resolved mode
            if not m_resolved:
                m_resolved = [m]

            # ------------------------------
            # Extend the list of resolved modes - no duplicates
            for s in m_resolved:
                if s not in resolved_modes:
                    resolved_modes.append(s)

        return resolved_modes

    # --------------------------------------------------------------------------
    def expand_group(self, gid, visited=None):
        """
        Look up the given group ID in the group DynamoDB table.  Recurses as
        required but with cicular reference protection.
        
        :param gid:         The group ID.
        :param visited:     A set of gids already visited. If gid is in this
                            list then we have circularity.
        :return:            A list of user IDs (which may be empty in some cases).
                            this ID.
        :rtype:             list[str]
        
        :raise Exception:   If there are circular references.
        
        """

        gid = gid.lower()

        if not self.group_table:
            self.groups[gid] = [gid]

        if gid in self.groups:
            return self.groups[gid]

        # ----------------------------------------
        # Make sure we're not going round in circles.
        if visited is None:
            visited = set()

        if gid in visited:
            raise Exception('{}: circular group definition'.format(gid))

        visited.add(gid)

        # ----------------------------------------
        # Need to do a new lookup in the group table
        try:
            group_entry = self.group_table.get_item(Key={'groupId': gid})['Item']
        except ClientError:
            # The table does not exist. Prevent further table lookups
            self.logger.debug('DynamoDB table %s table does not exist.', self.group_table.table_name)
            self.group_table = None
            self.groups[gid] = [gid]
        except KeyError:
            # Group ID is not present in the table. Assume it is a user entry
            self.groups[gid] = [gid]
        else:
            # Have a group table entry which can be a list of users / groups
            groups = splitstrip(group_entry['value'], ',')
            expansion = []
            if not group_entry.get('disabled'):
                for g in groups:
                    expansion.extend(self.expand_group(g, visited))

            self.groups[gid] = expansion

        return self.groups[gid]

    # --------------------------------------------------------------------------
    def get_messenger_name(self, mode):
        """
        Get the messenger plugin name for the specified mode. This will check
        the DynamoDB mbot.messengers table first, then an internal default
        mapping. If there is no entry in either of these then its assumed the
        mode and messenger plugin have the same name.

        :param mode:        The sending mode.
        :type mode:         str

        :return:            The purported messenger plugin name. There is no
                            guarantee at this point that this messenger actually
                            exists.
        :rtype:             str
        """

        if mode not in self.messengers:

            # Check the DynamoDB table if it exists.
            if self.messenger_table:
                try:
                    messenger_tab_entry = self.messenger_table.get_item(Key={'mode': mode})['Item']
                    self.messengers[mode] = messenger_tab_entry['messenger']
                except ClientError:
                    # The table does not exist. Prevent further table lookups but keep going.
                    self.logger.debug('DynamoDB table %s table does not exist.', self.messenger_table.table_name)
                    self.messenger_table = None
                except KeyError:
                    # Not present in the table. Move on.
                    pass

            # Check the internal default mapping otherwise use mode as messenger name
            if mode not in self.messengers:
                self.messengers[mode] = MSG_MESSENGERS.get(mode, mode)

        return self.messengers[mode]

    # --------------------------------------------------------------------------
    def get_user(self, user_id):
        """
        Retrieve the user record from DynamoDB for the given user ID.

        :param user_id:     The user ID.
        :type user_id:      str

        :return:            The user record.
        :rtype:             dict[str-->T}

        :raise KeyError:    If the user ID isn't in the MBOT_USERS_TABLE.
        """

        user_info = self.user_table.get_item(Key={'userId': user_id})['Item']

        # Artificially create fullName if not present. Won't work for everyone.
        if not user_info.get('fullName') and user_info.get('givenName') and user_info.get('familyName'):
            user_info['fullName'] = '{givenName} {familyName}'.format(**user_info)

        return user_info

    # --------------------------------------------------------------------------
    def s3_log_msg(self, msg):
        """
        Save the specified message in S3. It is stored in JSON format under the
        LOG bucket/prefix with a YYYY/MM/DD substructure based on the nessage
        timestamp. The filename is derived from the message ID and user ID.

        :param msg:         The msg object
        :type msg:          dict

        """

        timestamp = dateutil.parser.parse(msg['X-Mbot-Timestamp'])

        put_args = {
            'Key': '/'.join([
                MBOT_LOG_PREFIX,
                timestamp.strftime('%Y/%m/%d'),
                msg['X-Mbot-MessageId'],
                msg['X-Mbot-UserId'] + '.json'
            ]),
            'Body': json.dumps(msg, indent=4, sort_keys=True),
            'ContentType': 'application/json'
        }

        if MBOT_LOG_KEY:
            put_args['ServerSideEncryption'] = 'aws:kms'
            put_args['SSEKMSKeyId'] = 'alias/' + MBOT_LOG_KEY
        else:
            put_args['ServerSideEncryption'] = 'AES256'

        s3 = self.aws_session.resource('s3', config=Config(signature_version='s3v4'))
        s3.Bucket(MBOT_LOG_BUCKET).put_object(**put_args)

        self.logger.debug('Message %s: logged to s3://%s/%s',
                          msg['X-Mbot-MessageId'], MBOT_LOG_BUCKET, put_args['Key'])

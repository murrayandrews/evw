# Evw Event Handler Plugin - Pager

The **pager** event handler plugin provides a mechanism to send event
messages from AWS services to paging services. **Pager** can handle alert
triggering, acknowledgement and resolution.

Currently supported AWS sources are:

*   SNS
*   CloudWatch logs.

Currently supported paging services are:

*   PagerDuty
*   VictorOps

The sending process is:

1.  [Message publishing](#markdown-header-message-publishing)

    A message is published by the originating AWS source (e.g. SNS) to the
    **pager** Lambda function.

2.  [Message extracton and normalisation](#markdown-header-message-extraction-and-normalisation)

    **Pager** extracts a common set of message fields to generate an internal
    canonical message.

3.  [Message routing and disptach](#markdown-header-message-routing-and-dispatch)

    **Pager** looks up the source ID (e.g. SNS topic ARN) in the
    DynamoDB [routing table](#markdown-header-the-routing-table) table
    and obtains the name and associated connection information for the
    target paging service. It then loads a handler for the target paging system and uses the
    routing data to send the event.

# Message Publishing

Currently supported AWS sources are:

*   SNS
*   CloudWatch logs.

In each case, the following steps are required:

1.  Attach an appropriate trigger to the **pager** lambda function. Multiple
    sources can be attached to the lambda.
    
2.  Create an entry in the [routing table](#markdown-header-the-routing-table)
    with the `source_id` of the message source. This is the topic ARN for SNS
    and the log group name for CloudWatch logs.

# Message Extraction and Normalisation

Message extraction and normalisation involves the following steps:

1.  [Extraction](#markdown-header-base-message-extraction) of a base version of
    the message in the
    [canonical message format](#markdown-header-canonical-message-format)

2.  [Message analysis](#markdown-header-message-analysis) to generate a refined
    version of the message.

3.  Injection of default values from the routing table for missing fields.

## Canonical Message Format

The internal message format contains the following fields. Defaults for these
fields can be set in the [routing table](#markdown-header-the-routing-table),
although it would not make sense to set defaults for fields such as `subject` or
`timestamp`.

|Name|Description|
|---|---|
|action|Either `trigger`, `acknowledge` / `ack` or `resolve`. Default is `trigger`.|
|component|The component within the source system to which the event relates|
|event\_type|The class/type of the event.|
|message|A longer description of the event.|
|msg\_id|An event identifier used to correlate event triggers with subsequent acknowledgement and resolution messages. **Pager** has various heuristics to generate this.|
|severity|Event severity. Allowed values are `critical`, `error`, `warning` or `info`. The default is `warning` unless overridden by the `SEVERITY` environment variable for the lambda function.|
|subject|A brief summary of the event.|
|system|The source system to which the event relates or from where it originated. **Pager** will construct a value based on the source of the message if a value is not supplied.|
|timestamp|ISO 8601 string. Defaults to current time.|
|data|Arbitrary data from the event derived from a JSON/YAML message body. Maps to custom_details for PagerDuty. Ignored for VictorOps.|

## Base Message Extraction

### CloudWatch Logs

Messages from CloudWatch logs use the log group name as the `source_id` for
[routing](#markdown-header-message-routing-and-dispatch).

Messages are mapped into the canonical format thus:

|Canonical Field|Construction Method|
|---|---|
|component|Log stream name.|
|message|The log message.|
|subject|The first 1024 characters of the log message.|
|system|Log group name.|
|timestamp|Log message timestamp.|

### SNS

Messages from SNS topics use the topic ARN as the `source_id` for 
[routing](#markdown-header-message-routing-and-dispatch).

Messages are mapped into the canonical format thus:

|Canonical Field|Construction Method|
|---|---|
|system|The topic name prefixed by `SNS:`.|
|subject|The message subject (which is optional for SNS).|
|message|The SNS message body.|
|timestamp|The SNS message timestamp.|
|data|For SNS messages with a JSON/YAML body, any keys not otherwise mapped to canonical message fields.|

The body of the message is then analysed. By including JSON or YAML formatted
data in the message it is possible to specify values for any or all of the
[canonical message fields](#markdown-header-canonical-message-format).

If the message body begins with a left brace `{`, it is assumed to be a JSON
object which is decoded and used to update the base message.

If the body of the message begins with 3 hyphens `---`, it is assumed to be a
YAML object which is decoded and used to update the base message.

Any keys present in the JSON or YAML message body which do not map to one of the
canonical fields are added to the `data` field.
* For PagerDuty targets, data is added to the `custom_info` field in the payload
  to allow the content of the message to be rendered properly in PagerDuty.
* For VictorOps targets, any information in the `data` field is ignored.  

The raw content in the JSON/YAML message body also maps to the message field for
backwards compatibility.

## Message Analysis

The base message is generally not suitable for target paging systems as it may be
missing critical fields (e.g. subject) and may not have enough information to
allow correlation of trigger, acknowledgement and resolution actions.

**Pager** examines the base message and attempts to derive missing values (such
as deduplication keys).

By default, **pager** will apply some heuristics to determine the `action` and
`severity` fields if these are not set. If the subject starts with one of the
keywords shown below, the severity and action will be set as indicated if not
already set. These keywords are chosen to match the behaviour of CloudWatch
alarms but can be used by any message. Matching is case insensitive.

|Keyword|Severity|Action|
|---|---|---|
|ACKNOWLEDGE|warning|acknowledge|
|ACK|warning|acknowledge|
|ALARM|critical|trigger|
|CRITICAL|critical|trigger|
|ERROR|error|trigger|
|INFO|info|trigger|
|OK|info|resolve|
|RESOLVE|info|resolve|
|WARNING|warning|trigger|
|WARN|warning|trigger|

If the `msg_id` field of the message is not set by any other mechanism, the
keyword is removed from the subject and the remaining text is used to generate a
message identifier for correlation and deduplication. For example, if three
successive messages contain the following subject lines, the event will first be
triggered, then acknowledged and then resolved.

```
ALARM: Something broke
ACK: Something broke
OK: Something broke
```

Heuristics can be disabled for a source in the
[routing table](#markdown-header-message-routing-and-dispatch).

# Message Routing and Dispatch

**Pager** uses a DynamoDB table named `pager.routing` to map AWS message source
IDs (e.g. source SNS topic ARNs) to target paging system names and associated
routing and control information. All sources feeding messages to **pager** must
have an entry in the table. It may also contain default values for the fields in
the [canonical message format](#markdown-header-canonical-message-format).

The table must be created with `source_id` as the primary partition key. If the
CloudFormation template is used for
[installation](#markdown-header-installation-and-configuration), the table is
created as part of the stack.

The table contains the following standard fields. It will also contain additional
fields that are specific to
[supported paging systems](#markdown-header-supported-paging-systems).

|Name|Required|Type|Description|
|---|---|---|---|
|enabled|No|Boolean|If `false`, dispatch of messages from this source is disabled. Default is `true`.|
|source_id|Yes|String|The source ID for the AWS message source. See [Base Message Extraction](#markdown-header-base-message-extraction) for more information.|
|target|Yes|String|The type of pager system to which events are sent. e.g. `PagerDuty` or `VictorOps`. Case insensitive.|
|heuristics|No|Boolean|Enable message analysis heuristics for this source. Default is `true`.|

A typical entry would look something like this:

```json
{
  "routing_key": "jg10i3cqdmqwyr8bj1kf4x3ac07jcoz7",
  "severity": "error",
  "source_id": "arn:aws:sns:ap-southeast-2:607004478221:pagerduty",
  "system": "Y2",
  "target": "PagerDuty"
}
```

In this case, messages arriving from SNS topic `pagerduty` will be sent to the
PagerDuty system with the given PagerDuty routing key, The default event
severity will be `error` and system will be `Y2` when these values are not
derived from the source message.

## Supported Paging Systems

### PagerDuty

**Pager** sends alerts to [PagerDuty](https://pagerduty.com) via the latter's
[Events v2 API](https://v2.developer.pagerduty.com/docs/send-an-event-events-api-v2).

To setup a PagerDuty integration, do the following:

1.  Setup users, teams, schedules, escalation policies etc as per normal
    PagerDuty configuration.

2.  Select the `Services` option under the `Configuration` menu.

3.  Click on `New Service`.

4.  Set the `Integration Type` to `Use our API directly` with the `Events API 2.0`
    option.

5.  Set the incident behaviour settings as required and click `Add Service`.

6.  Copy the integration key provided into the `routing_key` key of the routing
    table entry.

In addition to the standard routing table keys, PagerDuty supports the
following:

|Name|Required|Type|Description|
|---|---|---|---|
|group|No|String|The default logical grouping of components of a service.|
|routing_key|Yes|String|The PagerDuty supplied routing key for the service.|
|target|Yes|String|Must be set to `PagerDuty` (case insensitive).|

### VictorOps

**Pager** sends alerts to [VictorOps](https://pagerduty.com) via the latter's
[REST endpoint](https://help.victorops.com/knowledge-base/rest-endpoint-integration-guide).

To setup a VictorOps integration, do the following:

1.  Setup users, teams, escalation policies etc as per normal VictorOps
    configuration.

2.  Select the `Integrations` option under the `Alert Behaviour` menu. Do not
    use the `API` main menu. That is for a different purpose.

3.  Select the `REST Generic` integration and enable it.

4.  Copy the URL provided, excluding the final `/$routing_key` component. Place
    the URL in the `url` key of the routing table entry (see below).

5.  The routing key component is added at message dispatch time where this field
    is set in the routing table. To manage routing keys in VictorOps, select the
    `Routing Keys` option under the `Alert Behaviour` menu. Routing keys can be
    created and linked to escalation policies.

In addition to the standard routing table keys, VictorOps supports the
following:

|Name|Required|Type|Description|
|---|---|---|---|
|routing\_key|No|String|An optional routing key to target alerts at a given escalation policy. If not set, VictorOps will send the event to the default escalation policy.|
|target|Yes|String|Must be set to `VictorOps` (case insensitive).|
|url|Yes|String|The full URL for the API endpoint.|

## Installation and Configuration

While **pager** can be installed in the same way as other **evw** modules,
it requires a number of AWS components to be present to allow it to work.
These are:

-   The DynamoDB [routing table](#markdown-header-message-routing-and-dispatch).
-   An IAM role for the Lambda function itself.
-   The Lambda function itself.

All of this is automated with a CloudFormation template:
`misc/pager/pager.cfn.json`. Documentation for the template can be found
[here](../misc/pager/pager.cfn.md).

Using this template, the process is:

1.  Clone the **evw** repository.

2.  Create a Python virtual environment and populate it using:
    Python 3.6+

    ```bash
    virtualenv -p python3
    source venv/bin/activate
    pip install -r requirements.txt --upgrade
    deactivate
    ```
3.  Run the following command to generate the Lambda install bundle.

    ```bash
    etc/pkg-evlambda.sh pager
    ```

    This will produce a file `evl-pager.zip`.

4.  Load `evl-pager.zip` into an S3 bucket somewhere. The location will be
    required when running the CloudFormation stack.

5.  Create a new CloudFormation stack using the template
    `misc/slacker/pager.cfn.json`. [Stack
    parameters](#markdown-header-cloudformation-template-parameters) are
    described below.

6.  Create some paging system services and obtain the corresponding routing /
    integration keys. These go in the
    [routing table](#markdown-header-message-routing-and-dispatch).

7.  Subscribe the **pager** Lambda function to an SNS topic.

8.  Update the [routing table](#markdown-header-message-routing-and-dispatch)
    with entries to map the source ID (SNS topic ARN in this example) to the
    corresponding paging system routing entry.

9.  Publish some SNS messages and check for incoming paging messages.

### CloudFormation Template Parameters

|Parameter|Required|Description|
|---|---|---|
|DynamoDbReadCapacity|Yes|Read capacity units for the DynamoDB table. The default is 5, which is quite low. This will minimise costs but may need to be increased if a lot of messages are sent.|
|MaxMessageLen|Yes|The maximum length in bytes of messages sent in the `custom_details` of a PagerDuty event message. The event summary is always limited to 1024 characters. Longer messages are truncated. This parameter sets the `MAX_MSG_LEN` [environment variable](#markdown-header-environment-variables) for the Lambda function.|
|S3CodeBucket|Yes|The bucket in which the  `evl-pager.zip` code bundle was placed. This must exist and must contain the code bundle prior to creating the stack.|
|S3CodeKey|Yes|The full path name of the code bundle in the `S3CodeBucket` (no leading `/`).|
|Severity|No|The default event severity. The default is `warning`.|

### Environment Variables

In addition to the [standard environment
variables](../README.md#markdown-header-lambda-environment-variables) supported
by all **evw** handlers, **pager** supports the following additional
variables.

|Name|Description|
|---|---|---|
|INSTALLATION\_ID|A UUID for the installation of **pager**. This must be a valid UUID. It is used to generate correlation keys for messages. If not specified a default is used. It is generally safe to use the internal default.|
|MAX\_MSG\_LEN|The maximum length in bytes of messages sent in the `custom_details` of a PagerDuty event message. Longer messages are truncated. The default is 4000.|
|ROUTING\_TABLE|The name of the DynamoDB routing table. The default is `pager.routing`.|
|SEVERITY|The default event severity. The default is `warning`.|

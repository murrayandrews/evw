"""
Event worker plugin that forwards SNS messages to pager systems. Currenty
supports:

    - PagerDuty Events API v2 services.
    - VictorOps REST API.

"""

# ..............................................................................
# region Imports and constants.
# ................................................................................

import json
import logging
import os
import re
from base64 import b64decode
from collections import namedtuple
from datetime import datetime
from gzip import GzipFile
from io import StringIO
from time import sleep
from typing import Iterator
from uuid import UUID, uuid5

import requests
import yaml

from common.event import EventProto
from common.utils import json_default

__author__ = 'Murray Andrews'

PLUGIN_NAME = __name__.split('.')[-1]

ROUTING_TABLE = os.environ.get('ROUTING_TABLE', PLUGIN_NAME + '.routing')
MAX_MESSAGE_LEN = int(os.environ.get('MAX_MSG_LEN', 4000))
MAX_SUBJECT_LEN = 1024
DEFAULT_SEVERITY = os.environ.get('SEVERITY', 'warning')
INSTALLATION_ID = UUID(os.environ.get('INSTALLATION_ID', '24730eec-8f16-448a-8177-d66722e0c5e0'))
ENABLE_HEURISTICS = True

MAX_ATTEMPTS = 3
BACKOFF = 5  # Seconds to add to each backoff between attempts


# ..............................................................................
# endregion Imports and constants.
# ..............................................................................

# ..............................................................................
# region Utilities
# ..............................................................................


# ------------------------------------------------------------------------------
def strip_dict(d):
    """
    Return a copy of a dict with all None values removed
    :param d:       Source dict
    :type d:        dict
    :return:        A copy of a dict with all None values removed
    :rtype:         dict
    """

    return {k: v for k, v in d.items() if v is not None}


# ------------------------------------------------------------------------------
def zip64_decode_data(data):
    """
    Decode a base64, gzipped JSON string into a Python object.

    :param data:    The data to decode.
    :type data:     str
    :return:        A Python object.

    :raise Exception:   If there is a decoding problem.

    """

    return json.loads(GzipFile(fileobj=StringIO(b64decode(data).decode('utf-8'))).read())


# ------------------------------------------------------------------------------
def multi_decode(s):
    """
    Try to decode a string into an object using multiple techniques in the
    following order:

        - JSON      String must start with '{'
        - YAML      String must start with '---'

    :param s:       The string to decode.
    :type s:        str
    :return:        Return the decoded object from the first one that succeeds.
    :rtype:         dict[str, T]

    :raise ValueError: If no decoding succeeds.
    """

    s = s.strip()

    # Try for JSON
    if s.startswith('{'):
        try:
            return json.loads(s)
        except json.JSONDecodeError:
            pass

    if s.startswith('---'):
        # noinspection PyBroadException
        try:
            return yaml.safe_load(s)
        except Exception:
            pass

    raise ValueError('Cannot decode')


# ------------------------------------------------------------------------------
class PagerException(Exception):
    """
    For pager related errors.
    """

    pass


Heuristic = namedtuple('Heuristic', ['re', 'severity', 'action'])


# ------------------------------------------------------------------------------
class PagerMessage(object):
    """
    Represents a more or less cannonical pager message. This is a clunky variant
    on a dataclass that ignores fields of no interest. Fields should be accessed
    using normal dictionary lookup syntax.

    """

    SEVERITY = {'critical', 'error', 'info', 'warning'}

    # Regexes that are applied to the message subject to
    # attempt to guess the security level. The capture group with label "id" is
    # used to generate a msg_id for deduplication.

    HEURISTICS = [
        Heuristic(r'^\s*(CRITICAL|ALARM)\s*:?\s*(?P<id>.*)', 'critical', 'trigger'),
        Heuristic(r'^\s*(ERROR)\s*:?\s*(?P<id>.*)', 'error', 'trigger'),
        Heuristic(r'^\s*(INFO)\s*:?\s*(?P<id>.*)', 'info', 'trigger'),
        Heuristic(r'^\s*(WARNING|WARN)\s*:?\s*(?P<id>.*)', 'warning', 'trigger'),
        Heuristic(r'^\s*(ACKNOWLEDGE|ACK)\s*:?\s*(?P<id>.*)', 'warning', 'acknowledge'),
        Heuristic(r'^\s*(RESOLVE|OK)\s*:?\s*(?P<id>.*)', 'info', 'resolve')
    ]

    FIELDS = {
        'action': 'trigger',
        'severity': DEFAULT_SEVERITY,  # Must be one of those in SEVERITY list
        'msg_id': None,  # Used for correlation or deduplication.
        'system': None,  # Source system name
        'component': None,  # Component within the source system
        'subject': None,
        'message': None,
        'timestamp': datetime.utcnow().isoformat(),  # ISO 8601 string.
        'event_type': None,
        'data': {}  # Free-form details from the event
    }

    # --------------------------------------------------------------------------
    def __init__(self, source_id, fields):
        """
        :param source_id:   Message source identifier. This is a key into the
                            routing table.
        :param fields:      Dict containing field values, Ignore any we don't need.

        :type source_id:    str
        :type fields:       dict

        """

        self.source_id = source_id
        self._fields = {}
        self._fields.update(fields)

    # --------------------------------------------------------------------------
    def __getitem__(self, item):
        """
        Return a field value or the default if not specified.
        :param item:        Key
        :type item:         str
        :return:            Value
        :rtype:             T
        """

        return self._fields.get(item, self.FIELDS.get(item))

    # --------------------------------------------------------------------------
    def __str__(self):
        return json.dumps(self._fields, sort_keys=True, indent=4, default=json_default)

    # --------------------------------------------------------------------------
    def update(self, fields):
        """
        Update existing fields.

        :param fields:      Dict containing supplementary field values. Ignore any
                            args we don't recognise.
        :type fields:       dict
        """

        if fields:
            self._fields.update({k: v for k, v in fields.items() if k in self.FIELDS})

    # --------------------------------------------------------------------------
    def fill(self, fields):
        """
        Supplement existing fields but don't replace any.

        :param fields:      Dict containing supplementary field values. Ignore any
                            args we don't recognise.
        :type fields:       dict
        """

        if fields:
            self._fields.update(
                {k: v for k, v in fields.items() if k in self.FIELDS and k not in self._fields}
            )

    # --------------------------------------------------------------------------
    def prepare(self, heuristics=False):
        """
        Analyse the message contents and apply some heuristics to fill in
        missing data.

        :param heuristics:  Enable heuristics to attempt to populate missing
                            values (e.g. severity). Default False.
        :type heuristics    bool
        """

        # ----------------------------------------
        # Tidy up subject

        if not self._fields.get('subject'):
            if not self._fields.get('message'):
                raise ValueError('Message must have at least one of subject and message')
            self._fields['subject'] = self._fields['message']

        self._fields['subject'] = self._fields['subject'][:MAX_SUBJECT_LEN]

        # ----------------------------------------
        # Tidy up message

        if self._fields.get('message'):
            self._fields['message'] = self._fields['message'][:MAX_MESSAGE_LEN]

        # ----------------------------------------
        # Heuristics to determine severity / action

        try:
            self._fields['severity'] = self._fields['severity'].lower()
            if self._fields['severity'] not in self.SEVERITY:
                raise ValueError(f'{self._fields["severity"]}: bad severity')
        except KeyError:
            pass

        # Try to match severity patterns against the message subject to guess severity
        if self._fields.get('system') and self._fields.get('component'):
            msg_summary = f'{self._fields["system"]}/{self._fields["component"]}'
        else:
            msg_summary = None

        if heuristics:
            for h in self.HEURISTICS:
                m = re.search(h.re, self._fields['subject'], re.IGNORECASE)
                if m:
                    if not self._fields.get('severity'):
                        self._fields['severity'] = h.severity
                    if not self._fields.get('action'):
                        self._fields['action'] = h.action
                    msg_summary = m.group('id').strip().lower()
                    break

        if not msg_summary:
            msg_summary = self._fields['subject'].strip().lower()

        # ----------------------------------------
        # Generate a message ID

        if not self._fields.get('msg_id'):
            self._fields['msg_id'] = str(uuid5(
                INSTALLATION_ID,
                msg_summary if msg_summary else self._fields['subject']
            ))


# ------------------------------------------------------------------------------
class Pager(object):
    """
    Models a pager system.
    """

    _handlers = {}

    # --------------------------------------------------------------------------
    @classmethod
    def register(cls, *args):
        """
        Decorator to register Pager system handlers.

        Usage:

            @Pager.handler('pager_type1', ...)
            a_class()

        :param args:        A list of pager system types that the decorated
                            function handles.
        :type args:         str
        """

        def decorate(c):
            """
            Register the handler function.

            :param c:       Class to decorate
            :return:        Unmodified class.

            """
            for pager_type in args:
                cls._handlers[pager_type.lower()] = c
            return c

        return decorate

    # --------------------------------------------------------------------------
    @classmethod
    def handler(cls, pager_type, routing_info, logger=None):
        """
        Factory method to create a handler instance for the given pager type.

        :param pager_type:      Name of the pager type (case insensitive).
        :param routing_info:    A dictionary of values from the routing table.
        :param logger:          A logger. If not specified, one is created.

        :type pager_type:       str
        :type routing_info:     dict
        :type logger:           logging.Logger

        :return:                The handler instance.
        :rtype:                 Pager
        """

        return cls._handlers[pager_type.lower()](routing_info, logger=logger)

    # --------------------------------------------------------------------------
    def __init__(self, routing_info, logger=None):
        """
        :param routing_info:    A dictionary of values from the routing table.
        :param logger:          A logger. If not specified, one is created.

        :type routing_info:     dict
        :type logger:           logging.Logger
        """

        self.logger = logger if logger else logging.Logger(PLUGIN_NAME)
        self.routing_info = routing_info

    # --------------------------------------------------------------------------
    def event(self, msg):
        """
        Work out what action to take and do it.
        :param msg:             A PagerMessage instance.
        :type msg:              PagerMessage
        """

        if msg['action'].startswith('ack'):
            self.acknowledge(msg)
        elif msg['action'] == 'resolve':
            self.resolve(msg)
        elif msg['action'] == 'trigger':
            self.trigger(msg)
        else:
            raise PagerException(f'{msg["action"]}: Unknown action')

    # --------------------------------------------------------------------------
    def trigger(self, msg):
        """
        Trigger a pager event.

        :param msg:             A PagerMessage instance.
        :type msg:              PagerMessage

        """

        raise NotImplementedError('trigger')

    # --------------------------------------------------------------------------
    def acknowledge(self, msg):
        """
        Acknowledge an event.

        :param msg:             A PagerMessage instance.
        :type msg:              PagerMessage
        """

        raise NotImplementedError('acknowledge')

    # --------------------------------------------------------------------------
    def resolve(self, msg):
        """
        Resolve an event.

        :param msg:             A PagerMessage instance.
        :type msg:              PagerMessage
        """

        raise NotImplementedError('resolve')


# ..............................................................................
# endregion Utilities
# ..............................................................................

# ..............................................................................
# region Pager system handlers
# ................................................................................


# ------------------------------------------------------------------------------
@Pager.register('PagerDuty')
class PagerDuty(Pager):
    """
    Handler for PagerDuty Events v2.0 API.
    """

    API_URL = 'https://events.pagerduty.com/v2'

    # --------------------------------------------------------------------------
    def __init__(self, routing_info, logger=None):
        """
        :param routing_info:    A dictionary of values from the routing table.
                                For PagerDuty it must contain routing_key and
                                may contain a default group.
        :param logger:          A logger. If not specified, one is created.
        :type routing_info:     dict
        :type logger:           logging.Logger

        """

        super(PagerDuty, self).__init__(routing_info, logger=logger)

        try:
            self.routing_key = routing_info['routing_key']
        except KeyError:
            raise PagerException('routing_key must be specified')

        self.group = routing_info.get('group')

    # --------------------------------------------------------------------------
    def _enqueue(self, action, msg_id, payload):
        """
        Submit a PagerDuty API request.

        :param action:          The PagerDuty action (e.g. trigger, acknowledge,
                                resolve)
        :param msg_id:          An identifier for the message.
        :param payload:         Message payload

        :type action:           str
        :type msg_id:           str
        :type payload:          dict[str, T]

        :raise PagerException:  If it fails.
        """

        request_body = {
            'routing_key': self.routing_key,
            'event_action': action,
            'dedup_key': msg_id,
            'payload': payload
        }

        for attempt in range(0, MAX_ATTEMPTS):
            sleep(attempt * BACKOFF)
            response = requests.post(
                f'{self.API_URL}/enqueue',
                json=request_body,
                headers={'Content-Type': 'application/json'}
            )
            if response.status_code == requests.codes['too_many_requests']:
                self.logger.warning(f'PagerDuty: Too many requests - will retry: {msg_id}')
                continue

            if response.status_code != requests.codes['accepted']:
                raise PagerException(
                    f'PagerDuty response {response.status_code} - {response.text}: {msg_id}')

            self.logger.info(
                f'Event sent to PagerDuty:'
                f' Dedup key: {response.json().get("dedup_key")}: {msg_id}'
            )
            return

        raise PagerException(f'PagerDuty request: Too many retries: {msg_id}')

    # --------------------------------------------------------------------------
    def trigger(self, msg):
        """
        Trigger a PagerDuty event

        :param msg:             A PagerMessage instance.
        :type msg:              PagerMessage

        """

        subject = msg['subject']

        payload = strip_dict({
            'summary': subject,
            'source': msg['system'],
            'severity': msg['severity'],
            'timestamp': msg['timestamp'],
            'component': msg['component'],
            'group': self.group,
            'class': msg['event_type'],
            'custom_details': {
                'message': msg['message'][0:MAX_MESSAGE_LEN] if msg['message'] else None,
                **msg['data']
            }
        })

        self._enqueue('trigger', msg['msg_id'], payload)

    # --------------------------------------------------------------------------
    def acknowledge(self, msg):
        """
        Acknowledge a PagerDuty event

        :param msg:             A PagerMessage instance.
        :type msg:              PagerMessage

        """

        payload = {
            'summary': (msg['subject']),
            'severity': msg['severity'],
            'source': msg['system'],
            'component': msg['component'],
            'group': self.group,
            'class': msg['event_type'],
        }

        self._enqueue('acknowledge', msg['msg_id'], payload)

    # --------------------------------------------------------------------------
    def resolve(self, msg):
        """
        Acknowledge a PagerDuty event

        :param msg:             A PagerMessage instance.
        :type msg:              PagerMessage

        """

        payload = {
            'summary': (msg['subject']),
            'severity': msg['severity'],
            'component': msg['component'],
            'source': msg['system'],
            'group': self.group,
            'class': msg['event_type'],
        }

        self._enqueue('resolve', msg['msg_id'], payload)


# ------------------------------------------------------------------------------
@Pager.register('VictorOps')
class VictorOps(Pager):
    """
    VictorOps paging system.
    """

    # Map from cannonical severities to VictorOps values.
    SEVERITY_MAP = {
        'info': 'INFO',
        'warning': 'WARNING',
        'error': 'CRITICAL',
        'critical': 'CRITICAL',
    }

    # --------------------------------------------------------------------------
    def __init__(self, routing_info, logger=None):
        """
        :param routing_info:    A dictionary of values from the routing table.
                                For PagerDuty it must contain routing_key and
                                may contain a default group.
        :param logger:          A logger. If not specified, one is created.
        :type routing_info:     dict
        :type logger:           logging.Logger

        """

        super(VictorOps, self).__init__(routing_info, logger=logger)

        try:
            self.url = routing_info['url']
        except KeyError:
            raise PagerException('url must be specified for VictorOps')

        try:
            self.url += '/' + routing_info['routing_key']
        except KeyError:
            pass

    # --------------------------------------------------------------------------
    def _enqueue(self, msg_id, payload):
        """
        Submit a VictorOps API request.

        :param msg_id:          An identifier for the message.
        :param payload:         Message payload

        :type msg_id:           str
        :type payload:          dict[str, T]

        :raise PagerException:  If it fails.
        """

        self.logger.debug('VictoOps payload: %s', json.dumps(payload))
        for attempt in range(0, MAX_ATTEMPTS):
            sleep(attempt * BACKOFF)
            response = requests.post(
                self.url,
                json=payload,
                headers={'Content-Type': 'application/json'}
            )
            if response.status_code == requests.codes['too_many_requests']:
                self.logger.warning(f'VictorOps: Too many requests - will retry: {msg_id}')
                continue

            if response.status_code != requests.codes['ok'] or \
                    response.json().get('result') != 'success':
                raise PagerException(
                    f'VictorOps response {response.status_code} - {response.text}: {msg_id}')

            self.logger.info(f'Event sent to VictorOps: {msg_id}')
            return

        raise PagerException(f'VictorOps request: Too many retries: {msg_id}')

    # --------------------------------------------------------------------------
    @staticmethod
    def _entity_id(msg):
        """
        Need to concoct an entity ID for VictorOps messages for correlation
        purposes. If the message has defined system+component values use that.
        If not we rely on the msg_id manufactured during the message
        preparation. Its far from perfect but since we're not maintaining any
        state information it will have to do.

        :param msg:         The message.
        :type msg:          PagerMessage

        :return:            An entity id.
        :rtype:             str

        """

        if msg['system'] and msg['component']:
            return f'{msg["system"]}/{msg["component"]}'

        return msg['msg_id']

    # --------------------------------------------------------------------------
    def trigger(self, msg):
        """
        Trigger a VictorOps event

        :param msg:             A PagerMessage instance.
        :type msg:              PagerMessage

        """

        subject = msg['subject']

        payload = strip_dict({
            'message_type': self.SEVERITY_MAP[msg['severity']],
            'entity_id': self._entity_id(msg),
            'entity_display_name': subject,
            'state_message': msg['message'][:MAX_MESSAGE_LEN],
            # TODO: Fix timestamps
            # 'state_start_time': ???
        })

        self._enqueue(msg['msg_id'], payload)

    # --------------------------------------------------------------------------
    def acknowledge(self, msg):
        """
        Acknowledge a VictorOps event

        :param msg:             A PagerMessage instance.
        :type msg:              PagerMessage

        """

        payload = {
            'message_type': 'ACKNOWLEDGEMENT',
            'entity_id': self._entity_id(msg),
        }

        self._enqueue(msg['msg_id'], payload)

    # --------------------------------------------------------------------------
    def resolve(self, msg):
        """
        Resolve a VictorOps event

        :param msg:             A PagerMessage instance.
        :type msg:              PagerMessage

        """

        payload = {
            'message_type': 'RECOVERY',
            'entity_id': self._entity_id(msg),
        }

        self._enqueue(msg['msg_id'], payload)


# ................................................................................
# endregion Pager system handlers
# ..............................................................................

# ..............................................................................
# region Extractors
# ..............................................................................


# ------------------------------------------------------------------------------
def sns_message(record):
    """
    Analyse an SNS message and return event data. SNS events look like
    this...

    {
      "Records": [
        {
          "EventVersion": "1.0",
          "EventSubscriptionArn": eventsubscriptionarn,
          "EventSource": "aws:sns",
          "Sns": {
            "SignatureVersion": "1",
            "Timestamp": "1970-01-01T00:00:00.000Z",
            "Signature": "EXAMPLE",
            "SigningCertUrl": "EXAMPLE",
            "MessageId": "95df01b4-ee98-5cb9-9903-4c221d41eb5e",
            "Message": "Hello from SNS!",
            "MessageAttributes": {
              "Test": {
                "Type": "String",
                "Value": "TestString"
              },
              "TestBinary": {
                "Type": "Binary",
                "Value": "TestBinary"
              }
            },
            "Type": "Notification",
            "UnsubscribeUrl": "EXAMPLE",
            "TopicArn": topicarn,
            "Subject": "TestInvoke"
          }
        }
      ]
    }

    :param record:      The SNS event record containing the message. The
                        outer "Records" wrapper has already been removed.
    :type record:       dict[str, T]

    :return:            A pager message
    :rtype:             PagerMessage
    """

    topic_arn = record['Sns']['TopicArn']  # type: str
    message = record['Sns']['Message']

    # Construct the base pager message
    pager_message = PagerMessage(
        source_id=topic_arn,
        fields=strip_dict({
            'system': 'SNS:' + topic_arn.rsplit(':', 1)[1],
            'subject': record['Sns'].get('Subject'),
            'message': message,
            'timestamp': record['Sns']['Timestamp'],
            'data': None
        })
    )

    if message:
        try:
            # If the message body is JSON or YAML we can use that to initialise fields
            m = multi_decode(message)
            pager_message.update(m)
            # Any keys present in the message which don't map to fields are copied
            # to the free-form data field.
            data = dict()
            for k in set(m.keys()) - set(pager_message.FIELDS.keys()):
                data[k] = m[k]
            pager_message.update({'data': data})
        except ValueError:
            pass

    return pager_message


# ------------------------------------------------------------------------------
def cloudwatch_logs(record) -> Iterator[PagerMessage]:
    """
    Analyse cloudwatch log messages and yield back event data.

    Sample event data.

        {
            "messageType": "DATA_MESSAGE",
            "owner": "123456789000",
            "logGroup": "logGroupName",
            "logStream": "logStreamName",
            "subscriptionFilters": ["cwatchTrigger"],
            "logEvents": [
                {
                    "id": "32876629689493193824632746953207793042945191065307971584",
                    "timestamp": 1474231062274,
                    "message": "hello world"
                }
            ]
        }


    :param record:      The CloudWatch log record.
    :type record:       dict[str, T]

    :return:            Yields PagerMessage instances.

    """

    log_group = record['logGroup']
    log_stream = record['logStream']
    source_name = 'Log:' + log_group

    for l in record['logEvents']:
        yield PagerMessage(
            source_id=log_stream,
            fields={
                'system': source_name,
                'component': log_stream,
                'timestamp': datetime.fromtimestamp(l['timestamp'] / 1000).isoformat(),
                'subject': l['message'][:MAX_SUBJECT_LEN],
                'message': l['message']
            }
        )


# ..............................................................................
# endregion Extractors
# ..............................................................................


# ------------------------------------------------------------------------------
class Event(EventProto):
    """
    Send AWS alert requests to a pager/on-call system. Currently handles
    messages from:

    - SNS
    - CloudWatch

    Supports messages to:

    - PagerDuty

    """

    # --------------------------------------------------------------------------
    def __init__(self, *args, **kwargs):

        super(Event, self).__init__(*args, **kwargs)

        dynamo_db = self.aws_session.resource('dynamodb')
        self.routing_table = dynamo_db.Table(ROUTING_TABLE)
        self._routing_info = {}  # Cache

    # --------------------------------------------------------------------------
    def process(self):
        """
        Process incoming lambda events
        """

        # self.logger.debug('Process %s', str(self.event))
        self.logger.debug('Process %s', json.dumps(self.event, sort_keys=True, indent=4))

        for msg in self.get_event_records():
            self.do_event(msg)

    # --------------------------------------------------------------------------
    def get_event_records(self) -> Iterator[PagerMessage]:
        """
        Extract incoming event messages from the lambda invocation event,
        extract a set of common data suitable for passing to a pager handler and
        yield them up. Yields PagerMessage instances

        """

        if 'awslogs' in self.event:
            # CloudWatch event is a base64, gzipped JSON string! Get a grip AWS.
            # Return a generator
            return cloudwatch_logs(zip64_decode_data(self.event['awslogs']['data']))

        # Uncomment the following for debugging when running via evw + SQS
        # self.event = {  # ZZ
        #     "Records": [
        #         {
        #             "EventSource": "aws:sns",
        #             "Sns": self.event
        #         }
        #     ]
        # }

        if 'Records' in self.event:
            for record in self.event['Records']:
                if record['EventSource'] == 'aws:sns':
                    yield sns_message(record)
                else:
                    raise Exception('Unexpected event source: {}'.format(record['EventSource']))
        else:
            raise Exception('Unknown event type')

    # --------------------------------------------------------------------------
    def do_event(self, msg):
        """
        Process a single pager request.

        :param msg:         Cannonical pager message.
        :type msg:          PagerMessage

        :rtype:             None
        """

        # ----------------------------------------
        # Get routing info from DynamoDB

        try:
            routing_info = self.get_routing_info(msg.source_id)
        except KeyError:
            raise Exception(f'Missing or malformed routing info for {msg.source_id}')

        if not routing_info.get('enabled', True):
            self.logger.info(f'{msg.source_id}: routing entry not enabled')
            return

        # Analyse the message
        msg.prepare(heuristics=routing_info.get('heuristics', ENABLE_HEURISTICS))

        # Supplement any missing fields in the message from the routing table.
        msg.fill(routing_info)
        self.logger.debug(f'Prepared message: {msg}')

        # ----------------------------------------
        # Get a handler for the target paging system

        try:
            handler_name = routing_info['target']
        except KeyError:
            self.logger.error(f'{msg.source_id}: Target pager system not specified in routing entry')
            return

        try:
            handler = Pager.handler(handler_name, routing_info, logger=self.logger)
        except KeyError:
            self.logger.error(f'{msg.source_id}: {handler_name}: Unknown target')
            return
        except PagerException as e:
            self.logger.error(f'{msg.source_id}: {handler_name}: {e}')
            return

        # ----------------------------------------
        # Invoke our handler

        try:
            handler.event(msg)
        except Exception as e:
            self.logger.error(f'{msg.source_id}: {e}')
        else:
            self.logger.info(f'{msg.source_id}: Sent: {msg["subject"]}')

    # --------------------------------------------------------------------------
    def get_routing_info(self, source_id):
        """
        Retrieve the PagerDuty routing info for the given topic ARN from
        DynamoDB. These are cached.

        :param source_id:   AWS source identifier. e.g for an SNS message this
                            is the topic ARN.
        :type source_id:    str

        :return:            The DynamoDB entry for the given source ID which
                            contains the routing key and some supporting
                            information.
        :rtype:             dict[str, T]

        :raise KeyError:    If the source ID isn't in the PAGERDUTY_TABLE.
        """

        if source_id not in self._routing_info:
            self._routing_info[source_id] = self.routing_table.get_item(Key={'source_id': source_id})['Item']

        return self._routing_info[source_id]

# Evw Event Handler Plugin - rsDropLoader

## Overview

The **rsDropLoader** event handler plugin loads a data file dropped into S3 into
a Redshift table.

It separates the control of the process from the submission of data for loading,
allowing an administrator to setup a control framework that allows non-admins
(users) to safely submit data files for loading into S3.

## Processing Flow

Trigger events are S3 _ObjectCreated_ events (i.e. files dropped in S3).

For each dropped file, there must be control file that indicates how
and where to perform the process of loading into Redshift. Each bucket
must also have a main configuration file.

Administrators control the main configuration file and the control files.

Users control the data files.

When a user drops a data file into S3, the event handler looks for a 
control object for the given data object. This is contained
in a YAML file with the same filename as the data object but with the suffix
replaced with `.yaml`. The control file may have the same
basename as the data object (or at a higher
level in the tree) but in the `control` tree.  So if the original object
key is:

```
users/fredbloggs/project1/z.dat
```

the control file is the first of the following to exist in the same bucket:

```
control/fredbloggs/project1/z.yaml
control/fredbloggs/z.yaml
control/z.yaml
```

## Control Files

Control files specify where a data file can be loaded in Redshift and also constrain
how large they can be.

Control files contain the following keys:

|Key|Required|Description|
|-|-|-|
|owner|Yes|Owner identification. Not used in the processing but required.|
|notify|No|Either a string or list of strings specifying _messenger_ destinations for status reports relating to the load. (e.g. `ses:fred@gmail.com`)|
|max-size|No|User supplied data files above this size will be rejected. If not specified, no limits are enforced. Individual data files must pass both control file and main config constraints.|
|cluster|Yes|Alias for the target Redshift cluster. There must be a cluster with this name in the main config file.|
|schema|No|Target schema name. If not specified, the public schema is used.|
|table|Yes|Target table name.|
|mode|Yes|Update mode for the target table. Must be one of `append`, `drop` or `truncate`.|
|columns|Sometimes|A list of SQL column specifications. Required if the target table must be created (e.g. if the table does not exist or the `mode` is `drop`).|
|copy-params|Yes|A list of Redshift COPY command parameters appropriate for the data file.|


Sample control file:

```yaml
worker-size: 10MB  # Jobs smaller than this run in Lambda. If not specified, all jobs in Lambda

clusters:
  dropload:
    user: master
    password:
      AQECllllllllllllllllllllllllllllllllllllllllllllllllllowaAYJKoZIhv
      cNAxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxWUDBAEuMBEEDIYIxDvu9ukB
      fqexxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxou5qZrHga8vQVOz3GVRnM
    max-size: 10MB
    host: dropload.cvvdjnvjnvbf.ap-southeast-2.redshift.amazonaws.com
    port: 5439
    database: db1
    ssl: yes  # Defaults to no
  cluster2:
    user: my-user2
    password: encrypted-password2
    max-size: 10M
    host: 10.a.b.c
    port: 5439
    database: yourdb

# s3-credentials must be one of 'role-arn' or 'access-key-id'/'access-secret-key' pair.
s3-credentials:
#  role-arn: aws_iam_role=arn:aws:iam::<aws-account-id>:role/<role-name>
  access-key-id: AKIAIVF72VBC6ASJOGWA
  access-secret-key:
    AQECAHgWgaa17GbdqERCtDX7ficvRZZidTQVxZhX6hsvh90IHgAAAIcwgYQGCSqGSIb3
    DQEHllllllllllllllllllllllllllllllllllllllllllllllllllyqB5LO0BbUpe6v
    rT4Cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx7t6y8Donzg0yn
    vwKUyyyyyyyyyyyyyyyyyyyyADkhKEk=
```

## Config Files

There is one YAML formatted config file per bucket.

It must be called `rsDropLoad.yaml` and it must sit at the bucket root.

It contains the following keys:

|Key|Required|Description|
|-|-|-|
|worker-size|No|Jobs smaller than this run in Lambda. If not specifed, all jobs in Lambda.|
|s3-credentials|Yes|A dictionary containing either either a `role-arn` key or a `access-key-id`/`access-secret-key` pair. The latter must be a KMS encrypted base64 encoded blob.|
|clusters|Yes|A list of Redshift clusters. Each item in the list is a dictionary (keyed on a cluster alias) containing the following keys.|
|...user|Yes|User name for login to the cluster.|
|...password|Yes|The KMS encrypted password for login to the cluster. This will be a long ugly base64 encode block of text.|
|...max-size|No|User supplied data files above this size will be rejected. If not specified, no limits are enforced.|
|...host|Yes|Host name for the cluster.|
|...port|Yes|Port number|
|...database|Yes|Database name (within the cluster).|

Sample config file:

```yaml
owner: sigourney@nostromo.com  # Job owner

notify:
  - sns:arn:aws:sns:ap-southeast-2:385495894852:rsDropLoader
  - ses:thebigguy@paul.com
  - sms:61402999111

max-size: 5MiB  # If the file is bigger than this, its rejected

cluster: dropload
schema: dessert
table: custard

# Column specs are required if the table needs to be created
columns:
  - ID INTEGER
  - CustardClubNo INTEGER
  - GivenName VARCHAR(20)
  - FamilyName VARCHAR(30)
  - Sex VARCHAR(1)
  - CustardBidPrice FLOAT(2)
  - CustardCode VARCHAR(10)
  - FavouriteCustard VARCHAR(10)
  - CustardJedi BOOLEAN
  - City VARCHAR
  - PostCode VARCHAR(20)
  - Email VARCHAR(100)
  - Phone VARCHAR(20)
  - LastCustard DATE
  - CustardQuota INTEGER

mode: truncate  # truncate, drop or append

copy-params:
  - BLANKSASNULL
  - BZIP2
  - CSV
  - EMPTYASNULL
  - IGNOREHEADER 1
  - MAXERROR 0
```


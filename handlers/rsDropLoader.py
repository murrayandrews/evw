"""
evlambda/evworker plugin to load files dropped in S3 into Redshift.

................................................................................
Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
................................................................................

"""

# TODO: Don't hold DB connections open

from __future__ import print_function

import datetime
import os
import re

import pg8000 as redshift

from common.aws_utils import kms_decrypt, s3_load_yaml
from common.event import EventProto
from common.utils import size_to_bytes, dict_check, sepjoin
from common.date import timedelta_to_hms

__author__ = 'Murray Andrews'

PLUGIN_NAME = __name__.split('.')[-1]


# ------------------------------------------------------------------------------
class Event(EventProto):
    """
    Handle a drop load event. These are S3 PUT events. The event record consists
    of a list of records (typically only one), each of which identifies a file
    dropped into S3 for loading into Redshift.

    For each dropped file, there must be control file which indicates how
    and where to perform the process of loading into Redshift. Each bucket
    must also have a main configuration file.

    Sample event record...

    {
        'Records': [
            {
                'eventSource': 'aws:s3',
                'eventName': 'ObjectCreated:Put',
                'eventVersion': '2.0',
                'userIdentity': {'principalId': 'AWS:AIDAJ3YYBPAYP22AQ3BNU'},
                'requestParameters': {'sourceIPAddress': '120.147.217.15'},
                'eventTime': '2016-09-24T06:02:55.886Z',
                's3': {
                    'configurationId': 'f48af588-2542-4a98-941d-eb828e291b28',
                    'object': {
                        'eTag': '6686853da3491a56c98917cc5c4ddea2',
                        'size': 5,
                        'sequencer': '0057E6170FC7ACE87B',
                        'key': 'dropbox/r.dat'
                    },
                    's3SchemaVersion': '1.0',
                    'bucket': {
                        'ownerIdentity': {'principalId': 'AVRD2C17Z6GLL'},
                        'arn': 'arn:aws:s3:::dev.canis2.com',
                        'name': 'dev.canis2.com'
                    }
                },
                'awsRegion': 'ap-southeast-2',
                'responseElements': {
                    'x-amz-request-id': '0C4C63F66A7E79E3',
                    'x-amz-id-2': 'XfW80wKuyFuv26mrre2uIh7NPxw/Zcl5P3+7Pl4E3ioMPHtOExUvmIgpoensUNbGLqsqmVrDbEw='
                }
            }
        ]
    }
    """

    CONFIG_NAME = PLUGIN_NAME + '.yaml'
    # Control files sit in the same bucket under this prefix.
    CONTROL_PREFIX = 'control'
    CONTROL_REQUIRED_KEYS = {'owner', 'schema', 'table', 'cluster', 'mode'}
    CONTROL_OPTIONAL_KEYS = {'max-size', 'copy-params', 'notify', 'columns'}

    CONFIG_REQUIRED_KEYS = {'clusters', 's3-credentials'}
    CONFIG_OPTIONAL_KEYS = {'worker-size'}

    CLUSTER_REQUIRED_KEYS = {'user', 'password', 'host', 'port', 'database'}
    CLUSTER_OPTIONAL_KEYS = {'max-size', 'ssl'}

    CONTROL_MODES = {'truncate', 'drop', 'append'}

    REDSHIFT_COPY_PARAMS = {
        # Note: NULL has to be specified as 'NULL AS' in the YAML file
        # Note: Credentials excluded - these are sourced from main config
        # Not supported: MANIFEST, SSH, REGION
        'ACCEPTANYDATE', 'ACCEPTINVCHARS', 'AVRO', 'BLANKSASNULL', 'BZIP2',
        'COMPROWS', 'COMPUPDATE', 'CSV', 'DATEFORMAT', 'DELIMITER', 'EMPTYASNULL',
        'ENCODING', 'ENCRYPTED', 'ESCAPE', 'EXPLICIT_IDS', 'FILLRECORD',
        'FIXEDWIDTH', 'FORMAT', 'GZIP', 'IGNOREBLANKLINES', 'IGNOREHEADER',
        'JSON', 'LZOP', 'MAXERROR', 'NOLOAD', 'NULL', 'READRATIO', 'REMOVEQUOTES',
        'ROUNDEC', 'STATUPDATE', 'TIMEFORMAT', 'TRIMBLANKS', 'TRUNCATECOLUMNS',
    }

    # --------------------------------------------------------------------------
    def __init__(self, *args, **kwargs):

        # Caches for configs and control objects. We reload these for each new
        # event - deliberately as the event workers are long running and long
        # held caches can become stale.

        self._main_configs = {}  # Dict keyed on bucket name.
        self._control_objects = {}  # Dict keyed on bucket/data_object

        super(Event, self).__init__(*args, **kwargs)

        for record in self.event['Records']:
            if not record['eventName'].startswith('ObjectCreated:'):
                raise Exception('Unexpected eventName: {}'.format(record['eventName']))

    # --------------------------------------------------------------------------
    def handoff(self):
        """
        Determine if the event could be handed off to an event worker. For this
        implementation the following events can handed off:

            - All events containing multiple records
            - Single record events above the worker-size attribute in the main
              config

        :return:    True if the event can be handed off or False otherwise.
        :rtype:     bool

        """

        if len(self.event['Records']) > 1:
            # If multiple records to process, hand event to an event worker.
            return True

        # If we have a single file to load, check its size.
        record = self.event['Records'][0]  # type: dict
        bucket_name = record['s3']['bucket']['name']  # type: str
        object_size = record['s3']['object']['size']  # type: int

        worker_size = self.main_config(bucket_name)['_worker-size']

        return worker_size is not None and object_size >= worker_size

    # --------------------------------------------------------------------------
    def do_record(self, record):
        """

        :param record:
        :return:
        """

        bucket_name = record['s3']['bucket']['name']  # type: str
        object_name = record['s3']['object']['key']  # type: str
        main_config = self.main_config(bucket_name)
        control_obj = self.control_object(bucket_name, object_name)
        cluster_name = control_obj['cluster']  # type: str
        cluster = main_config['clusters'][cluster_name]  # type: dict
        object_size = record['s3']['object']['size']  # type: int

        # ----------------------------------------
        # Make sure ts not too big. Two things to check (1) cluster limit
        # and (2) jobs size limit.

        if cluster['_max-size'] is not None and object_size > cluster['_max-size']:
            raise Exception('{}/{}: Size {} exceeds maximum size for cluster {} of {}'.format(
                bucket_name, object_name, object_size, cluster_name, cluster['max-size']))

        if control_obj['_max-size'] is not None and object_size > control_obj['_max-size']:
            raise Exception('{}/{}: Size {} exceeds maximum size for job of {}'.format(
                bucket_name, object_name, object_size, control_obj['max-size']))

        # ----------------------------------------
        # Do the Redshift COPY
        self.redshift_copy(bucket_name, object_name)

    # --------------------------------------------------------------------------
    def process(self):
        """
        Process the event.
        """

        for record in self.event['Records']:
            # Process individual records. Abort on first failure

            bucket_name = record['s3']['bucket']['name']  # type: str
            object_name = record['s3']['object']['key']  # type: str
            control_obj = self.control_object(bucket_name, object_name)
            notify = control_obj.get('notify', [])

            try:
                t_start = datetime.datetime.utcnow()
                self.do_record(record)
                t_end = datetime.datetime.utcnow()
            except Exception as e:
                self.send_msg(
                    to=notify,
                    subject='{} failed for {}/{}'.format(PLUGIN_NAME, bucket_name, object_name),
                    message='{} failed for {}/{}\n\nThe following error occurred:'
                            ' {}'.format(PLUGIN_NAME, bucket_name, object_name, e))
                # Abort - even if more records to process.
                raise
            else:
                elapsed = timedelta_to_hms(t_end - t_start)
                self.send_msg(
                    to=notify,
                    subject='{} completed ok for {}/{}'.format(PLUGIN_NAME, bucket_name, object_name),
                    message='{} completed ok for {}/{}\n\n'
                            'Start time: {}Z\nEnd time: {}Z\nElapsed: {}h {}m {}s'.format(
                        PLUGIN_NAME, bucket_name, object_name,
                        t_start.isoformat(), t_end.isoformat(), *elapsed
                    )
                )

    # --------------------------------------------------------------------------
    def finish(self):
        """
        Processing of event is complete. Close any open database connections.

        """

        for bucket_name, bucket in self._main_configs.items():
            clusters = bucket['clusters']  # type: dict
            for cluster_name, cluster in clusters.items():
                if '_conn' in cluster:
                    self.logger.debug('%s: Closing connection to cluster %s',
                                      bucket_name, cluster_name)
                    try:
                        cluster['_conn'].close()
                    except Exception as e:
                        # Log it and move on
                        self.logger.warning(
                            '%s: Attempt to close connection to cluster %s failed - %s',
                            bucket_name, cluster_name, e
                        )

    # --------------------------------------------------------------------------
    def main_config(self, bucket_name):
        """
        Load the main config for the given bucket. This is a YAML file at the
        bucket root named after the the plugin name (e.g. rsDropLoader.yaml).
        Some format checking is done and an '_worker-size' key is added,
        representing the byte size limit below which jobs are processed locally.
        Results are cached internally so its efficient to call this multiple
        times.

        :param bucket_name:     The bucket name.
        :type bucket_name:      str
        :return:                The main config opbject.
        :rtype:                 dict

        """

        # ----------------------------------------
        # Try for a cached entry
        try:
            return self._main_configs[bucket_name]
        except KeyError:
            pass

        # ----------------------------------------
        # No cached entry - load from S3
        try:
            mconf = self._main_configs[bucket_name] = s3_load_yaml(
                bucket_name,
                self.CONFIG_NAME,
                aws_session=self.aws_session
            )
        except Exception as e:
            raise Exception('{}: Cannot load main config: {}'.format(bucket_name, e))

        # ----------------------------------------
        # Check the main config
        try:
            dict_check(mconf, self.CONFIG_REQUIRED_KEYS, self.CONFIG_OPTIONAL_KEYS)
        except Exception as e:
            raise Exception('{}/{}: {}'.format(bucket_name, self.CONFIG_NAME, e))

        mconf['_worker-size'] = size_to_bytes(mconf['worker-size']) if 'worker-size' in mconf else None

        # ----------------------------------------
        # Check the cluster specs in the main config
        for cluster_name, cluster in mconf['clusters'].items():
            try:
                dict_check(cluster, self.CLUSTER_REQUIRED_KEYS, self.CLUSTER_OPTIONAL_KEYS)
            except Exception as e:
                raise Exception('{}/{}: Cluster {}: {}'.format(
                    bucket_name, self.CONFIG_NAME, cluster_name, e))

            cluster['_max-size'] = size_to_bytes(cluster['max-size']) if 'max-size' in cluster else None

        return mconf

    # --------------------------------------------------------------------------
    def control_object(self, bucket_name, data_object_name):
        """
        Retrieve the control object for the given data object. This is contained
        in a YAML file with the same basename as the data object (or at a higher
        level in the tree) but in the CONTROL area.  So if the original object
        key is:

            users/fredbloggs/project1/z.dat

        the control file is the first of these to exist.

            control/fredbloggs/project1/z.yaml
            control/fredbloggs/z.yaml
            control/z.yaml

        in the same bucket. This allows separation of permissions for creating
        control files and data files.

        Results are cached so its efficient to call multiple times.

        This method also does some validation of fields.

        :param bucket_name:         Bucket name.
        :param data_object_name:    Data object name.

        :type bucket_name:          str
        :type data_object_name:     str

        :return:                    The control object.
        :rtype:                     dict

        :raise Exception:           If the control object can't be loaded or doesn't
                                    pass some basic format checks.
        """

        object_name = bucket_name + '/' + data_object_name

        # ----------------------------------------
        # Try for a cached entry
        try:
            return self._control_objects[object_name]
        except KeyError:
            pass

        # ----------------------------------------
        # No cached entry - load from S3.

        # ----------------------------------------
        # Search for a control file. These are in the CONTROL area parallel to
        # the data file path. The control file can be at the same depth as the
        # data file or higher up so need to search from low to high in the
        # control area.

        obj_path = data_object_name.split('/')
        if len(obj_path) < 2:
            raise Exception('{}: Data objects cannot be located at bucket root'.format(
                data_object_name))

        # Ascend the CONTROL area
        control_basename = os.path.splitext(obj_path[-1])[0] + '.yaml'
        control_file_name = None
        control_obj = None
        for n in range(len(obj_path), 1, -1):
            control_file_name = sepjoin('/', self.CONTROL_PREFIX, obj_path[1:n - 1], control_basename)
            self.logger.debug('Control search: %s', control_file_name)
            try:
                control_obj = self._control_objects[object_name] = s3_load_yaml(
                    bucket_name, control_file_name, aws_session=self.aws_session)
            except IOError:
                # Assume this file doesn't exist and keep going up the CONTROL tree
                pass
            except ValueError as e:
                # File exists but bad YAML - can't continue
                raise Exception('{}/{}: Bad control file: {}'.format(
                    bucket_name, control_file_name, e))
            else:
                # control file load succeeded
                break

        if not control_obj:
            # Got all the way up the tree with no luck
            raise Exception('{}/{}: Cannot locate a control file'.format(bucket_name, object_name))

        # ----------------------------------------
        # Now have a control object - check it contains the right fields
        try:
            dict_check(control_obj, self.CONTROL_REQUIRED_KEYS, self.CONTROL_OPTIONAL_KEYS)
        except ValueError as e:
            raise Exception('{}/{}: Bad control file: {}'.format(
                bucket_name, control_file_name, e))

        # ----------------------------------------
        # Look for target cluster details.
        if control_obj['cluster'] not in self.main_config(bucket_name)['clusters']:
            raise Exception('{}/{}: No such cluster: {}'.format(
                bucket_name, control_file_name, control_obj['cluster']))

        control_obj['_source'] = '{}/{}'.format(bucket_name, control_file_name)
        control_obj['_max-size'] = \
            size_to_bytes(control_obj['max-size']) if 'max-size' in control_obj else None

        # ----------------------------------------
        # Validate target schema & table names. Only alpha numerics and _ allowed.
        if not isinstance(control_obj['schema'], str) or not re.match(r'^\w+$', control_obj['schema']):
            raise Exception('Bad schema name {}'.format(control_obj['schema']))

        if not isinstance(control_obj['table'], str) or not re.match(r'^\w+$', control_obj['table']):
            raise Exception('Bad table name {}'.format(control_obj['table']))

        # ----------------------------------------
        # Validate owner - very basic format check on email address
        if not isinstance(control_obj['owner'], str) or not re.match(r'[^@]+@[^@]+\.[^@]+', control_obj['owner']):
            raise Exception('Bad owner email address: {}'.format(control_obj['owner']))

        # ----------------------------------------
        # Validate the database update mode
        if not isinstance(control_obj['mode'], str) or control_obj['mode'] not in self.CONTROL_MODES:
            raise Exception('Bad mode: {}'.format(control_obj['mode']))

        # ----------------------------------------
        # Validate notify target
        if 'notify' in control_obj and not isinstance(control_obj['notify'], (str, list)):
            raise Exception('Bad notify: must be string or list of strings')

        # ----------------------------------------
        # Validate the Redshift COPY params (basic health check)
        if control_obj['copy-params']:
            if not isinstance(control_obj['copy-params'], list):
                raise Exception('copy-params must be a list')

            # Get a normalised list of all keywords
            copy_params = {cp.split()[0].upper() for cp in control_obj['copy-params']}

            bad_params = copy_params - self.REDSHIFT_COPY_PARAMS
            if bad_params:
                raise Exception('Invalid copy-params: {}'.format(', '.join(sorted(bad_params))))

        return control_obj

    # --------------------------------------------------------------------------
    def redshift_copy(self, bucket_name, data_object_name):
        """
        Do the Redshift COPY, including any preparatory drop or truncate.

        :param bucket_name:         Bucket name.
        :param data_object_name:    Data object name.

        :type bucket_name:          str
        :type data_object_name:     str

        """

        self.logger.debug('Preparing for Redshift COPY')

        control_obj = self.control_object(bucket_name, data_object_name)

        # ----------------------------------------
        # Connect to the cluster

        cluster = self.main_config(bucket_name)['clusters'][control_obj['cluster']]
        db_password = kms_decrypt(cluster['password']).strip()
        self.logger.debug('DB Password extracted OK')

        if '_conn' not in cluster:

            self.logger.debug('Connecting to cluster %s', control_obj['cluster'])
            cluster['_conn'] = redshift.connect(
                user=cluster['user'],
                password=db_password,
                host=cluster['host'],
                port=cluster['port'],
                database=cluster['database'],
                # ssl=cluster.get('ssl', False),
                ssl_context=True if cluster.get('ssl', False) else None,
            )
            self.logger.debug('Connected to cluster %s', control_obj['cluster'])
        else:
            self.logger.debug('Using existing connection to cluster %s', control_obj['cluster'])

        cursor = cluster['_conn'].cursor()
        schema = control_obj['schema']  # type: str
        table = control_obj['table']  # type: str

        # ----------------------------------------
        # Check if the target table exists.
        self.logger.debug('Checking if table %s.%s exists', schema, table)
        cursor.execute(
            """
            SELECT EXISTS (
                SELECT 1 FROM information_schema.tables
                WHERE  table_schema = '{}' AND table_name = '{}'
            );
            """.format(schema, table)
        )
        table_exists = cursor.fetchone()[0]

        # ----------------------------------------
        # Prepare the data table
        if control_obj['mode'] == 'truncate':
            if table_exists:
                self.logger.info('Truncating table %s.%s', schema, table)
                cursor.execute('DELETE FROM {}.{};'.format(schema, table))
                self.logger.debug('Truncated table %s.%s', schema, table)
        elif control_obj['mode'] == 'drop':
            if table_exists:
                self.logger.info('Dropping table %s.%s', schema, table)
                cursor.execute('DROP TABLE IF EXISTS {}.{};'.format(schema, table))
                self.logger.debug('Dropped table %s.%s', schema, table)
                table_exists = False

        if not table_exists:
            # Need to create the table
            columns = control_obj.get('columns')
            if not columns:
                raise Exception('Table {}.{} doesn\'t exist and no column spec provided to create it'.format(
                    schema, table))

            self.logger.info('Creating table %s.%s', schema, table)
            create_sql = 'CREATE TABLE {}.{} ({});'.format(schema, table, ', '.join(columns))
            self.logger.debug('SQL: %s', create_sql)
            cursor.execute(create_sql)
            self.logger.debug('Created table %s.%s', schema, table)

        # ----------------------------------------
        # Prepare the copy command.
        s3_credentials = self.get_copy_credentials(bucket_name)
        self.logger.debug('S3 credentials extracted ok')

        copy_cmd = """
                    COPY {schema}.{table} FROM 's3://{bucket}/{key}'
                    CREDENTIALS '{credentials}'
                    {params};
            """.format(
            schema=schema,
            table=table,
            bucket=bucket_name,
            key=data_object_name,
            credentials='{credentials}',  # Don't insert these yet
            params=' '.join(control_obj.get('copy-params', []))
        )
        # Take care to not log secret credentials
        self.logger.info(copy_cmd)
        copy_cmd = copy_cmd.format(credentials=s3_credentials)

        # ----------------------------------------
        self.logger.info('Running COPY command')
        cursor.execute(copy_cmd)
        cluster['_conn'].commit()
        self.logger.info('COPY complete')

    # --------------------------------------------------------------------------
    def get_copy_credentials(self, bucket_name):
        """
        Extract a CREDENTIALS string for use in a Redshift COPY command from the
        main config for the given bucket.  Must be either a role-arn key or
        access-key-id / access-secret-key pair.

        :param bucket_name:     Name of bucket
        :type bucket_name:      str
        :return:                A credentials string.
        :rtype:                 str
        :raise Exception:       If credentials missing or malformed.

        """

        s3c = self.main_config(bucket_name)['s3-credentials']

        if 'role-arn' in s3c:
            if 'access-key-id' in s3c:
                raise Exception('s3-credentials: Only one of role and access-key-id allowed')

            return s3c['role-arn']

        # Access keys instead of role. Need to decrypt the secret key
        try:
            access_secret_key = kms_decrypt(s3c['access-secret-key'],
                                            aws_session=self.aws_session).strip()
            return 'aws_access_key_id={};aws_secret_access_key={}'.format(
                s3c['access-key-id'], access_secret_key)
        except KeyError:
            raise Exception('s3-credentials: Must contain role-arn or access-key-id/access-secret-key')
        except Exception as e:
            # Probably decryption error
            raise Exception('s3-credentials: {}'.format(e))

# Evw Event Handler Plugin - rsDropLoader2

[TOC]

## Overview

**RsDropLoader2** is an event handler plugin for the
[evw](https://bitbucket.org/murrayandrews/evw) framework that loads a data file
dropped into AWS S3 into a Redshift table.

It is an alternative, and much more powerful, implementation of **rsDropLoader**
that provides the end user with more autonomy in the loading process and a range
of additional capabilities.

**RsDropLoader2** is a natural counterpart of
[rsUnloader](https://bitbucket.org/murrayandrews/rsunloader) which automates the
process of unloading data from Redshift to S3.

## Processing Flow

The basic processing flow is:

1.  User drops a [data file](#markdown-header-data-files) into S3.

    This results in an S3 _ObjectCreated_ event being sent to the
    **rsDropLoader2** handler.

1.  If the handler is running in AWS Lambda, it looks at the size of the data
    file and decides whether or not to perform handoff to a remote worker.

    The handoff size limit is controlled by the `RSDROPLOAD_HANDOFF_SIZE`
    [environment variable](#markdown-header-environment-variables). The
    rest of the flow is the same for Lambda based handlers and remote workers.

1.  The handler searches the S3 bucket for a YAML formatted
    [control file](#markdown-header-control-files) associated with the data file
    that defines how and where to load the data into Redshift (target cluster,
    schema and table and table structure).

    The user is responsible for creating the control file and ensuring it is in
    the right location in S3 prior to dropping the data file in S3.

1.  The handler looks up the target cluster in the
    [cluster table](#markdown-header-the-cluster-table) (a DynamoDB table) to
    obtain the connection details for the cluster (address, port, database etc.)

    The administrator is responsible for creating and populating the cluster
    table in DynamoDB.

1.  The handler looks for [credential files](#markdown-header-credential-files)
    in S3 containing access credentials for the target Redshift cluster and S3.

    Credential files are small YAML files. They can be user based, team based or
    global, depending on requirements. Sensitive data (e.g. passwords) are KMS
    encrypted.

1.  The handler establishes a connection to the target cluster.

    This processes uses the cluster connection information obtained from the
    DynamoDB cluster table as well as the cluster credentials.

1.  The handler creates the target table if necessary and optionally removes
    any existing data from the table.
    
    Elements in the control file specify the table structure and whether
    existing data should be replaced.
    
1.  The handler runs a Redshift COPY command to load the data from S3.

    This processes uses the details in the control file as well as the S3
    credentials.

1.  Optionally, the handler records the load event in an Aurora (MySQL) cluster.
    This information can be used to prevent a table from loading too frequently.

1.  The handler sends a result status message to the destinations
    specified in the control file.
    
    These destinations can be MSISDNs (via SMS), SNS topics,
    email addresses or SQS queues.

## Data Files

Data files contain structured, tabular data. Most of the formats supported
by the Redshift COPY command can be used, including:

* CSV files
* JSON files
* Bzip2 or Gzipped versions of the above.

The event triggers on the S3 drop load bucket must be set up to invoke the
Lambda function when the data files with recognised suffixes are deposited.

## Control Files

When a user drops a data file into S3, the **rsDropLoader2** event handler looks
for a [YAML](http://yaml.org) formatted control file for the given data file.

Control files specify where data files can be loaded in Redshift and the
target table schema. They can also constrain how large data files can be.

### Location

The control file can be at the same level in the bucket as the data file or at a
higher level in the tree in the same bucket. So, if a data file is named:

```
users/team1/fredbloggs/dataset2/z.dat
```

the control file is the first of the following to exist in the same bucket:

```
users/team1/fredbloggs/dataset2/z.yaml
users/team1/fredbloggs/dataset2.yaml
users/team1/fredbloggs.yaml
users/team1.yaml
users.yaml
```

The first option allows a one to one relationship between a data file and a
control file.

The other options allow a single control file to be used for a collection of
data files, all of which must have the same structure and destination in
Redshift. For example the control file:

```
users/team1/fredbloggs/dataset2.yaml
```

would be applied to all of the following data files:

```
users/team1/fredbloggs/dataset2/file1.dat
users/team1/fredbloggs/dataset2/file2.dat
users/team1/fredbloggs/dataset2/2015/dec/31/file.dat
```

The control file specifies an alias for the target Redshift cluster which must
exist as a key in the [cluster table](#markdown-header-the-cluster-table) in
DynamoDB.

### Contents

Control files are in [YAML](http://yaml.org) format and contain the following keys:

|Key|Required|Description|
|-|-|-|
|cluster|No|Alias for the target Redshift cluster. There must be a cluster with this name in the [cluster table](#markdown-header-the-cluster-table). If not specified, the value contained in the [RSDROPLOAD_CLUSTER](#markdown-header-environment-variables) environment variable is used. If neither are specified, the job is rejected.|
|columns|Sometimes|A list of SQL column specifications. Required if the target table must be created (e.g. if the table does not exist or the `mode` is `drop`). If specified, the COPY command will explicitly include the columns specified. This is useful when only some of the columns in the target table are to be loaded with data.|
|copy-params|Yes|A list of Redshift COPY command parameters appropriate for the data file. Most of the parameters supported by Redshift COPY, including MANIFEST, can be used.|
|load-period|No|A duration string that specifies that a table should not be loaded more frequently than the specified value. A duration string is an integer followed by one of `s` (seconds), `m` (minutes), `h` (hours), `d` (days), `w` (weeks). See [Managing Load State](#markdown-header-managing-load-state) below.|
|max-size|No|User supplied data files above this size will be rejected. Values are of the form `20K`, `20KB` or `20KiB`. (The example shows `K` for kilo. `B` for bytes, `M` for mega, `G` for giga etc. also work.) If not specified, only the cluster limit applies. Individual data files must pass both control file and cluster limit constraints.|
|mode|Yes|Update mode for the target table. See [Table Update Modes](#markdown-header-table-update-modes) for more information.|
|notify|No|Either a string or list of strings specifying [messenger](../messengers/README.md) destinations for status reports relating to the load (e.g. `ses://fred@gmail.com`). This key is deprecated - use `on-fail` / `on-success` / `on-retry` instead.|
|on-fail|No|A list of [actions](#markdown-header-post-load-actions) that are performed if the load fails.|
|on-retry|No|A list of [actions](#markdown-header-post-load-actions) that are performed if the load fails but the failure could be temporary and the load can be retried.|
|on-success|No|A list of [actions](#markdown-header-post-load-actions) that are performed if the load succeeds.|
|owner|Yes|Owner identification. Not used in the processing but required. Typically an email address.|
|schema-map|No|A dictionary used to map the schema name determined from the `schema` key to a final target schema name. All entries must be lower case. If not present then no mapping is performed.|
|schema|No|Target schema name. If not specified, the public schema is used. It is assumed that the Redshift administrator will have setup appropriate GRANTs to control where users are able to load data. If the value starts with `/`, it must be a regular expression with a single capture group. The regex is applied to the S3 object name that triggered the event (minus any suffix) and the value of the capture group is used as the schema name. The schema name is passed through the map defined using the `schema-map` key, if present, to obtain the final schema name. If the regex option is used, the `columns` key is not permitted and the target table must already exist.|
|table|Yes|Target table name. If the value starts with `/`, it must be a regular expression with a single capture group. The regex is applied to the S3 object name that triggered the event (minus any suffix) and the value of the capture group is used as the table name. If the regex option is used, the `columns` key is not permitted and the target table must already exist.|
|table-map|No|A dictionary used to map the table name determined from the `table` key to a final target table name. All entries must be lower case. If not present then no mapping is performed.|


The regex options for `schema` and `table`, coupled with the optional
`schema-map`, allow the target table to be derived at run time from the S3 object
name. This allows a single control file to be used for multiple tables provided
an appropriate object naming convention is used.


### Sample Control File

```yaml

owner: fred@wherever.com  # Job owner

# notify is deprecated -- use on-success / on-fail instead.
notify:
  - sns:arn:aws:sns:ap-southeast-2:123456789002:rsDropLoader2
  - fred@wherever.com
  - sms:61412312312

max-size: 5MiB  # If the file is bigger than this, its rejected

cluster: dropload
schema: fredschema
table: custard

# Column specs are required if the table needs to be created.
# Even if the table does not need to be created, listing columns
# will result in the column list being included in the COPY command.
columns:
  - ID INTEGER
  - CustardClubNo INTEGER
  - GivenName VARCHAR(20)
  - FamilyName VARCHAR(30)
  - Sex VARCHAR(1)
  - CustardBidPrice FLOAT(2)
  - CustardCode VARCHAR(10)
  - FavouriteCustard VARCHAR(10)
  - CustardJedi BOOLEAN
  - City VARCHAR
  - PostCode VARCHAR(20)
  - Email VARCHAR(100)
  - Phone VARCHAR(20)
  - LastCustard DATE
  - CustardQuota INTEGER

mode: delete 

copy-params:
  - BLANKSASNULL
  - BZIP2
  - CSV
  - EMPTYASNULL
  - IGNOREHEADER 1
  - MAXERROR 0

# See below for more information about on-* actions
on-success:
  - action: sqs
    message:
      schemaName: "{{ ctrl.schema }}"
      tableName: "{{ctrl.schema }}"
      loadTime: "{{ ctrl.eventTime }}"

# This one shows the other way to reference Jinja2 params.
on-fail:
  - action: notify
    to: ses://support@myorg.com
    subject: "Load failed for {{ctrl['schema']}}.{{ctrl['table']}}"
    message: |-
      This is an optional message -- Table {{ctrl['table']}} failed
      The problem was {{ctrl['statusInfo']}}.
    
```

## Post-load Actions

[Control files](#markdown-header-control-files) may contain any or all of the following
keys that specify a list of *actions* to be performed after the load has been attempted:

*   `on-success` - The load has succeeded.
*   `on-fail` - The load has failed and there is no point in retrying.
*   `on-retry` - The load has failed but may succeed later and is eligible for a
    retry. Intermittent communication errors, cluster reboots etc. tend to cause
    this condition.

These actions are only executed if the load is attempted. If an error occurs
prior to the load being attempted no post-load actions will be performed.

Each *action* is a dictionary consisting of:

1.  [Keys common to all actions](#markdown-header-keys-common-to-all-actions)
2.  [Keys specific to the given action type](#markdown-header-available-actions).

### Keys Common to All Actions

|Key|Required|Description|
|-|-|-|
|action|Yes|Specifies the type of action to perform. Allowed values are specified [below](#markdown-header-available-actions).|
|schema|No|If specified, the action is only performed if the target schema matches. If the value starts with `/`, the rest of the string is used as a regex to match against the target schema name instead of performing a literal match.|
|table|No|If specified, the action is only performed if the target table matches. If the value starts with `/`, the rest of the string is used as a regex to match against the target table name instead of performing a literal match.|


### Available Actions

This section lists the currently supported action types and the keys specific to
each.

A number of action parameters support rendering of their contents using 
[Jinja2](http://jinja.pocoo.org). The contents of the control file, augmented
with internally generated fields, are made available for the Jinja2 rendering in
the form of a dictionary named `ctrl`. See
[below](#markdown-header-rendering-of-action-parameters). Parameters that
support rendering with Jinja2 are indicated below.


#### notify

The `notify` action provides a preferred alternative to the `notify` control file
key. Unlike the latter, the `notify` action can differentiate between success
and failure and also customise the message contents.

|Key|Required|Jinja|Description|
|-|-|-|-|
|message|No|Yes|Message body.|
|subject|No|Yes|Message subject.|
|to|Yes|No|Either a string or list of strings specifying [messenger](../messengers/README.md) destinations for status reports relating to the load (e.g. `ses://fred@gmail.com`).|

At least one of `subject` and `message` must be provided as there are no default
values.


#### sns

The `sns` action sends a message to an SNS topic. Messages can be either normal
strings or full-formed JSON encoded objects.

|Key|Required|Jinja|Description|
|-|-|-|-|
|message|Yes|Yes|Either a string or an object that will be JSON encoded and sent as the SNS message.|
|subject|No|Yes|Message subject.|
|topic|Yes|No|The topic ARN.|

#### sqs

The `sqs` action sends a message to an SQS queue. Messages can be either normal
strings or full-formed JSON encoded objects.

|Key|Required|Jinja|Description|
|-|-|-|-|
|message|Yes|Yes|Either a string or an object that will be JSON encoded and sent as the SQS message.|
|queue|Yes|No|The queue name or URL.|

### Rendering of Action Parameters

A number of action parameters support rendering of their contents using using
[Jinja2](http://jinja.pocoo.org). The contents of the control file, augmented
with internally generated fields, are made available for the Jinja rendering
in the form of a dictionary named `ctrl`.

Normal Jinja2 syntax applies. So, for example, `{{ ctrl.table }}` or
`ctrl['table'] }}` would inject the value of the table name into a rendered
parameter.

In addition to whatever keys are contained in the control file itself,
the following items are made available (or redefined) in the `ctrl` dictionary
passed to the Jinja2 renderer.

|Key|Description|
|-|-|
|bucket|Name of the S3 bucket.|
|cluster|Name of the target Redshift cluster.|
|endTime|The UTC date-time when the load completed.|
|eventId|A unique identifier for the load event.|
|eventSource|Where the load request initiated. Typically `aws:s3`.|
|eventTime|The UTC date-time when the S3 event occurred.|
|objectSize|The size in bytes of the loaded object. Where a manifest is being loaded this is the size of the manifest, not the actual data.|
|object|The key in S3 for the object being loaded.|
|realTableName|The name of the actual table being loaded. When the [loading mode](#markdown-header-table-update-modes) is `switch`. This will be different from the value of the `table` key.|
|schema|The name of the schema to which the data is loaded.|
|schemaName|An alias for `schema`.|
|sourceIp|The IP address from which the S3 event was triggered.|
|startTime|The UTC date-time when the load started.|
|status|A string indicating the result of the load (e.g. `failed`, `completed` etc).|
|statusInfo|An error message if the load failed.|
|table|The name of the table to which the data is loaded. See also `schema` and `realTableName`.|
|tableName|An alias for `table`.|

## Managing Load State

If the `load-period` key is not specified in the job control file,
**rsDropLoader2** does not maintain any state information between load
activities. If a source data file is constantly updated in S3, then
**rsDropLoader2** will COPY it to Redshift every time it changes. This is not
always desirable as it can impose a significant load on Redshift and also
disrupt queries on the table.

If the `load-period` key is specified, **rsDropLoader2** maintains state
information in a companion Aurora (MySQL) cluster that records when each table
load started and finished. It will delay any request to load a table if a state
record indicates that another load commenced within the embargo period specified
by `load-period`.

### How it Works

Consider the following timing diagrams showing what happens when multiple files
are placed in S3 for the *same target table* over time. Assume the `load-period`
embargo is 10 characters long

```makefile
S3 File Drops: 1---2--3--
S3 Events:     1---2--3--
```

As each file is placed in S3, an event is generated and sent to an
**rsDropLoader2** event worker. Load 1 commences when the event is received and
a load embargo period also commences. In this diagram, load 1 is shown
as taking 4 time units to complete.

```makefile
S3 File Drops: 1---2--3--
S3 Events:     1---2--3--
Redshift Load: 1111------
Load Period:   **********
```

When events 2 and 3 are handed to an event worker, they are sent back to the
event queue until the load period is over. This may be some time after the load
period is over -- it depends on the SQS visibility timeout of the event queue.

```makefile
S3 File Drops: 1---2--3-------------
S3 Events:     1---2--3------2--3---
Redshift Load: 1111------
Load Period:   **********
```

Eventually the load period is over and event 2 is handed to an event worker. A
new load starts and a new load embargo period begins.

```makefile
S3 File Drops: 1---2--3----------------
S3 Events:     1---2--3------2--3------
Redshift Load: 1111----------2222------
Load Period:   **********----**********
```

At some time during the second load embargo period, event 3 will be handed to an
event worker. At this point it could be sent back to the queue again until the
latest embargo is over.

For fast moving tables that are frequently updated in S3, this will lead to a
growing *bow-wave* of events that get pushed further and further into the future
with a risk that some events will not be processed.

If the [table update mode](#markdown-header-table-update-modes) is not `append`,
the contents of the table are fully replaced on each load. As event 3 originally
occurred before the load for event 2 started, it is assumed that is superseded
and no longer required. Event 3 is then discarded. This prevents the event
*bow-wave* but does require some care in the selection of file names in S3. i.e.
It is best if each update uses the same file (or manifest) name in S3.

```makefile
S3 File Drops: 1---2--3----------------
S3 Events:     1---2--3------2--3------
Redshift Load: 1111----------2222------
Load Period:  **********----**********
Discard Event: -----------------3------
```

If the [table update mode](#markdown-header-table-update-modes) is `append`,
load embargo periods still apply but all events will lead to load attempts. Be
careful using this combination.


### Set up

To enable this capability, the following additional configuration elements are
required.

1.  An Aurora cluster must be available for state information.

    This is done in Aurora rather than Redshift itself as the latter is not
    suitable for transactional activity of this kind. (It probably should have
    been done in another DynamoDB table but it wasn't.)

1.  The Aurora cluster must contain a state table accessible to **rsDropLoader2**
    into which it writes state information.

1.  An additional entry for the Aurora cluster must be placed in the DynamoDB
    [cluster table](#markdown-header-the-cluster-table).

1.  The environment variable `RSDROPLOAD_LOGGING_CLUSTER` must be set to the
    name of the Aurora cluster entry in the cluster table.
    
1.  If using **eworker**, the `--retries` parameter should probably be set to `-1` or a
    a large enought value to allow events to be pushed forward in time far enough for
    for any `load-period` embargo to have expired. This has its own risks.

## Credential Files

The **rsDropLoader2** handler needs access to credentials to enable it to login
to the target Redshift cluster(s) and also to access S3 for the Redshift COPY
commands.

These credentials are located in small YAML files in S3. A separate file is
required for each target cluster and for S3 itself.

Credentials can be managed on a per user, per team or global basis, depending on
where in the S3 bucket prefix tree the credential files are placed.

### Location

Credential files must be placed either at the same location as the data object
or higher up the tree in the same bucket.

The S3 credentials file must be called `s3.cred`.

For Redshift clusters, the file must be named after the cluster alias with
`.cred` appended. i.e. for a cluster with an alias of `xyz`, the login
credentials file must be named `xyz.cred`.

### Contents - S3 Credentials

|Key|Required|Description|
|-|-|-|
|role-arn|Sometimes|An IAM role ARN. Required if `access-key-id`/`access-secret-key` are not provided.|
|access-key-id|Sometimes|An IAM access key ID. If `role-arn` is not used then both `access-key-id` and `access-secret-key` are required.|
|access-secret-key|Sometimes|A KMS encrypted IAM access secret key. This will be a long, ugly base64 encoded block of text. Any whitespace will be removed so this string can be safely split across multiple lines if desired. Note that the IAM role used by the **rsDropLoader2** handler will need permission to use whatever KMS key is used to encrypt the key. If `role-arn` is not used then both `access-key-id` and `access-secret-key` are required.|

### Contents - Redshift Clusters

|Key|Required|Description|
|-|-|-|
|user|Yes|Redshift cluster user name.|
|password|Yes|KMS encrypted password. This will be a long, ugly base64 encoded block of text. Any whitespace will be removed so this string can be safely split across multiple lines if desired. Note that the IAM role used by the **rsDropLoader2** handler will need permission to use whatever KMS key is used to encrypt passwords.|

## The Cluster Table

The _cluster table_ is a DynamoDB table that contains connection information for
one or more target Redshift and Aurora clusters.

The default table name is `evw.rsDropLoader2.clusters` but can be overridden
with the `RSDROPLOAD_CLUSTER_TABLE`
[environment variable](#markdown-header-environment-variables).

### Contents

Each item in the cluster table contains the following attributes.

|Name|Required|Type|Description|
|-|-|-|-|
|cluster|Yes|String|Cluster alias. This is the DynamoDB partition key.|
|ca-cert|Sometimes|String|The name of the file containing the CA certificate for Aurora clusters. Required for an Aurora cluster when SSL is enabled. Ignored by **rsDropLoader2** for Redshift clusters.|
|database|Yes|String|Database name within the cluster. Note that the term *database* has a different meaning for Redshift and Aurora (MySQL) clusters.|
|description|No|String|Cluster description. For information purposes only.|
|host|Yes|String|The cluster host name or IP address.|
|max-size|Yes|String|User supplied data files above this size will be rejected when loading to a Redshift cluster. Values are of the form `20K`, `20KB` or `20KiB`. (The example shows `K` for kilo. `B` for bytes, `M` for mega, `G` for giga etc. also work.). This attribute is ignored by **rsDropLoader2** for Aurora clusters.|
|password|Sometimes|String|Either the name of an EC2 SSM parameter containing a password or a KMS encrypted, base 64 encoded password. **RsDropLoader2** requires this to be specified for the Aurora logging cluster. It is not used for Redshift clusters.|
|port|No|Number|The cluster connection port. If not provided, **rsDropLoader2** assumes the standard Redshift port (5439) for Redshift clusters. It must be specified for an Aurora cluster.|
|ssl|No|Boolean|If `true`, SSL is forced on the connection. Defaults to `false`.|
|status|No|String|One of `active`, `disabled` or `paused`. Applies to Redshift clusters only. If `active`, the cluster is accepting loads. If `paused`, the cluster is not currently accepting loads but load requests will be requeued. If `disabled`, the cluster is not accepting loads and all load requests will be discarded. Defaults to `active`.|
|table|Sometimes|string|The name of the logging table in the Aurora cluster (without database/schema). Ignored for Redshift clusters.|
|type|Sometimes|string|The cluster type. Must be set to `mysql` for Aurora clusters. Ignored for Redshift clusters.|
|user|Sometimes|string|The user name for accessing the Aurora logging cluster. Ignored for Redshift clusters.|

A typical entry for a Redshift cluster looks like:


```json
{
  "cluster": "big-red",
  "database": "red_data",
  "description": "My Redshift cluster",
  "host": "10.20.30.40",
  "max-size": "2GB",
  "port": 5439,
  "ssl": true
}
```

A typical entry for the Aurora logging cluster looks like:


```json
{
  "ca-cert": "/usr/local/lib/rds/rds-ca-2015-root.pem",
  "cluster": "aurora-log",
  "database": "rsdroploader",
  "description": "Aurora cluster for rsDropLoader logging",
  "host": "my-aurora.cluster-fdjkfdjifh0a.ap-southeast-2.rds.amazonaws.com",
  "password": "/aurora-log/password",
  "port": 3306,
  "ssl": true,
  "table": "load_log",
  "type": "mysql",
  "user": "rsdroploader"
}
```

## Table Update Modes

The `mode` key in a [control file](#markdown-header-control-files) specifies how any existing data in the table
should be handled and how new data should be loaded. 

|Mode|Description|
|-|-|
|abort|If the target table contains any data, then abort with an error. This prevents the loss of any existing data.|
|append|New data is appended to any existing data in the table.|
|drop|The table is dropped and recreated prior to loading. This requires the `columns` key to be provided so **rsDropLoader** knows how to create the table.|
|delete|Delete all existing data from the table.|
|switch|Perform A/B table switching. If the `table` key specifies a target table of `mytable`, data will be alternately loaded into switch tables `mytable_a` and `mytable_b`. During the load phase, **rsDropLoader2** will find whichever switch table is empty and load the new data into it. It will then delete all data from the other table. All this is done inside a transaction. Once the transaction is committed, the empty table is truncated to minimise vacuuming requirements. If neither switch table is empty, the load is aborted. Generally there will be a view on top of the switch tables presenting the union of the two.|
|truncate|Deprecated. This is actually a synonym for `delete` for historical reasons. It is somewhat misleading in that it causes a DELETE FROM rather than a TRUNCATE on the target table. This mode will be removed or redefined in a future release.|

## Environment Variables

In addition to the standard `EVW*` environment variables, **rsDropLoader2** also
supports the following, all of which are optional.

|Name|Description|
|-|-|
|RSDROPLOAD_LOGGING_CLUSTER|The Aurora cluster to use for logging state information. This must be specified if a control file contains the `load-period` key.|
|RSDROPLOAD_CLUSTER|The default cluster to load to if the `cluster` key is not specified in a control file. This is handy to allow the same control file to be used to load into multiple target Redshift clusters.|
|RSDROPLOAD_CLUSTER_TABLE|Name of the _cluster table_ in DynamoDB. If not specified, the default value is derived from the plugin name and will be of the form `evw.rsDropLoader2.clusters`.|
|RSDROPLOAD_HANDOFF_SIZE|Data files above this size will be handed off to a remote worker. Values are of the form `20K`, `20KB` or `20KiB`. (The example shows `K` for kilo. `B` for bytes, `M` for mega, `G` for giga etc. also work.). Only used when **rsDropLoader2** is running in Lambda. Default is 0.|

## IAM Requirements

_In addition_ to the standard Lambda execution permissions that allow logging to
CloudWatch, the role assigned to the Lambda function will need IAM policies to
allow the following:

*   Use KMS to decrypt data
*   Read the _cluster table_ in DynamoDB.
*   Send messages to the SQS handoff queue.
*   Read credential and control files from the drop bucket(s) in S3.
*   Send messages to SNS, SES and SQS as required by the
    post load actions and [messenger](../messengers/README.md) plugins.



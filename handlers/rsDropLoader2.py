"""
evlambda/evworker plugin to load files dropped in S3 into Redshift.

It can use the following environment variables:

    RSDROPLOAD_HANDOFF_SIZE:
        A file size string (e.g. 100K or 100KB or 100KiB). Data files larger
        than this will be handed off to a remote worker. If not specified there
        is no handoff (unless forced by EVW_HANDOFF).

    RSDROPLOAD_CLUSTER_TABLE:
        Name of the cluster table in DynamoDB. If not specified, the default
        value is derived from the plugin name and will be of the form
        'evw.rsDropLoader2.clusters'. It is used to locate target Redshift
        clusters as well as Aurora logging clusters.

    RSDROPLOAD_CLUSTER:
        The default cluster to load to if the 'cluster' key is not specified in
        a control file.

    RSDROPLOAD_LOGGING_CLUSTER:
        The Aurora cluster to use for logging state information. This must be
        specified if a control file contains the 'load-period' key.

................................................................................
Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
................................................................................

"""

# TODO: Handle manifest size calculations properly


# ..............................................................................
# region imports

from __future__ import print_function

import datetime
import json
import os
import re
from ssl import SSLContext

try:
    # noinspection PyCompatibility
    from urllib.parse import unquote_plus
except ImportError:
    # noinspection PyUnresolvedReferences
    from urllib import unquote_plus

import dateutil.parser
import jinja2

# noinspection PyPackageRequirements
import pg8000 as redshift
# noinspection PyPackageRequirements
import pymysql
from botocore.client import Config

from common.aws_utils import kms_decrypt, s3_load_yaml, s3_search_yaml, \
    redshift_table_exists, get_ssm_param
from common.date import duration_to_seconds
from common.date import timedelta_to_hms
from common.event import EventProto, EventRetryException
from common.utils import size_to_bytes, dict_check, splitext2, json_default, \
    STRING_TYPES

# endregion imports
# ..............................................................................

# ..............................................................................
# region constants

__author__ = 'Murray Andrews'

PLUGIN_NAME = __name__.split('.')[-1]

SSL_CHECK_HOSTNAME = False  # Aaargh. Required with Python 2 and new RDS certs.

# Job status codes -- used for the logging cluster.
JOB_STARTING = 'starting'
JOB_COMPLETE = 'complete'
JOB_FAILED = 'failed'
JOB_SKIPPED = 'skipped'
JOB_DELAYED = 'delayed'
JOB_RETRY = 'retry'

# Regular expressions for various things

RE = {
    'schema': re.compile(r'^\w+$'),  # Schema names -- alphanumerics only
    'table': re.compile(r'^[\w#$]+$'),  # Allow # and $ characters in the table name
    'owner': re.compile(r'[^@]+@[^@]+\.[^@]+')  # A bit like an email address
}

# If an exception for a connect+copy matches any of these regexes (re.search)
# then a retry exception is raised instead of a permanent fail.
# WARNING : Some of these are Redshift driver dependent. Assuming pg8000.

RETRY_ERRORS = [
    re.compile(p, flags=re.I) for p in
    [
        'Server refuses SSL',  # Cluster is probably off-line for a while
        'unpack_from requires a buffer of at least',  # As above
        'Connection refused',  # Could be rebooting
        'database is in restore mode',
        'Operation timed out',
        'Serializable isolation violation on table',
        'could not complete because of conflict with concurrent transaction'
    ]
]

# Cluster status codes -- used for the data cluster.
CLUSTER_ACTIVE = 'on'  # Load requests can be processed
CLUSTER_PAUSED = 'paused'  # Load requests will not be processed but will queue.
CLUSTER_DISABLED = 'off'  # Load requests are discarded

# Control modes for the loading process: 'delete' and 'truncate' are synonyms,
# 'truncate' is misleading but retained for backward compatibility
CONTROL_MODES = {'truncate', 'drop', 'append', 'abort', 'switch', 'delete'}

CLUSTER_REQUIRED_KEYS = {'cluster', 'host', 'port', 'database'}
# CLUSTER_OPTIONAL_KEYS = {
#     'status', 'max-size', 'ssl', 'type', 'table', 'user', 'password', 'ca-cert',
#     'description'
# }

REDSHIFT_COPY_PARAMS = {
    # Note: NULL has to be specified as 'NULL AS' in the YAML file
    # Note: Credentials excluded - these are sourced from main config
    # Not supported: SSH, REGION
    'ACCEPTANYDATE', 'ACCEPTINVCHARS', 'AVRO', 'BLANKSASNULL', 'BZIP2',
    'COMPROWS', 'COMPUPDATE', 'CSV', 'DATEFORMAT', 'DELIMITER', 'EMPTYASNULL',
    'ENCODING', 'ENCRYPTED', 'ESCAPE', 'EXPLICIT_IDS', 'FILLRECORD',
    'FIXEDWIDTH', 'FORMAT', 'GZIP', 'IGNOREBLANKLINES', 'IGNOREHEADER',
    'JSON', 'LZOP', 'MANIFEST', 'MAXERROR', 'NOLOAD', 'NULL', 'READRATIO',
    'REMOVEQUOTES', 'ROUNDEC', 'STATUPDATE', 'TIMEFORMAT', 'TRIMBLANKS',
    'TRUNCATECOLUMNS',
}

CONTROL_REQUIRED_KEYS = {'cluster', 'owner', 'schema', 'table', 'mode'}
CONTROL_OPTIONAL_KEYS = {
    'max-size', 'copy-params', 'notify', 'columns', 'schema-map', 'load-period',
    'table-map', 'on-fail', 'on-success', 'on-retry'
}

# Files ending with any of the ignored suffixes are not processed. This
# Makes it much simpler to setup bucket event triggers.
IGNORE_SUFFIXES = {'.yaml', '.cred'}

DBLOG_FIELDS = (
    'eventId',
    'eventTime', 'startTime', 'endTime',
    'eventSource', 'sourceIp',
    'cluster', 'schemaName', 'realTableName', 'tableName',
    'status', 'statusInfo',
    'bucket', 'object'
)

DBLOG_FIELDS_CSV = ', '.join(DBLOG_FIELDS)


# endregion constants
# ..............................................................................


# ------------------------------------------------------------------------------
class Event(EventProto):
    """
    Handle a drop load event. These are S3 PUT events. The event record consists
    of a list of records (typically only one), each of which identifies a file
    dropped into S3 for loading into Redshift.

    For each dropped file, there must be control file which indicates how
    and where to perform the process of loading into Redshift. Each bucket
    must also have a main configuration file.

    Sample event record...

    {
        'Records': [
            {
                'eventSource': 'aws:s3',
                'eventName': 'ObjectCreated:Put',
                'eventVersion': '2.0',
                'userIdentity': {'principalId': 'AWS:AIDAJ3YYBPAYP22AQ3BNU'},
                'requestParameters': {'sourceIPAddress': '120.147.217.15'},
                'eventTime': '2016-09-24T06:02:55.886Z',
                's3': {
                    'configurationId': 'f48af588-2542-4a98-941d-eb828e291b28',
                    'object': {
                        'eTag': '6686853da3491a56c98917cc5c4ddea2',
                        'size': 5,
                        'sequencer': '0057E6170FC7ACE87B',
                        'key': 'dropbox/r.dat'
                    },
                    's3SchemaVersion': '1.0',
                    'bucket': {
                        'ownerIdentity': {'principalId': 'AVRD2C17Z6GLL'},
                        'arn': 'arn:aws:s3:::dev.canis2.com',
                        'name': 'dev.canis2.com'
                    }
                },
                'awsRegion': 'ap-southeast-2',
                'responseElements': {
                    'x-amz-request-id': '0C4C63F66A7E79E3',
                    'x-amz-id-2': 'XfW80wKuyFuv26mrre2uIh7NPxw/Zcl5P3+7Pl4E3ioMPHtOExUvmIgpoensUNbGLqsqmVrDbEw='
                }
            }
        ]
    }
    """

    # --------------------------------------------------------------------------
    def __init__(self, *args, **kwargs):

        super(Event, self).__init__(*args, **kwargs)

        # Caches for configs and control objects. We reload these for each new
        # event - deliberately as the event workers are long running and long
        # held caches can become stale.

        self._control_objects = {}  # Dict keyed on bucket/data_object
        self._clusters = {}  # Redshift clusters
        self._credentials = {}  # Credentials for Redshift and S3
        self._dblog_conn = None
        self.dblog_table = None

        # ----------------------------------------
        # Environment variables

        self.dyn_cluster_table = os.environ.get('RSDROPLOAD_CLUSTER_TABLE',
                                                f'evw.{PLUGIN_NAME}.clusters')
        self.default_cluster = os.environ.get('RSDROPLOAD_CLUSTER')
        self.logging_cluster = os.environ.get('RSDROPLOAD_LOGGING_CLUSTER')

        # ----------------------------------------
        for record in self.event['Records']:
            if not record['eventName'].startswith('ObjectCreated:'):
                raise Exception(f'Unexpected eventName: {record["eventName"]}')

    # --------------------------------------------------------------------------
    @property
    def dblog_conn(self):
        """
        Return a connection to the logging cluster. The logging cluster alias is
        specified using an environment variable RSDROPLOAD_LOGGING_CLUSTER. This is an
        alias for an entry in the clusters table. It has a few extra parameters
        compared to a Redshift cluster.

        The connection gets closed in self.close()


        :return:        Connection object. May be None if no logging cluster is
                        configured or a connection cannot be established.
        """

        if self._dblog_conn:
            return self._dblog_conn

        if not self.logging_cluster:
            self.logger.debug('No logging cluster configured')
            return None

        # ----------------------------------------
        # Get the cluster spec from DynamoDB and do some extra validation.

        cluster = self.cluster_info(self.logging_cluster)

        if not cluster:
            raise Exception(f'Cluster {self.logging_cluster}: unknown logging cluster')

        missing_fields = {'table', 'type', 'user', 'password'} - set(cluster)
        if missing_fields:
            raise Exception('Cluster {}: {} must be specified for logging cluster'.format(
                self.logging_cluster, ', '.join(sorted(missing_fields))))

        # We allow xxx or xxx.yyy as a table name
        m = re.match(r'^((?P<schema>\w+)\.)?(?P<table>\w+)$', cluster['table'])
        if not m:
            raise Exception(f'Cluster {self.logging_cluster}: invalid table name')

        # If the table includes a schema name we keep it otherwise use the database.
        # This is a kludge to handle the differences between mySQL and Postgres.
        self.dblog_table = cluster['table'] if m.group('schema') else cluster['database'] + '.' + m.group('table')

        # ----------------------------------------
        # Prepare to connect.

        # Common connect params.
        connect_params = {
            'user': cluster['user'],
            'host': cluster['host'],
            'port': cluster['port'],
            'database': cluster['database'],
            'connect_timeout': 5
        }

        # Get the password
        if cluster['password'].startswith('/'):
            # Get it from SSM
            connect_params['password'] = get_ssm_param(cluster['password'], aws_session=self.aws_session).strip()
        else:
            # Assume its a KMS encrypted string.
            connect_params['password'] = kms_decrypt(cluster['password'], aws_session=self.aws_session).strip()

        if cluster['type'].lower() == 'mysql':
            driver = pymysql
            # SSL handling is driver specific
            if cluster['ssl']:
                connect_params['ssl'] = {
                    'ca': cluster.get('ca-cert'),
                    'check_hostname': SSL_CHECK_HOSTNAME
                }
        else:
            raise Exception(
                f'Cluster {self.logging_cluster}:'
                f' type {cluster["type"]} not supported for logging cluster'
            )

        # ----------------------------------------
        # Connect to the cluster.

        try:
            self._dblog_conn = driver.connect(**connect_params)
        except Exception as e:
            self.logger.warning('Cannot connect to %s - %s', self.logging_cluster, str(e))
            self._dblog_conn = None

        self.logger.info('Cluster %s: connected to logging cluster', self.logging_cluster)

        return self._dblog_conn

    # --------------------------------------------------------------------------
    def dblog_store(self, logrec):
        """
        Log a data load to the logging cluster. This creates a record in a
        logging database/cluster that is separate from the data cluster. It is
        unrelated to the normal logger (self.logger).

        :param logrec:      The record to log as a dict of values. Superfluous
                            values are ignored.

        :type logrec:       dict[str, T]

        """

        conn = self.dblog_conn

        if not conn:
            self.logger.debug('Logging cluster not enabled')
            return

        sql = f'''
            INSERT INTO {self.dblog_table} ({DBLOG_FIELDS_CSV})
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        '''

        with conn.cursor() as cursor:
            cursor.execute(sql, tuple(logrec.get(f) for f in DBLOG_FIELDS))

            if hasattr(cursor, '_executed'):
                # noinspection PyProtectedMember
                self.logger.debug('Log SQL: %s', cursor._executed)

        conn.commit()

    # --------------------------------------------------------------------------
    def dblog_latest_load(self, cluster, schema, table):
        """
        Find the most recent load event record for the given table. We ignore
        skipped, delayed and failed loads.

        :param cluster:     Data cluster alias.
        :param schema:      Schema name
        :param table:       Table name

        :type cluster:      str
        :type schema:       str
        :type table:        str

        :return:    The record as a dict or None if no record can be found.
        :rtype:     dict[str, T]
        """

        conn = self.dblog_conn

        if not conn:
            return None

        sql = f'''
            SELECT {DBLOG_FIELDS_CSV}
            FROM {self.dblog_table}
            WHERE cluster = %s
            AND schemaname = %s
            AND tablename = %s
            AND status <> '{JOB_SKIPPED}'
            AND status <> '{JOB_DELAYED}'
            AND status <> '{JOB_FAILED}'
            ORDER BY logTime DESC
            LIMIT 1
        '''

        with conn.cursor() as cursor:
            cursor.execute(sql, (cluster, schema, table))
            row = cursor.fetchone()

        if not row:
            return None

        return dict(zip(DBLOG_FIELDS, row))

    # --------------------------------------------------------------------------
    def finish(self):
        """
        The last word.
        """

        if self._dblog_conn:
            self._dblog_conn.close()

    # --------------------------------------------------------------------------
    def handoff(self):
        """
        Determine if the event could be handed off to an event worker. For this
        implementation the following events can handed off:

            -   All events containing multiple records
            -   Single record events above the size specifed by the
                RSDROPLOAD_HANDOFF_SIZE environment variable.

        :return:    True if the event can be handed off or False otherwise.
        :rtype:     bool

        """

        if len(self.event['Records']) > 1:
            # If multiple records to process, hand event to an event worker.
            return True

        # If we have a single file to load, check its size.
        record = self.event['Records'][0]  # type: dict
        object_size = record['s3']['object']['size']  # type: int

        worker_size = os.environ.get('RSDROPLOAD_HANDOFF_SIZE')

        return worker_size is not None and object_size >= size_to_bytes(worker_size)

    # --------------------------------------------------------------------------
    def do_record(self, record):
        """
        Process an S3 event record which specifies a file (or manifest of files)
        to load to Redshift.

        :param record:  S3 event record
        :type record:   dict
        """

        bucket_name = record['s3']['bucket']['name']  # type: str
        object_name = record['s3']['object']['key']  # type: str

        object_size = record['s3']['object']['size']  # type: int

        if not object_size:
            self.logger.info('Skipping empty file %s/%s ', bucket_name, object_name)
            return

        control_obj = self.control_object(bucket_name, object_name)

        cluster = control_obj['cluster']  # type: str
        cluster_info = self.cluster_info(cluster)

        if cluster_info['status'] == CLUSTER_PAUSED:
            # Requeue all load requests.
            raise EventRetryException(f'Redshift cluster {cluster} is paused')
        elif cluster_info['status'] == CLUSTER_DISABLED:
            # Drop all load requests
            self.logger.warning(f'Redshift cluster {cluster} is disabled')
            return

        # ----------------------------------------
        # Augment the control object -- used for the logging cluster. Field names
        # need to align with the logging table def

        control_obj['bucket'] = bucket_name
        control_obj['endTime'] = None
        control_obj['eventId'] = self.event_id
        control_obj['eventSource'] = record['eventSource']
        control_obj['eventTime'] = dateutil.parser.parse(record['eventTime']).replace(tzinfo=None)
        control_obj['object'] = object_name
        control_obj['objectSize'] = object_size
        control_obj['realTableName'] = None  # Don't know yet
        control_obj['schemaName'] = control_obj['schema']
        control_obj['sourceIp'] = record.get('requestParameters', {}).get('sourceIPAddress')
        control_obj['startTime'] = datetime.datetime.utcnow()
        control_obj['status'] = JOB_STARTING
        control_obj['tableName'] = control_obj['table']

        # ----------------------------------------
        # Check if we are trying to load again too soon

        if control_obj['load-period-s']:
            # Find when we last started a load of the target table
            last_log_rec = self.dblog_latest_load(cluster, control_obj['schema'], control_obj['table'])
            self.logger.debug('Previous log record: %s', str(last_log_rec))

            if last_log_rec:
                time_since_last_load = (control_obj['startTime'] - last_log_rec['startTime']).total_seconds()

                self.logger.debug('Last load of %s.%s was %d seconds ago',
                                  control_obj['schema'], control_obj['table'], time_since_last_load)

                if time_since_last_load < control_obj['load-period-s']:
                    # We are in a load embargo period for the table. If this is
                    # an append operation, or a replace operation that is not
                    # superseded, we have to requeue it for later. An event is
                    # superseded if it occurred before the most recent load started.
                    # This stops a bow-wave of events pushing forward.

                    self.logger.debug('Load embargo period active for %s.%s',
                                      control_obj['schema'], control_obj['table'])

                    if control_obj['mode'] == 'append' or last_log_rec['startTime'] < control_obj['eventTime']:
                        # We have to do this load but not now
                        control_obj['status'] = JOB_DELAYED
                        control_obj['statusInfo'] = 'Delayed due to previous load at {} (eventId {})'.format(
                            last_log_rec['startTime'], last_log_rec['eventId'])
                    else:
                        # We are going to skip this load. Assumed to be superseded.
                        self.logger.info('Skipping new load of %s.%s ',
                                         control_obj['schema'], control_obj['table'])
                        control_obj['endTime'] = control_obj['startTime']
                        control_obj['status'] = JOB_SKIPPED
                        control_obj['statusInfo'] = 'Skipped due to previous load at {} (eventId {})'.format(
                            last_log_rec['startTime'], last_log_rec['eventId']
                        )

        self.dblog_store(logrec=control_obj)

        if control_obj['status'] == JOB_SKIPPED:
            return
        elif control_obj['status'] == JOB_DELAYED:
            raise EventRetryException(
                f'{control_obj["schema"]}.{control_obj["table"]}: {control_obj["statusInfo"]}')

        # ----------------------------------------
        # Now we have all that out of the way we can start the load.

        notify = control_obj.get('notify', [])

        try:
            # ----------------------------------------
            # Make sure its not too big. Two things to check (1) cluster limit
            # and (2) job size limit.

            if cluster_info['_max-size'] is not None and object_size > cluster_info['_max-size']:
                raise Exception('{}/{}: Size {} exceeds maximum size for cluster {} of {}'.format(
                    bucket_name, object_name, object_size, cluster, cluster_info['max-size']))

            if control_obj['_max-size'] is not None and object_size > control_obj['_max-size']:
                raise Exception('{}/{}: Size {} exceeds maximum size for job of {}'.format(
                    bucket_name, object_name, object_size, control_obj['max-size']))

            # ----------------------------------------
            # Do the Redshift COPY

            self.redshift_copy(bucket_name, object_name)

        except EventRetryException as e:
            # Temporary fail (maybe)
            control_obj['status'] = JOB_RETRY
            control_obj['endTime'] = datetime.datetime.utcnow()
            control_obj['statusInfo'] = str(e)[:256]

            # Run any load retry actions
            try:
                self.do_actions('on-retry', record)
            except Exception as ex:
                self.logger.warning('do_action: on-retry: %s', str(ex))

            try:
                # Deprecated
                self.send_msg(
                    to=notify,
                    subject=f'{PLUGIN_NAME} retry for {bucket_name}/{object_name}',
                    message=f'{PLUGIN_NAME} retry for {bucket_name}/{object_name}\n\n'
                    f'The following (temporary?) error occurred: {e}'
                )
            except Exception as ex:
                # Send failed - log it
                self.logger.warning('send_msg: %s', str(ex))
            raise
        except Exception as e:
            # Failed
            control_obj['status'] = JOB_FAILED
            control_obj['endTime'] = datetime.datetime.utcnow()
            control_obj['statusInfo'] = str(e)[:256]

            # Run any load fail actions
            try:
                self.do_actions('on-fail', record)
            except Exception as ex:
                self.logger.warning('do_action: on-fail: %s', str(ex))

            try:
                # Deprecated
                self.send_msg(
                    to=notify,
                    subject=f'{PLUGIN_NAME} failed for {bucket_name}/{object_name}',
                    message=f'{PLUGIN_NAME} failed for {bucket_name}/{object_name}\n\n'
                    f'The following error occurred: {e}')
            except Exception as ex:
                # Send failed - log it
                self.logger.warning(f'send_msg: {ex}')
            raise
        else:
            # Success
            control_obj['status'] = JOB_COMPLETE
            control_obj['endTime'] = datetime.datetime.utcnow()
            control_obj['elapsed'] = timedelta_to_hms(control_obj['endTime'] - control_obj['startTime'])
            control_obj['statusInfo'] = 'Load complete'

            # Run any post load actions
            try:
                self.do_actions('on-success', record)
            except Exception as ex:
                self.logger.warning('do_action: on-success: %s', str(ex))

            try:
                self.send_msg(
                    to=notify,
                    subject='{} completed ok for {}/{}'.format(PLUGIN_NAME, bucket_name, object_name),
                    message='{} completed ok for {}/{}\n\n'
                            'Start time: {}Z\nEnd time: {}Z\nElapsed: {}h {}m {}s'.format(
                        PLUGIN_NAME, bucket_name, object_name,
                        control_obj['startTime'].isoformat(), control_obj['endTime'].isoformat(),
                        *control_obj['elapsed']
                    )
                )
            except Exception as ex:
                # Send failed - log it but don't report the load as a failure
                self.logger.warning('send_msg: %s', str(ex))
        finally:
            self.dblog_store(logrec=control_obj)

    # --------------------------------------------------------------------------
    def process(self):
        """
        Process the event.
        """

        for record in self.event['Records']:
            # Process individual records. Abort on first failure

            # S3 events have the object key URL encoded -- fix it.
            # The string cast is a bit dangerous.
            record['s3']['object']['key'] = str(unquote_plus(record['s3']['object']['key']))

            object_name = record['s3']['object']['key']  # type: str

            # splitext is OK here because we only want the last dotted component
            # When getting the control file, splitext is not OK because we
            # need everything after the first dot in the basename.
            if os.path.splitext(object_name)[1].lower() in IGNORE_SUFFIXES:
                self.logger.info('Ignoring %s', object_name)
                continue

            self.do_record(record)

    # --------------------------------------------------------------------------
    def control_object(self, bucket_name, data_object_name):
        """
        Retrieve the control object for the given data object. This is contained
        in a YAML file with the same basename as the data object (or at a higher
        level in the tree).  So if the original object key is:

            users/fredbloggs/project1/z.dat

        the control file is the first of these to exist.

            users/fredbloggs/project1/z.yaml
            users/fredbloggs/project1.yaml
            users/fredbloggs.yaml
            users.yaml

        in the same bucket. The control file specifies the target schema and
        table and also the column structure.

        A control file generally applies to a single target table. However, if
        the "table" and/or "schema" keys start with "/" the value (excluding the
        leader /) is used as a regular expression on the object key (excluding
        the bucket name and suffix). The first capture group is used as the
        value for the parameter (after converstion to lower case.).  This allows
        one control file to handle multiple tables but in this case the
        "columns" key is not permitted and hence the target table must already
        exist in the datebase. For example if the object key is 'x/y/TABLE_FRED.bz2'
        and the table key is specified as '/TABLE_(.*)$' then the table name
        will be set to 'fred'.

        In addition:

        *   schema names are subject to mapping through the table
            provided in the 'schema-map' key.

        *   table names are subject to mapping through the table
            provided in the 'table-map' key.

        If the control file occurs higher up in the tree it allows multiple data
        files with the same structure but arbitrary names to be loaded.

        Results are cached so it's efficient to call multiple times.

        This method also does some validation of fields.

        :param bucket_name:         Bucket name.
        :param data_object_name:    Data object name.

        :type bucket_name:          str
        :type data_object_name:     str

        :return:                    The control object.
        :rtype:                     dict

        :raise IOError:             If the control object can't be found
        :raise Exception:           If the control object has format problems
                                    or doesn't pass some basic format checks.
        """

        object_name = bucket_name + '/' + data_object_name

        # ----------------------------------------
        # Try for a cached entry
        try:
            return self._control_objects[object_name]
        except KeyError:
            pass

        # ----------------------------------------
        # No cached entry - load from S3.

        # ----------------------------------------
        # Search for a control file. These are in the same tree as the data
        # file.  The control file can be at the same depth as the data file or
        # higher up so need to search from low to high.

        # Note that we do not use os.path.splitext as it won't handle double
        # suffixes properly (e.g. .csv.bz2).

        data_object_nosuffix = splitext2(data_object_name, pathsep='/', extsep='.')[0]
        control_path = data_object_nosuffix.split('/')
        control_file_name = None
        control_obj = None

        for n in range(len(control_path), 0, -1):
            control_file_name = '/'.join(control_path[0:n]) + '.yaml'
            self.logger.debug('Control search: %s/%s', bucket_name, control_file_name)

            try:
                control_obj = self._control_objects[object_name] = s3_load_yaml(
                    bucket_name, control_file_name, aws_session=self.aws_session)
            except IOError:
                # Assume this file doesn't exist and keep going up the tree
                pass
            except ValueError as e:
                # File exists but bad YAML - can't continue
                raise Exception(f'{bucket_name}/{control_file_name}: Bad control file: {e}')
            else:
                # control file load succeeded
                break

        if not control_obj:
            # Got all the way up the tree with no luck
            raise IOError(f'{object_name}: Cannot locate a control file')

        # ----------------------------------------
        # Now have a control object

        # ----------------------------------------
        # If target cluster is not specified, there must be a default.
        if not control_obj.get('cluster') and self.default_cluster:
            control_obj['cluster'] = self.default_cluster

        # ----------------------------------------
        # Check control object contains the right fields

        try:
            dict_check(control_obj, CONTROL_REQUIRED_KEYS, CONTROL_OPTIONAL_KEYS)
        except ValueError as e:
            raise Exception(f'{bucket_name}/{control_file_name}: Bad control file: {e}')

        control_obj['_max-size'] = \
            size_to_bytes(control_obj['max-size']) if 'max-size' in control_obj else None

        # ----------------------------------------
        # Extract and validate target shema and table
        for key in 'table', 'schema':
            # Look for search patterns in table and schema names
            if control_obj[key].startswith('/'):
                if control_obj.get('columns'):
                    raise Exception(f'Cannot specify columns if {key} specified with regex')

                pat = control_obj[key][1:]  # Remove leading /

                self.logger.debug('Matching object name %s against pattern %s to get %s',
                                  data_object_nosuffix, pat, key)

                m = re.search(pat, data_object_nosuffix)
                if not m:
                    raise Exception(f'Object name {data_object_nosuffix} does not match {key} regex')
                try:
                    control_obj[key] = m.group(1).lower()
                    self.logger.debug('Setting %s to %s', key, control_obj[key])
                except IndexError:
                    raise Exception(f'Regex for {key} does not contain a capture group')
            control_obj[key] = control_obj[key].lower()

            # Process schema and table through schema and table map respectively
            # and validate them. Note that the maps must be in lower case.
            vmap = control_obj.get(key + '-map', {})
            try:
                new_value = vmap[control_obj[key]]
            except KeyError:
                # No mapping for this value
                pass
            else:
                self.logger.debug('Mapping %s %s to %s', key, control_obj[key], new_value)
                control_obj[key] = new_value

            # Validate the final value
            if not isinstance(control_obj[key], STRING_TYPES) \
                    or not RE[key].match(control_obj[key]):
                raise Exception(f'Bad {key} name {control_obj[key]}')

        # ----------------------------------------
        # Validate owner - very basic format check on email address
        if not isinstance(control_obj['owner'], STRING_TYPES) \
                or not RE['owner'].match(control_obj['owner']):
            raise Exception(f'Bad owner email address: {control_obj["owner"]}')

        # ----------------------------------------
        # Validate the database update mode
        if not isinstance(control_obj['mode'], STRING_TYPES) \
                or control_obj['mode'] not in CONTROL_MODES:
            raise Exception(f'Bad mode: {control_obj["mode"]}')

        # ----------------------------------------
        # Validate notify target
        if 'notify' in control_obj and not isinstance(control_obj['notify'], (STRING_TYPES, list)):
            raise Exception('Bad notify: must be string or list of strings')

        # ----------------------------------------
        # Validate the Redshift COPY params (basic health check)
        if control_obj['copy-params']:
            if not isinstance(control_obj['copy-params'], list):
                raise Exception('copy-params must be a list')

            # Get a normalised list of all keywords
            copy_params = {cp.split()[0].upper() for cp in control_obj['copy-params']}

            bad_params = copy_params - REDSHIFT_COPY_PARAMS
            if bad_params:
                raise Exception('Invalid copy-params: {}'.format(', '.join(sorted(bad_params))))

        control_obj['load-period-s'] = duration_to_seconds(control_obj.get('load-period', 0))

        return control_obj

    # --------------------------------------------------------------------------
    def cluster_info(self, cluster):
        """
        Returns a cluster definition. These are located in a DynamoDB table
        evw.WORKER.clusters. Results are cached. This is used both for Redshift
        data clusters and also load logging clusters.

        :param cluster:     Cluster alias. These are referenced from data load
                            control files.
        :type cluster:      str

        :return:            The cluster definition structure.
        :rtype:             dict

        :raise Exception:   If the cluster spec cannot be retrieved.
        """

        if cluster not in self._clusters:
            try:
                dyn_table = self.aws_session.resource('dynamodb').Table(self.dyn_cluster_table)
                cluster_info = dyn_table.get_item(Key={'cluster': cluster})['Item']  # type: dict
            except Exception as e:
                raise Exception(f'Cluster {cluster}: Cannot get info from DynamoDB - {e}')

            # ----------------------------------------
            # Check the cluster specs in the main config. Ignore the optional keys
            try:
                dict_check(cluster_info, required=CLUSTER_REQUIRED_KEYS)
            except Exception as e:
                raise Exception(f'Cluster {cluster}: {e}')

            cluster_info['_max-size'] = size_to_bytes(cluster_info['max-size']) \
                if 'max-size' in cluster_info else None

            cluster_info['port'] = int(cluster_info['port'])

            cluster_info.setdefault('status', CLUSTER_ACTIVE)
            if cluster_info['status'] not in (CLUSTER_ACTIVE, CLUSTER_PAUSED, CLUSTER_DISABLED):
                raise Exception(f'Cluster {cluster}: Bad status {cluster_info["status"]}')

            self._clusters[cluster] = cluster_info

        return self._clusters[cluster]

    # --------------------------------------------------------------------------
    def cluster_credentials(self, cluster, bucket_name, data_object_name):
        """
        Locate cluster login credentials for the given target data cluster.
        These are in YAML formatted files in S3, either at the same location as
        the data object or higher up the tree. They are named after the cluster
        with a .cred suffix.  This allows credentials to be managed at a user,
        team or global level.

        The file must contain 'user' and 'password' keys (only).

        :param cluster:             Cluster alias. This is used to search for a
                                    cluster credentials file. If the cluster
                                    alias is xyz, the credentials must be in a
                                    file xyz.cred in the same location as the
                                    data_object_name or at a higher level in the
                                    tree.
        :param bucket_name:         Bucket name.
        :param data_object_name:    Data object name.

        :type cluster:              str
        :type bucket_name:          str
        :type data_object_name:     str

        :return:                    A tuple (user-name, password).
        :rtype:                     (str, str)

        """

        data_path = data_object_name.split('/')

        try:
            cred_file, credentials = s3_search_yaml(
                bucket_name=bucket_name,
                prefix='/'.join(data_path[0:-1]),
                filename=cluster + '.cred',
                aws_session=self.aws_session
            )
        except Exception as e:
            raise Exception(f'Cannot load credentials for cluster {cluster}: {e}')

        try:
            dict_check(credentials, required={'user', 'password'})
        except ValueError as e:
            raise Exception(f'{cred_file}: Bad cluster credentials - {e}')

        return credentials['user'], kms_decrypt(credentials['password']).strip()

    # --------------------------------------------------------------------------
    def redshift_copy_simple(self, conn, bucket_name, data_object_name, s3_credentials):
        """
        Handles copy modes truncate/delete, drop, append and abort.

        :param conn:                Database connection.
        :param bucket_name:         Bucket name.
        :param data_object_name:    Data object name.
        :param s3_credentials:      S3 credentials required by Redshift to do
                                    COPY

        :type bucket_name:          str
        :type data_object_name:     str
        :type conn:                 redshift.core.Connection
        :type s3_credentials:       str
        """

        control_obj = self.control_object(bucket_name, data_object_name)
        cursor = conn.cursor()
        schema = control_obj['schema']  # type: str
        table = control_obj['table']  # type: str
        control_obj['realTableName'] = table

        # ----------------------------------------
        # Check if the target table exists.
        self.logger.debug('Checking if table %s.%s exists', schema, table)
        table_exists = redshift_table_exists(cursor, schema, table)

        if table_exists and control_obj['mode'] == 'abort':
            raise Exception(f'Table {schema}.{table} already exists')

        # ----------------------------------------
        # Prepare the data table
        if control_obj['mode'] in ('truncate', 'delete'):
            if table_exists:
                self.logger.info('Deleting table %s.%s', schema, table)
                cursor.execute(f'DELETE FROM {schema}.{table};')
                self.logger.debug('Deleted table %s.%s', schema, table)
        elif control_obj['mode'] == 'drop':
            if table_exists:
                self.logger.info('Dropping table %s.%s', schema, table)
                cursor.execute(f'DROP TABLE IF EXISTS {schema}.{table};')
                self.logger.debug('Dropped table %s.%s', schema, table)
                table_exists = False

        columns = control_obj.get('columns')
        if not table_exists:
            # Need to create the table
            if not columns:
                raise Exception(
                    f'Table {schema}.{table} doesn\'t exist and no column spec provided to create it')

            self.logger.info('Creating table %s.%s', schema, table)
            create_sql = f'CREATE TABLE {schema}.{table} ({", ".join(columns)});'
            self.logger.debug('SQL: %s', create_sql)
            cursor.execute(create_sql)
            self.logger.debug('Created table %s.%s', schema, table)

        # ----------------------------------------
        # Prepare the copy command. If a column spec is provided use that to specify columns.
        copy_cmd = """
                        COPY {schema}.{table}{columns} FROM 's3://{bucket}/{key}'
                        CREDENTIALS '{credentials}'
                        {params};
                """.format(
            schema=schema,
            table=table,
            columns='(' + ','.join([c.split(' ', 1)[0] for c in columns]) + ')' if columns else '',
            bucket=bucket_name,
            key=data_object_name,
            credentials='{credentials}',  # Don't insert these yet
            params=' '.join(control_obj.get('copy-params', []))
        )
        # Take care to not log secret credentials
        self.logger.info(copy_cmd)
        copy_cmd = copy_cmd.format(credentials=s3_credentials)

        # ----------------------------------------
        self.logger.info('Running COPY command')
        cursor.execute(copy_cmd)
        self.logger.info('COPY to %s.%s complete', schema, table)
        conn.commit()
        self.logger.info('Commit %s.%s', schema, table)

    # --------------------------------------------------------------------------
    def redshift_copy_switch(self, conn, bucket_name, data_object_name, s3_credentials):
        """
        Handles copy mode switch which alternates between two A and B tables.

        If the user specifies table name x then the actual tables that are used
        are x_a and x_b. One of them must be empty.

        :param conn:                Database connection.
        :param bucket_name:         Bucket name.
        :param data_object_name:    Data object name.
        :param s3_credentials:      S3 credentials required by Redshift to do
                                    COPY

        :type bucket_name:          str
        :type data_object_name:     str
        :type conn:                 redshift.core.Connection
        :type s3_credentials:       str
        """

        control_obj = self.control_object(bucket_name, data_object_name)
        cursor = conn.cursor()
        schema = control_obj['schema']  # type: str
        table = control_obj['table']  # type: str

        # ----------------------------------------
        # Check if the target tables exist and that one of them is empty.

        table_new = None  # Data will get loaded in here
        table_old = None  # This one will be emptied after
        columns = control_obj.get('columns')

        for new, old in ('a', 'b'), ('b', 'a'):
            t = table + '_' + new

            self.logger.debug('Checking if table %s.%s exists', schema, t)
            if not redshift_table_exists(cursor, schema, t):
                # Need to create the table
                if not columns:
                    raise Exception(f'Table {schema}.{t} doesn\'t exist and no column spec provided to create it')

                self.logger.info('Creating table %s.%s', schema, t)
                create_sql = f'CREATE TABLE {schema}.{t} ({", ".join(columns)});'
                self.logger.debug('SQL: %s', create_sql)
                cursor.execute(create_sql)
                self.logger.debug('Created table %s.%s', schema, t)

            if not table_new:
                cursor.execute(f'SELECT COUNT(*) FROM {schema}.{t};')
                if cursor.fetchone()[0] == 0:
                    self.logger.debug('Table %s.%s is empty - setting as load target', schema, t)
                    table_new = t
                    table_old = table + '_' + old

        if not table_new:
            raise Exception(f'Neither A nor B switch table is empty for {schema}.{table}')

        # At this point the A and B tables exist and table_new is empty.

        control_obj['realTableName'] = table_new

        # ----------------------------------------
        # Prepare the copy command. If a column spec is provided use that to specify columns.

        copy_cmd = """
                        COPY {schema}.{table}{columns} FROM 's3://{bucket}/{key}'
                        CREDENTIALS '{credentials}'
                        {params};
                """.format(
            schema=schema,
            table=table_new,
            columns='(' + ','.join([c.split(' ', 1)[0] for c in columns]) + ')' if columns else '',
            bucket=bucket_name,
            key=data_object_name,
            credentials='{credentials}',  # Don't insert these yet
            params=' '.join(control_obj.get('copy-params', []))
        )
        # Take care to not log secret credentials
        self.logger.info(copy_cmd)
        copy_cmd = copy_cmd.format(credentials=s3_credentials)

        # ----------------------------------------
        self.logger.info('Running COPY command for %s.%s', schema, table_new)
        cursor.execute(copy_cmd)
        self.logger.info('COPY to %s.%s complete', schema, table_new)

        # ----------------------------------------
        # Now empty the old table.
        self.logger.info('Deleting table %s.%s', schema, table_old)
        cursor.execute(f'DELETE FROM {schema}.{table_old};')
        self.logger.debug('Deleted table %s.%s', schema, table_old)

        # ----------------------------------------
        # Commit the changes and then TRUNCATE the old table. Note that TRUNCATE
        # also commits any transaction of which it is a part.
        conn.commit()
        self.logger.info('Commit %s.%s', schema, table)

        self.logger.info('Truncating table %s.%s', schema, table_old)
        cursor.execute(f'TRUNCATE {schema}.{table_old};')
        self.logger.debug('Truncated table %s.%s', schema, table_old)

    # --------------------------------------------------------------------------
    def redshift_copy(self, bucket_name, data_object_name):
        """
        Do the Redshift COPY, including any preparatory drop or delete.

        :param bucket_name:         Bucket name.
        :param data_object_name:    Data object name.

        :type bucket_name:          str
        :type data_object_name:     str

        :raise RetryException:      If the load failed and it is worth retrying.
        :raise Exception:           If the load failed and there is no point in
                                    retrying.

        """

        self.logger.debug('Preparing for Redshift COPY')

        control_obj = self.control_object(bucket_name, data_object_name)

        # ----------------------------------------
        # Get cluster and S3 credentials
        cluster_alias = control_obj['cluster']
        cluster = self.cluster_info(cluster_alias)

        user, password = self.cluster_credentials(cluster_alias,
                                                  bucket_name,
                                                  data_object_name)
        self.logger.debug('Cluster credentials extracted OK')

        s3_credentials = self.get_copy_credentials(bucket_name, data_object_name)
        self.logger.debug('S3 credentials extracted ok')

        # ----------------------------------------
        # Connect to the cluster
        conn = None

        ssl_context = SSLContext() if cluster.get('ssl', False) else None
        if ssl_context:
            self.logger.debug('Loading default SSL certs')
            ssl_context.load_default_certs()

        try:
            self.logger.debug('Connecting to cluster %s', cluster_alias)
            conn = redshift.connect(
                user=user,
                password=password,
                host=cluster['host'],
                port=cluster['port'],
                database=cluster['database'],
                # ssl=cluster.get('ssl', False),
                ssl_context=ssl_context
            )
            self.logger.debug('Connected to cluster %s', cluster_alias)

            if control_obj['mode'] in {'truncate', 'drop', 'append', 'abort', 'delete'}:
                self.redshift_copy_simple(conn, bucket_name, data_object_name, s3_credentials)
            elif control_obj['mode'] == 'switch':
                self.redshift_copy_switch(conn, bucket_name, data_object_name, s3_credentials)
            else:
                raise Exception(f'Bad mode: {control_obj["mode"]}')

        except Exception as e:
            if conn:
                conn.rollback()
                self.logger.warning('Rollback')

            # Check if the error is suitable for a retry
            e_msg = str(e)  # Cannot rely on e.message :-(
            for pattern in RETRY_ERRORS:
                if pattern.search(e_msg):
                    self.logger.warning('Possible intermittent error on cluster %s - %s',
                                        cluster_alias, e_msg)
                    raise EventRetryException(e_msg + ' (eligible for retry)')
            raise
        finally:
            if conn:
                self.logger.debug('%s: Closing connection to cluster %s',
                                  bucket_name, cluster_alias)
                try:
                    conn.close()
                except Exception as e:
                    # Log it and move on
                    self.logger.warning(
                        'Attempt to close connection to cluster %s failed - %s',
                        cluster_alias, str(e)
                    )

    # --------------------------------------------------------------------------
    def get_copy_credentials(self, bucket_name, data_object_name):
        """
        Locate S3 access credentials and use these to create a a CREDENTIALS
        string for use in a Redshift COPY command from the main config for the
        given bucket.  Must be either a role-arn key or access-key-id /
        access-secret-key pair.

        These are in YAML formatted files in S3, either at the same location as
        the data object or higher up the tree. They are named 's3.cred'.  This
        allows credentials to be managed at a user, team or global level.

        :param bucket_name:         Bucket name.
        :param data_object_name:    Data object name.

        :type bucket_name:          str
        :type data_object_name:     str

        :return:                    A credentials string.
        :rtype:                     str

        :raise Exception:           If credentials missing or malformed.

        """

        data_path = data_object_name.split('/')

        try:
            cred_file, s3c = s3_search_yaml(
                bucket_name=bucket_name,
                prefix='/'.join(data_path[0:-1]),
                filename='s3.cred',
                aws_session=self.aws_session
            )
        except Exception as e:
            raise Exception(f'Cannot load S3 credentials: {e}')

        try:
            dict_check(s3c, optional={'access-key-id', 'access-secret-key', 'role-arn'})
        except ValueError as e:
            raise Exception(f'{cred_file}: Bad S3 credentials - {e}')

        if 'role-arn' in s3c:
            if 'access-key-id' in s3c:
                raise Exception('s3-credentials: Only one of role and access-key-id allowed')

            return f'aws_iam_role={s3c["role-arn"]}'

        # Access keys instead of role. Need to decrypt the secret key
        try:
            access_secret_key = kms_decrypt(s3c['access-secret-key'],
                                            aws_session=self.aws_session).strip()
            return f'aws_access_key_id={s3c["access-key-id"]};aws_secret_access_key={access_secret_key}'
        except KeyError:
            raise Exception('s3-credentials: Must contain role-arn or access-key-id/access-secret-key')
        except Exception as e:
            # Probably decryption error
            raise Exception(f's3-credentials: {e}')

    # ----------------------------------------------------------------------------
    def do_actions(self, action_key, record):
        """
        Execute any post load actions. These are specified as a list in the
        control object. Each action contains an "action" key which identifies the
        action to be performed. If the action is xyz, there must be an action_xyz
        method to handle it. Actions can be filtered by schema and table.

        :param action_key:      The key in the control object containing the list
                                of actions. Typically 'on-fail', 'on-success' or
                                'on-retry'.
        :param record:          S3 event record

        :type action_key:       str
        :type record:           dict

        """

        bucket_name = record['s3']['bucket']['name']  # type: str
        object_name = record['s3']['object']['key']  # type: str
        control_obj = self.control_object(bucket_name, object_name)

        for a in control_obj.get(action_key, []):

            try:
                action = a['action']
                action_handler = getattr(self, 'action_' + action)
            except KeyError:
                self.logger.error('%s: "action" key required in all elements in list', action_key)
                continue
            except AttributeError:
                self.logger.error('%s: %s: unknown action', action_key, a['action'])
                continue

            self.logger.debug('do_action: %s: %s', action_key, action)

            # If the action contains schema and/or table keys, these must match
            # the values in the control object. This can either be a literal match
            # of a regex match if the value of the action's schema/table key
            # starts with /

            do_action = True
            for key in 'schema', 'table':
                value = control_obj.get(key)  # type: str
                if not value:
                    continue
                if value.startswith('/'):
                    # Its a regex we have to match table/schema name against
                    pat = value[1:]
                    self.logger.debug('do_action: %s: %s: matching %s name %s against %s',
                                      action_key, action, key, control_obj[key], pat)
                    if not re.search(pat, control_obj[key]):
                        do_action = False
                        break
                elif value.lower() != control_obj[key]:
                    do_action = False
                    break

            if not do_action:
                self.logger.debug('do_action: %s: %s: action skipped', action_key, action)
                continue

            # Run the action handler
            try:
                action_handler(record=record, params=a)
            except Exception as e:
                self.logger.warning('do_action: %s: %s: %s', action_key, action, e)

    # ----------------------------------------------------------------------------
    def action_sqs(self, record, params):
        """
        Action handler to send an SQS message. The required keys are:

        - queue:    The name or URL for the queue.

        - message:  Either a string or An object that will be JSON encoded and
                    sent as the SQS message. It is rendered using Jinja2 with
                    the control object being injected.

        :param record:      S3 event record
        :param params:      A dictionary of parameters for this action.
        :type record:       dict
        :type params:       dict[str, T]

        """

        dict_check(params, required=('queue', 'message'))
        if '.amazonaws.com/' in params['queue']:
            # Assume its a URL
            sqs_queue = self.aws_session.resource('sqs').Queue('https://' + params['queue'])
        else:
            sqs_queue = self.aws_session.resource('sqs').get_queue_by_name(QueueName=(params['queue']))

        message = params['message'] if isinstance(params['message'], str) \
            else json.dumps(params['message'], default=json_default)
        bucket_name = record['s3']['bucket']['name']  # type: str
        object_name = record['s3']['object']['key']  # type: str
        control_obj = self.control_object(bucket_name, object_name)

        sqs_queue.send_message(
            MessageBody=jinja2.Template(message).render(ctrl=control_obj)
        )

    # ----------------------------------------------------------------------------
    def action_sns(self, record, params):
        """
        Action handler to send an SNS message. The required keys are:

        - topic:    The topic ARN.

        - message:  Either a string or an object that will be JSON encoded and
                    sent as the SNS message. It is rendered using Jinja2 with
                    the control object being injected.

        Optional keys:

        - subject:  Message subject string. It is rendered using Jinja2 with the
                    control object being injected.

        :param record:      S3 event record
        :param params:      A dictionary of parameters for this action.
        :type record:       dict
        :type params:       dict[str, T]
        """

        dict_check(params, required=('topic', 'message'))

        sns = self.aws_session.client('sns', config=Config(signature_version='s3v4'))

        message = params['message'] if isinstance(params['message'], str) \
            else json.dumps(params['message'], default=json_default)
        bucket_name = record['s3']['bucket']['name']  # type: str
        object_name = record['s3']['object']['key']  # type: str
        control_obj = self.control_object(bucket_name, object_name)

        args = {
            'TopicArn': params['topic'],
            'Message': jinja2.Template(message).render(ctrl=control_obj)
        }

        try:
            args['Subject'] = jinja2.Template(params['subject']).render(ctrl=control_obj)
        except KeyError:
            pass

        sns.publish(**args)

    # ----------------------------------------------------------------------------
    def action_notify(self, record, params):
        """
        Action handler to interface to the messenger subsystem. The required keys
        are:

        - to:       THe destination in the format required by the main messenger
                    interface.

        Optional keys:

        - subject:  Message subject string. It is rendered using Jinja2 with the
                    control object being injected.

        - message:  Message body string. It is rendered using Jinja2 with the
                    control object being injected.

        Probably a good idea to supply at least one of subject and message.

        :param record:      S3 event record
        :param params:      A dictionary of parameters for this action.
        :type record:       dict
        :type params:       dict[str, T]
        """

        dict_check(params, required=('to',))

        bucket_name = record['s3']['bucket']['name']  # type: str
        object_name = record['s3']['object']['key']  # type: str
        control_obj = self.control_object(bucket_name, object_name)

        args = {}
        for k in 'message', 'subject':
            if k not in params:
                continue
            if not isinstance(params[k], str):
                raise Exception(f'{k} arg for notify must be a string')

            args[k] = jinja2.Template(params[k]).render(ctrl=control_obj)

        self.send_msg(params['to'], **args)

# Evw Event Handler Plugin - s3handler

[TOC]

## Overview

**S3handler** is an event handler plugin for the
[evw](https://bitbucket.org/murrayandrews/evw) framework that runs an arbitrary
[object processing executable](#markdown-header-object-processing-executable) in
response to an AWS S3 object event. The S3 object details are passed to the
executable via [command line arguments](#markdown-header-command-line-arguments).

**S3handler** provides a simple mechanism to run scripts (shell, Python etc.) to
handle S3 events. It can be deployed as a Lambda function with evlambda but
would be more commonly deployed on an EC2 or other Linux node using evworker.

## Processing Flow

The basic processing flow is:

1.  User initiates an S3 event (e.g. drops a file in S3).

2.  An S3 event (e.g. _ObjectCreated_) is sent to the **s3handler** handler.

3.  The handler calls the executable specified by the
    [S3HANDLER_PATH](#markdown-header-environment-variables) environment variable
    with the S3 object details as
    [command line arguments](#markdown-header-command-line-arguments).

    The return status and output are captured and logged.

## Environment Variables

In addition to the standard `EVW*` environment variables, **s3handler** also
supports the following.

|Name|Required|Description|
|-|-|
|S3HANDLER_PATH|Yes|Name of the executable. **S3handler** will respect the PATH set in the enclosing environment. Bear this in mind when specifying the executable.|
|S3HANDLER_NOTIFY_ERROR|No|A string specifying a [messenger](../messengers/README.md) destination to notify when processing of the event fails.|
|S3HANDLER_NOTIFY_OK|No|A string specifying a [messenger](../messengers/README.md) destination to notify when processing of the event succeeds.|
|S3HANDLER_NOTIFY_RETRY|No|A string specifying a [messenger](../messengers/README.md) destination to notify when processing of the event has failed but can be retried.|


Any other environment variables will also be made available in the environment
of the executable itself which can be useful for passing configuration details.

## Object Processing Executable

This object processing executable can be any executable that conforms to the
required invocation signature, including shell scripts, Python scripts etc.

Sample shell and Python scripts are contained in [../misc/s3handler](../misc/s3handler).

### Run-time Environment

The executable will be invoked directly, not via a shell. It will inherit the
environment of the controlling evworker / evlambda instance, including PATH.

Stdin will be redirected from `/dev/null` and stdout/stderr will be captured.

The executable **must**:

*   Assume there will be multiple instances running at the same time.
    
    Multi-threading is inherent in evw plugins. Take care to avoid things such
    as fixed temporary file names that will inevitably result in instances
    fighting each other. In shell scripts, `$$` is your friend.

*   Clean up after itself.

    Do not leave rubbish behind in the file system - even in the event of an
    executable crash. In shell scripts, `trap` is your friend.

*   Not take forever to do its business.

    Even though evw is multi-threaded, it has (by design) a limited number of
    threads and events will queue waiting for an available worker thread.


### Command Line Arguments

The executable must accept the following arguments. What it does with those
arguments is entirely its own business.

| Argument                | Description                                 |
| ----------------------- | ------------------------------------------- |
| `--dt` *DATE-TIME*      | Event datetime in ISO 8601 format.          |
| `--id` *EVENT-ID*       | Event ID.                                   |
| `--region` *AWS-REGION* | AWS region containing the S3 bucket.        |
| `--size` *BYTES*        | Size of the S3 object.                      |
| `--ts` *SECONDS*        | Event timestamp in seconds since the epoch. |
| `--type` *EVENT-TYPE*   | S3 event type e.g. ObjectCreated.           |
| *bucket*                | S3 bucket name.                             |
| *key*                   | S3 object key.                              |

The Sample shell and Python scripts in [../misc/s3handler](../misc/s3handler)
show how to handle the arguments.

### Output and Return Status

The executable must exit with status 0 on success. The first 2048 characters of
output will be logged as an *INFO* level message by **s3handler**.

A return status of 28 signals to **s3handler** that processing of the event can
be retried. Whether it will be retried depends on the retry settings on the SQS
queue and evworker. Why 28? Its a [really interesting
number](https://en.wikipedia.org/wiki/28_(number)).

Any other non-zero exit status indicates permanent failure. The executable
should produce a brief error message on stderr which **s3handler** will log as
an *ERROR* level message.

## IAM Requirements

_In addition_ to the standard Lambda execution permissions that allow logging to
CloudWatch, the role assigned to the Lambda function will need IAM policies to
allow the following:

*   Send messages to the SQS handoff queue (if deployed in Lambda with evlambda).
*   Read messages from the task queue (if deployed using evworker).
*   Anything required by the external script. Typically this would include
    permission to read the objects from S3 to which the events relate.



"""
evlambda/evworker plugin to run an external program when an S3 event occurs.

It can use the following environment variables:

    S3HANDLER_PATH:
        The path to the external executable. This must be set. This must return
        one of the following status values

        0   Success
        28  Retry. Why 28? I like 28. https://en.wikipedia.org/wiki/28_(number))
        *   Fail

    S3HANDLER_NOTIFY_ERROR:
    S3HANDLER_NOTIFY_OK:
    S3HANDLER_NOTIFY_RETRY:
        A string specifying a messenger destination to notify when processing of
        the event fails / succeeds / can be retried.

................................................................................

"""

# ..............................................................................
# region imports

from __future__ import print_function

import os
import subprocess
import time
import datetime

try:
    # noinspection PyCompatibility
    from urllib.parse import unquote_plus
except ImportError:
    # noinspection PyUnresolvedReferences
    from urllib import unquote_plus

try:
    from subprocess import DEVNULL  # Python 3.
except ImportError:
    DEVNULL = open(os.devnull, 'r')

import dateutil.parser

from common.event import EventProto, EventRetryException
from common.date import timedelta_to_hms

# endregion imports
# ..............................................................................

# ..............................................................................
# region constants

__author__ = 'Murray Andrews'

PLUGIN_NAME = __name__.split('.')[-1]

MAXOUT = 2048  # Truncate output from the executabl.

STATUS_RETRY = 28  # Retry if executable returns this status.


# endregion constants
# ..............................................................................


# ------------------------------------------------------------------------------
class Event(EventProto):
    """
    Handle an S3 object event by invoking an external executable (e.g. a shell
    script).

    Sample event record...

    {
        'Records': [
            {
                'eventSource': 'aws:s3',
                'eventName': 'ObjectCreated:Put',
                'eventVersion': '2.0',
                'userIdentity': {'principalId': 'AWS:AIDAJ3YYBPAYP22AQ3BNU'},
                'requestParameters': {'sourceIPAddress': '120.147.217.15'},
                'eventTime': '2016-09-24T06:02:55.886Z',
                's3': {
                    'configurationId': 'f48af588-2542-4a98-941d-eb828e291b28',
                    'object': {
                        'eTag': '6686853da3491a56c98917cc5c4ddea2',
                        'size': 5,
                        'sequencer': '0057E6170FC7ACE87B',
                        'key': 'dropbox/r.dat'
                    },
                    's3SchemaVersion': '1.0',
                    'bucket': {
                        'ownerIdentity': {'principalId': 'AVRD2C17Z6GLL'},
                        'arn': 'arn:aws:s3:::dev.canis2.com',
                        'name': 'dev.canis2.com'
                    }
                },
                'awsRegion': 'ap-southeast-2',
                'responseElements': {
                    'x-amz-request-id': '0C4C63F66A7E79E3',
                    'x-amz-id-2': 'XfW80wKuyFuv26mrre2uIh7NPxw/Zcl5P3+7Pl4E3ioMPHtOExUvmIgpoensUNbGLqsqmVrDbEw='
                }
            }
        ]
    }
    """

    # --------------------------------------------------------------------------
    def __init__(self, *args, **kwargs):

        super(Event, self).__init__(*args, **kwargs)

        # ----------------------------------------
        # Environment variables

        self.s3handler = os.environ.get('S3HANDLER_PATH')
        if not self.s3handler:
            raise Exception('S3HANDLER_PATH must be set')

        # Who do we notify on error/retry/success?
        self.notify_error = os.environ.get('S3HANDLER_NOTIFY_ERROR')
        self.notify_retry = os.environ.get('S3HANDLER_NOTIFY_RETRY')
        self.notify_ok = os.environ.get('S3HANDLER_NOTIFY_OK')

    # --------------------------------------------------------------------------
    def do_record(self, record):
        """
        Process an S3 event record.

        :param record:  S3 event record
        :type record:   dict
        """

        event_time = record['eventTime'].split('.', 1)[0]
        event_dt = dateutil.parser.parse(record['eventTime']).replace(tzinfo=None)
        bucket_name = record['s3']['bucket']['name']  # type: str
        object_name = record['s3']['object']['key']  # type: str
        start_time = datetime.datetime.utcnow()

        # Prepare command args
        args = [
            self.s3handler,
            '--dt', event_time,
            '--id', self.event_id,
            '--region', record['awsRegion'],
            '--size', record['s3']['object']['size'],
            '--ts', int(time.mktime(event_dt.timetuple())),  # Epoch
            '--type', record['eventName'].split(':', 1)[0],  # Event type
            bucket_name,
            object_name
        ]
        args = [str(a) for a in args]

        # Run the command and capture output.
        try:
            output = subprocess.check_output(args, stdin=DEVNULL, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:

            # Failed -- should we retry or not?
            if e.returncode == STATUS_RETRY:
                # Yes -- retry
                if self.notify_retry:
                    try:
                        self.send_msg(
                            to=self.notify_retry,
                            subject='{} retry for {}/{}'.format(PLUGIN_NAME, bucket_name, object_name),
                            message='{} retry for {}/{}\n{}\n'.format(
                                PLUGIN_NAME, bucket_name, object_name,
                                e.output[:MAXOUT].strip() if e.output else ''
                            )
                        )
                    except Exception as ex:
                        # Send failed - log it but don't report the load as a failure
                        self.logger.warning('send_msg: %s', str(ex))

                raise EventRetryException('{}: Status {}: {}'.format(
                    ' '.join(args), e.returncode, e.output[:MAXOUT].strip() if e.output else ''))
            else:
                # Permanent fail
                if self.notify_error:
                    try:
                        self.send_msg(
                            to=self.notify_error,
                            subject='{} failed for {}/{}'.format(PLUGIN_NAME, bucket_name, object_name),
                            message='{} failed for {}/{}\nStatus: {}\n\n{}\n'.format(
                                PLUGIN_NAME, bucket_name, object_name,
                                e.returncode,
                                e.output[:MAXOUT].strip() if e.output else ''
                            )
                        )
                    except Exception as ex:
                        # Send failed - log it but don't report the load as a failure
                        self.logger.warning('send_msg: %s', str(ex))
                raise Exception('{}: Status {}: {}'.format(
                    ' '.join(args), e.returncode, e.output[:MAXOUT].strip() if e.output else ''))
        else:
            if self.notify_ok:
                end_time = datetime.datetime.utcnow()
                elapsed = timedelta_to_hms(end_time - start_time)
                try:
                    self.send_msg(
                        to=self.notify_ok,
                        subject='{} completed ok for {}/{}'.format(PLUGIN_NAME, bucket_name, object_name),
                        message='{} completed ok for {}/{}\n\n'
                                'Start time: {}Z\nEnd time: {}Z\nElapsed: {}h {}m {}s\n{}'.format(
                            PLUGIN_NAME, bucket_name, object_name,
                            start_time.isoformat(), end_time.isoformat(),
                            elapsed[0], elapsed[1], elapsed[2],
                            output[:MAXOUT] if output else ''
                        )
                    )
                except Exception as e:
                    # Send failed - log it but don't report the load as a failure
                    self.logger.warning('send_msg: %s', str(e))

        if output:
            self.logger.info(output[:MAXOUT].strip())

    # --------------------------------------------------------------------------
    def process(self):
        """
        Process the event.
        """

        for record in self.event['Records']:
            # Process individual records. Abort on first failure

            # S3 events have the object key URL encoded -- fix it.
            # The string cast is a bit dangerous.
            record['s3']['object']['key'] = str(unquote_plus(record['s3']['object']['key']))

            self.do_record(record)

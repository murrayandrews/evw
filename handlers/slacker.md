# Evw Event Handler Plugin - slacker

## Overview

The **slacker** event handler plugin provides a mechanism to send messages from
AWS services to Slack channels via the latter's [incoming
webhook](https://api.slack.com/incoming-webhooks) mechanism.

Currently supported AWS sources are:

*   SNS
*   CloudWatch logs.

The sending process is:

1.  Message Publishing

    A message is published by the originating AWS source (e.g. SNS) to the
    **Slacker** Lambda function.

2.  Slack Webhook Lookup

    **Slacker** looks up the source ID (e.g. SNS topic ARN) in the DynamoDB
    [webhooks table](#markdown-header-the-webhooks-table) table and obtains the
    URL of the Slack webhook and some other control information.

3.  Message Sending

    **Slacker** uses the webhook URL to send the message to Slack.

## The Webhooks Table

The webhooks table is a DynamoDB table named `slacker.webhooks`. It maps AWS
message source IDs to Slack webhook URLs. All sources feeding messages to
**slacker** must have an entry in the table.

The table must be created with `sourceId` as the primary partition key. If the
CloudFormation template is used for
[installation](#markdown-header-installation-and-configuration), the table is
created as part of the stack.

The table contains the following fields.

|Name|Required|Type|Description|
|-|-|-|-|
|sourceId|Yes|String|The source ID for the AWS message source. See [Message Sources](#markdown-header-message-sources) below for more information.|
|colour|No|String|Slack messages will have a colour bar displayed down the left margin specified by the value of this field. This can be any hex colour code or one of the Slack special values `good`, `warning` or `danger`. The default value is a light grey `#bbbbbb` unless overridden by the `SLACKER_COLOUR` [environment variable](#markdown-header-environment-variables).|
|enabled|No|boolean|If `false`, sending from this source to Slack is disabled. Default is `true`.|
|preamble|No|String|A fixed text preamble for all messages. The most useful values are things such as `<!here>` and `<!channel>` which will cause Slack to insert `@here` and `@channel` alert tags respectively.|
|url|Yes|String|The full URL of the Slack webhook.|

## Message Sources

The value of the `sourceId` field of the  [webhooks table](#markdown-header-the-webhooks-table)
is dependent on the source type.

|Source|Value of `sourceId`|
|-|-|
|CloudWatch|Log group name.|
|SNS|Topic ARN.|

## Installation and Configuration

While **slacker** can be installed in the same way as other **evw** modules,
it requires a number of AWS components to be present to allow it to work.
These are:

-   The DynamoDB [webhooks table](#markdown-header-the-webhooks-table).
-   An IAM role for the Lambda function itself.
-   The Lambda function itself.

All of this is automated with a CloudFormation template:
`misc/slacker/slacker.cfn.json`.  Documentation for the template can be found
[here](../misc/slacker/slacker.cfn.md).

Using this template, the process is:

1.  Clone the **evw** repository.

2.  Create a Python virtual environment and populate it using:
    Python 3.6

    ```bash
    virtualenv -p python3
    source venv/bin/activate
    pip install -r requirements.txt --upgrade
    deactivate
    ```
3.  Run the following command to generate the Lambda install bundle.

    ```bash
    etc/pkg-evlambda.sh slacker
    ```

    This will produce a file `evl-slacker.zip`.

4.  Load `evl-slacker.zip` into an S3 bucket somewhere. The location will be
    required when running the CloudFormation stack.

5.  Create a new CloudFormation stack using the template
    `misc/slacker/slacker.cfn.json`. [Stack
    parameters](#markdown-header-cloudformation-template-parameters) are
    described below.

6.  Create some [Slack webhooks](https://api.slack.com/incoming-webhooks). The
    webhook URLs created as part of this process go in the [webhooks
    table](#markdown-header-the-webhooks-table).

7.  Subscribe the **slacker** Lambda function to an SNS topic.

8.  Update the [webhooks table](#markdown-header-the-webhooks-table) with
    entries to map the source ID (SNS topic ARN in this example) to the
    corresponding webhook URLs.

9.  Publish some SNS messages and Slack off.

### CloudFormation Template Parameters

|Parameter|Required|Description|
|-|-|-|
|DefaultSideBarColour|Yes|Slack messages will have a colour bar displayed down the left margin specified by the value of this field unless overridden for a given source ID in the [webhooks table](#markdown-header-the-webhooks-table). This can be any hex colour code or one of the Slack special values `good`, `warning` or `danger`. This parameter sets the `SLACKER_COLOUR` [environment variable](#markdown-header-environment-variables) for the Lambda function.|
|DynamoDbReadCapacity|Yes|Read capacity units for the DynamoDB table. The default is 5, which is quite low. This will minimise costs but may need to be increased if a lot of messages are sent.|
|MaxMessageLen|Yes|The maximum length in bytes of messages sent to Slacker. Longer messages are truncated. This parameter sets the `SLACKER_MSG_LEN` [environment variable](#markdown-header-environment-variables) for the Lambda function.|
|S3CodeBucket|Yes|The bucket in which the  `evl-slacker.zip` code bundle was placed. This must exist and must contain the code bundle prior to creating the stack.|
|S3CodeKey|Yes|The full path name of the code bundle in the `S3CodeBucket` (no leading `/`).|

### Environment Variables

In addition to the [standard environment
variables](../README.md#markdown-header-lambda-environment-variables) supported
by all **evw** handlers, **slacker** supports the following additional
variables.

|Name|Description|Default|
|-|-|-|
|SLACKER_COLOUR|The default colour for the bar on the left hand margin of messages sent to Slack unless overridden for a given source ID in the [webhooks table](#markdown-header-the-webhooks-table). This can be any hex colour code or one of the Slack special values `good`, `warning` or `danger`.|#bbbbbb|
|SLACKER_MSG_LEN|The maximum length in bytes of messages sent to Slacker. Longer messages are truncated.|4000|
|WEBHOOK_TABLE|Name of the [webhooks table](#markdown-header-the-webhooks-table).|slacker.webhooks|

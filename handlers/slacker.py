"""
Event worker plugin that forwards SNS messages to Slack channels.

Copyright (c) 2018, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import json
import os
import time

try:
    # Python 2
    # noinspection PyCompatibility
    from StringIO import StringIO
except ImportError:
    # Python 3
    from io import StringIO

from base64 import b64decode
from datetime import datetime
from gzip import GzipFile

import dateutil.parser
import requests

from common.event import EventProto

__author__ = 'Murray Andrews'

PLUGIN_NAME = __name__.split('.')[-1]

WEBHOOK_TABLE = os.environ.get('SLACKER_WEBHOOK_TABLE', PLUGIN_NAME + '.webhooks')
DEFAULT_COLOUR = os.environ.get('SLACKER_COLOUR', '#bbbbbb')
MAX_MESSAGE_LEN = int(os.environ.get('SLACKER_MSG_LEN', 4000))

EPOCH = datetime(year=1970, month=1, day=1)


# ------------------------------------------------------------------------------
def zip64_decode_data(data):
    """
    Decode a base64, gzipped JSON string into a Python object.

    :param data:    The data to decode.
    :type data:     str
    :return:        A Python object.

    :raise Exception:   If there is a decoding problem.

    """

    return json.loads(GzipFile(fileobj=StringIO(b64decode(data).decode('utf-8'))).read())


# ------------------------------------------------------------------------------
def iso8601_to_ts(iso):
    """
    Convert an ISO-8601 timestamp to a UNIX timestamp. A bit painful on Python2.

    WARNING: Only handles UTC timestamps.

    :param iso:     ISO-8601 timestamp. Must be UTC.
    :type iso:      str

    :return:        Unix timestamp.
    :rtype:         int
    """

    if not iso.endswith('Z'):
        raise ValueError('iso8601_to_ts can only handle UTC')

    d = dateutil.parser.parse(iso).replace(tzinfo=None)

    return (d - EPOCH).total_seconds()


# ------------------------------------------------------------------------------
class Event(EventProto):
    """
    Send AWS event messages to Slack. Currently handles messages from:

    - SNS
    - CloudWatch

    """

    # --------------------------------------------------------------------------
    def __init__(self, *args, **kwargs):

        super(Event, self).__init__(*args, **kwargs)

        dynamo_db = self.aws_session.resource('dynamodb')
        self.webhook_table = dynamo_db.Table(WEBHOOK_TABLE)
        self._webhooks = {}  # Cache

    # --------------------------------------------------------------------------
    def process(self):
        """
        Send messages to slack.
        """

        self.logger.debug('Process %s', str(self.event))

        # Try to work out what sort of event we have and send to a handler.
        if 'awslogs' in self.event:
            # CloudWatch event is a base64, gzipped JSON string! Get a grip AWS.
            self.cloudwatch_to_slack(zip64_decode_data(self.event['awslogs']['data']))
        elif 'Records' in self.event:
            for record in self.event['Records']:
                if record['EventSource'] == 'aws:sns':
                    self.sns_to_slack(record)
                else:
                    raise Exception('Unexpected event source: {}'.format(record['EventSource']))
        else:
            raise Exception('Unknown event type')

    # --------------------------------------------------------------------------
    def sns_to_slack(self, record):
        """
        Send an SNS message to Slack. SNS events look like this...

        {
          "Records": [
            {
              "EventVersion": "1.0",
              "EventSubscriptionArn": eventsubscriptionarn,
              "EventSource": "aws:sns",
              "Sns": {
                "SignatureVersion": "1",
                "Timestamp": "1970-01-01T00:00:00.000Z",
                "Signature": "EXAMPLE",
                "SigningCertUrl": "EXAMPLE",
                "MessageId": "95df01b4-ee98-5cb9-9903-4c221d41eb5e",
                "Message": "Hello from SNS!",
                "MessageAttributes": {
                  "Test": {
                    "Type": "String",
                    "Value": "TestString"
                  },
                  "TestBinary": {
                    "Type": "Binary",
                    "Value": "TestBinary"
                  }
                },
                "Type": "Notification",
                "UnsubscribeUrl": "EXAMPLE",
                "TopicArn": topicarn,
                "Subject": "TestInvoke"
              }
            }
          ]
        }

        :param record:      The SNS event record containing the message.
        :type record:       dict[str, T]
        """

        topic_arn = record['Sns']['TopicArn']  # type: str

        self.slack(
            source_id=topic_arn,
            source_name='SNS:' + topic_arn.rsplit(':', 1)[1],
            subject=record['Sns']['Subject'],
            message=record['Sns']['Message'],
            timestamp=iso8601_to_ts(record['Sns']['Timestamp'])
        )

    # --------------------------------------------------------------------------
    def cloudwatch_to_slack(self, record):
        """
            Sample event data.

            {
                "messageType": "DATA_MESSAGE",
                "owner": "123456789000",
                "logGroup": "logGroupName",
                "logStream": "logStreamName",
                "subscriptionFilters": ["cwatchTrigger"],
                "logEvents": [
                    {
                        "id": "32876629689493193824632746953207793042945191065307971584",
                        "timestamp": 1474231062274,
                        "message": "hello world"
                    }
                ]
            }


        :param record:      The CloudWatch log record.
        :type record:       dict[str, T]
        """

        log_group = record['logGroup']
        log_stream = record['logStream']
        source_name = 'Log:' + log_group

        for l in record['logEvents']:
            self.slack(
                source_id=log_group,
                source_name=source_name,
                subject=log_stream,
                message=l['message'],
                timestamp=l['timestamp'] // 1000
            )

    # --------------------------------------------------------------------------
    def slack(self, source_id, source_name, subject, message, timestamp=None):
        """
        Send the msg to slack.

        :param source_id:   Source identifier. Used to lookup webhooks table.
        :param source_name: A text identifier for the source.
        :param subject:     Message subject.
        :param message:     Message body.
        :param timestamp:   Message timestamp. If None, current time is used.

        :type source_id:    str
        :type source_name:  str
        :type subject:      str
        :type message:      str
        :type timestamp:    int

        """

        try:
            webhook = self.get_webhook(source_id)
        except KeyError:
            raise Exception('Missing or malformed Slack webhook for {}'.format(source_id))

        if not webhook.get('enabled', True):
            self.logger.info('Dropping message "%s" - channel not enabled', subject)
            return

        slack_msg = {
            'attachments': [
                {
                    'fallback': message[:MAX_MESSAGE_LEN],
                    'author_name': source_name,
                    'color': webhook.get('colour', DEFAULT_COLOUR),
                    'title': subject,
                    'text': message[:MAX_MESSAGE_LEN],
                    'footer': 'Event time',
                    'ts': timestamp if timestamp else int(time.time())
                }
            ]
        }

        # Add in optional intro text (e.g. <!here>)
        try:
            slack_msg['text'] = webhook['preamble']
        except KeyError:
            pass

        response = requests.post(
            webhook['url'], data=json.dumps(slack_msg),
            headers={'Content-Type': 'application/json'}
        )
        if response.status_code != 200:
            raise Exception('Slack response {} - {}'.format(response.status_code, response.text))

        self.logger.info('Message from %s (%s) sent to %s', source_name, source_id, webhook['url'])

    # --------------------------------------------------------------------------
    def get_webhook(self, source_id):
        """
        Retrieve the Slack webhook for the given topic ARN from DynamoDB. These
        are cached.

        :param source_id:   AWS source identifier. e.g for an SNS message this
                            is the topic ARN.
        :type source_id:    str

        :return:            The DynamoDB entry for the given source ID which
                            contains the webhook URL and some supporting
                            information.
        :rtype:             dict[str, T]

        :raise KeyError:    If the source ID isn't in the SLACK_WEBHOOK_TABLE.
        """

        if source_id not in self._webhooks:
            self._webhooks[source_id] = self.webhook_table.get_item(Key={'sourceId': source_id})['Item']

        return self._webhooks[source_id]

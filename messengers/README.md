# Messenger Plugins

Event handler plugins have access to a `send_msg()` method that can
send a message to someone or something.

The destinations are always pseudo URLs of the form

```
plugin://address
```

The dest-address can, optionally, be followed with a URL query string
providing plugin specific parameters.  e.g.

```
plugin://address?x=10&p=Hello%20World
```

New plugins can be added as required.
Currently available messenger plugins are:

|Plugin|Description|
|-|-|
|disabled|Sending via this messenger always fails. It is mostly used by [mbot](../handlers/mbot.md) to allow a sending mode to be disabled.|
|s3|Write the message to an S3 file. The address is of the form `bucket/object` or `bucket/object?kmskey=key-alias`. In the second case the file will be KMS encrypted using the key with the specified alias instead of the default AES256 encryption. The object name may contain `{}` which will be replaced with the UTC date/time in ISO 8601 format. Take care with S3 event triggers when using this messenger to avoid infinite event loops.|
|[ses](#markdown-header-ses)|Send an email via AWS SES. The address is an email address.|
|sms|This is a synonym for `smssns` for historical reasons.
|smssns|Send an SMS text message. The address is a full MSISDN (international mobile phone number with no leading +). AWS SNS is used to do the sending.|
|sns|Send a message to an AWS SNS topic. The address is a topic ARN.|
|sqs|Send a message to an AWS SQS queue. The address is either a queue name or a queue URL. In the latter case _do not_ include the `https://` prefix in the address component.|
|[pushover](#markdown-header-pushover)|Send a push notification message via [Pushover](https://pushover.net). The address is a Pushover user key.|


Event handlers that use AWS based messenger plugins will require appropriate IAM
permissions to do so.

Some of the messengers require further configuration via environment variables.
For `evworker`, environment variables are provide via the normal operating
system environment or using either the `-e`/`--environ` or `-E`/`--environ-file`
options.  For `evlambda`, environment variables are set via the normal AWS
Lambda mechanism for environment variables.


## SES

AWS Simple Email Service (SES) is the email messenger. It requires that
at least one email domain be verified in the SES region used for sending.

The handler requires the following IAM permissions:

*   `ses:ListIdentities`
*   `ses:SendEmail`.

### Environment Variables for the SES messenger

|Name|Required|Description|
|-|-|-|
|EVW_SES_DOMAIN|No|The domain part of the source address for emails sent via SES. If not specified, the SES `ListIdentities` function is used to find a verified domain and one is selected at random from the list.|
|EVW_SES_REGION|No|The AWS region to use for SES. The default is `us-east-1`.|
|EVW_SES_SUBDOMAIN|No|If not empty and `EVW_SES_DOMAIN` is not specified, this value will be prepended to the domain name discovered by scanning the SES configuration for a verified domain. Ignored if `EVW_SES_DOMAIN` is specified. The default is `event`.|
|EVW_SES_USER|No|The user name part of the source address for emails sent via SES. The default is `no-reply`.|

## Pushover

Pushover is a mobile push notification service that supports iOS and Android
devices.

### Setup

To use the _pushover_ messenger, a Pushover account is required. This is free.

Users need to download the appropriate app. This is free for 7 days and there is
then a small one-off charge.

Once a Pushover account is created, create a Pushover _application_. This will
yield an API Token/Key for the application. This is a 30 character random
string.  The pushover messenger plugin needs this value provided via the
`EVW_PUSHOVER_APP_KEY` environment variable.


### Environment Variables for the Pushover Messenger

|Name|Required|Description|
|-|-|-|
|EVW_PUSHOVER_APP_KEY|Yes|The application key provided by Pushover. Treat it as sensitive data.|

### Parameters for Sending Pushover Messages

|Parameter|Description|
|-|-|
|device|Pushover allows a user to register multiple devices and messages can optionally target one device by name.|
|priority|One of `silent`, `quiet`, `normal`, `high`, `confirm`. These correspond to the [Pushover defined priorities](https://pushover.net/api#priority). The default is `normal`.|
|sound|Allowed values are as per the [Pushover defined sounds](https://pushover.net/api#sounds) Default is `pushover`.





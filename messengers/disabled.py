"""
Messenger plugin to disable sending. Sending via this messenger always fails.
This is mostly for mbot to allow a sending mode to be disabled.

The configuration of this plugin is a bit agricultural. If an AWS SES domain
is not specified in the SENDER_DOMAIN constant, we look for verified domains
in the SES service for the account and grab one at random.

................................................................................
Copyright (c) 2017, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
................................................................................

"""

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def send(to, subject, message, aws_session=None, **kwargs):
    """
    Always fails.

    :param to:          Not used.
    :param subject:     Not used.
    :param message:     Message body.
    :param aws_session: A boto3 session object. If None a default session will
                        be created. Default None.
    :param kwargs:      Ignored.

    :type to:           str
    :type subject:      str
    :type message:      str
    """

    raise Exception('Sending disabled')

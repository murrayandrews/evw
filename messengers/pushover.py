"""
Messenger plugin to use send a message via Pushover (http://pushover.net).
This is a port from MessageFlex.

Pushover app key must be specified via environment variable EVW_PUSHOVER_APP_KEY.

Copyright (c) 2017, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import os
import requests

from common.utils import shorten_string

__author__ = 'Murray Andrews'

MAX_SUBJECT_LEN = 250
MAX_BODY_LEN = 1024
PUSHOVER_SEND_URL = 'https://api.pushover.net:443/1/messages.json'

PUSHOVER_PRIORITIES = {
    'silent': -2,  # No notification
    'quiet': -1,  # Quiet notification
    'normal': 0,  # Normal
    'high': 1,  # Bypass user's quiet hours
    'confirm': 2,  # Require user confirmation
}


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def send(to, subject, message, aws_session=None, priority='normal', device=None,
         sound=None, **kwargs):
    """

    :param to:          Destination. A Pushover user key
    :param subject:     Message subject.
    :param message:     Message body.
    :param aws_session: A boto3 session object. If None a default session will
                        be created. Default None. Not used in this messenger.
    :param priority:    Message priority. Can be an integer between -2 and 2
                        (inclusive) or one of 'silent', 'quiet', 'normal',
                        'high' or 'confirm' (case insensitive). See the Pushover
                        site for explanation of the integer values.
    :param device:      A Pushover device label for the target user. If not
                        specified all registered devices for the user are
                        targetted.
    :param sound:       One of the allowed Pushover sound names (bike, bugle,
                        cosmic etc.)
    :param kwargs:      Ignored.

    :type to:           str
    :type subject:      str
    :type message:      str
    :type aws_session:  boto3.Session
    :type priority:     int | str
    :type device:       str
    :type sound:        str

    """

    app_key = os.environ.get('EVW_PUSHOVER_APP_KEY')
    if not app_key:
        raise Exception('Pushover messenger requires EVW_PUSHOVER_APP_KEY to be set')

    # ----------------------------------------
    # Construct Pushover API call payload

    title = shorten_string(subject, MAX_SUBJECT_LEN, '...')
    message = shorten_string(message, MAX_BODY_LEN - len(title), ' (more)')

    msg_dict = {
        'user': to,
        'token': app_key,
        'title': title,
        'message': message,
    }

    if priority.lower() in PUSHOVER_PRIORITIES:
        msg_dict['priority'] = PUSHOVER_PRIORITIES[priority.lower()]
    else:
        try:
            priority = int(priority)
            if not -2 <= priority <= 2:
                raise ValueError('priority out of range')
            msg_dict['priority'] = priority
        except ValueError:
            raise Exception('Invalid Pushover message priority: {}'.format(priority))

    if device:
        msg_dict['device'] = device

    if sound:
        msg_dict['sound'] = sound

    # ---------------------------------------
    # Send the Pushover message. Let exceptions propagate
    result = requests.post(PUSHOVER_SEND_URL, data=msg_dict)
    if not result.ok:
        raise Exception('{} - {}'.format(to, result.status_code, result.reason))

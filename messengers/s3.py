"""
Messenger plugin to write the message to a file on S3.

................................................................................
Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
................................................................................

"""

from datetime import datetime

import boto3
from common.aws_utils import s3_split
from botocore.client import Config

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def send(to, subject, message, aws_session=None, kmskey=None, **kwargs):
    """
    Write the message to a file in S3.

    :param to:          Destination file. Must be bucket/object or
                        bucket/object:kms-key-alias. In the second case the file
                        will be KMS encrypted using the specified key. The
                        object name may contain {} which will be replaced with
                        the UTC date/time in ISO 8601 format.
    :param subject:     Message subject.
    :param message:     Message body.
    :param aws_session: A boto3 session object. If None a default session will
                        be created. Default None.
    :param kmskey:      Alias for a KMS key to encrypt the message file. Don't
                        include the alias/ prefix. If None, the S3 object is not
                        encrypted.
    :param kwargs:      Ignored.

    :type to:           str
    :type subject:      str
    :type message:      str
    :type kmskey:       str
    """

    if not aws_session:
        aws_session = boto3.Session()

    bucket_name, s3_object_key = s3_split(to)

    s3 = aws_session.resource('s3', config=Config(signature_version='s3v4'))
    bucket = s3.Bucket(bucket_name)

    put_args = {
        'Key': s3_object_key.format(datetime.utcnow().isoformat().rsplit('.', 1)[0]),
        'Body': 'Subject: {}\n\n{}\n'.format(subject, message),
        'ContentType': 'text/plain'
    }

    if kmskey:
        put_args['ServerSideEncryption'] = 'aws:kms'
        put_args['SSEKMSKeyId'] = 'alias/' + kmskey
    else:
        put_args['ServerSideEncryption'] = 'AES256'

    bucket.put_object(**put_args)

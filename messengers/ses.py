"""
Messenger plugin to send an email via AWS SES. Note that SES is not available
in all regions so here we enforce us-east-1.

................................................................................
Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
................................................................................

"""

import os
import boto3

__author__ = 'Murray Andrews'

REGION = os.environ.get('EVW_SES_REGION', 'us-east-1')

SES_USER = os.environ.get('EVW_SES_USER', 'no-reply')
SES_SUBDOMAIN = os.environ.get('EVW_SES_SUBDOMAIN', 'event')
# If SENDER_DOMAIN is None, try to find a verified domain and use that.
SES_DOMAIN = os.environ.get('EVW_SES_DOMAIN')


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def send(to, subject, message, aws_session=None, **kwargs):
    """
    Send an email via SES. Note that SES is not available in all regions so we
    force us-east-1.

    :param to:          Destination email address. Must be SES verified email.
    :param subject:     Message subject.
    :param message:     Message body.
    :param aws_session: A boto3 session object. If None a default session will
                        be created. Default None.
    :param kwargs:      Ignored.

    :type to:           str
    :type subject:      str
    :type message:      str
    """

    global SES_DOMAIN

    if not aws_session:
        aws_session = boto3.Session()

    ses = aws_session.client('ses', region_name=REGION)

    if not SES_DOMAIN:
        identities = ses.list_identities(IdentityType='Domain')['Identities']
        if not identities:
            raise Exception('No verified domains - cannot use AWS SES')

        # Just pick the first one and subdomain it.
        SES_DOMAIN = SES_SUBDOMAIN + '.' + identities[0] if SES_SUBDOMAIN else identities[0]

    ses.send_email(
        Source=SES_USER + '@' + SES_DOMAIN,
        Destination={
            'ToAddresses': [to]
        },
        Message={
            'Subject': {
                'Data': subject,
                'Charset': 'UTF-8'
            },
            'Body': {
                'Text': {
                    'Data': message + '\n\n---\nDo not reply to this email.',
                    'Charset': 'UTF-8'
                }
            }
        }
    )

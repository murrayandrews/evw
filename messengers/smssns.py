"""
Messenger plugin to send an SMS. Uses the AWS SNS service.

WARNING: There are better / cheaper SMS services out there.

Copyright (c) 2016, Murray Andrews
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1.  Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

2.  Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

3.  Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""


import boto3

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def send(to, subject, message, aws_session=None, **kwargs):
    """
    Send an SMS message.

    :param to:          A single MSISDN. Must include the full country code etc.
    :param subject:     Message subject. This will be used as the body of the
                        SMS. If not specified then the message param will be
                        used instead. Default None.
    :param message:     Message body. Ignored unless no subject is provided in
                        which case this is used as the SMS text instead.
                        Default None. At least one of subject/message must
                        be provided.
    :param aws_session: A boto3 session object. If None a default session will
                        be created. Default None.
    :param kwargs:      Ignored.

    :type to:           str
    :type subject:      str
    :type message:      str
    :type aws_session:  boto3.Session
    """

    if not aws_session:
        aws_session = boto3.Session()

    if not subject:
        subject = message

    sns = aws_session.client('sns')

    sns.publish(
        PhoneNumber=to,
        Subject=subject[:100],
        Message=message
    )

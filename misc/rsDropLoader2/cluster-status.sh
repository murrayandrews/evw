#!/bin/bash

PROG=`basename $0`
CLUSTERS_TABLE=evw.rsDropLoader.clusters

# ------------------------------------------------------------------------------
function usage {
	cat >&2 <<!

Manage the status of entries in the droploader cluster table.

Usage: $PROG [-h] [-d|-e|-p] cluster-id ...

Args:
  -h  --help		Print help and exit.
  -d  --disable  --off	Set the status of the cluster to "off".
  -e  --enable  --on	Set the status of the cluster to "on".
  -p  --pause		Set the status of the cluster to "paused".
  cluster-id		One or more cluster identifiers referencing entries in
    			the DynamoDB clusters table ($CLUSTERS_TABLE).
 
If none of -d, -e or -p are specified, the current status is displayed without
changing anything.

!
}


# ------------------------------------------------------------------------------
function abort {
	echo "$*" >&2
	exit 1
}

# Get current status for the specified cluster from the clusters table.
function get_cluster_status {
	local result status
	result=$(
		aws dynamodb get-item --table-name $CLUSTERS_TABLE \
			--key "{\"cluster\": { \"S\": \"$1\" }}" \
			2>&1
		)

	[ "$result" == "" ] && echo No such cluster && return 1
	
	echo "$result" | \
		python3 -c "import json, sys; print(json.load(sys.stdin).get('Item',{}).get('status',{}).get('S', 'on'))"
}

# Update clusters table for specified cluster.
# Usage: set_cluster_status cluster status
function set_cluster_status {
	local result status
	result=$(
		aws dynamodb update-item --table-name $CLUSTERS_TABLE \
			--key "{\"cluster\": { \"S\": \"$1\" }}" \
			--return-values UPDATED_NEW \
			--condition-expression "attribute_exists(#C)" \
			--update-expression 'SET #S = :s' \
			--expression-attribute-names '{ "#C" : "cluster", "#S" : "status" }' \
			--expression-attribute-values "{ \":s\" : { \"S\" : \"$2\" } }" \
			--output text \
			--query Attributes.status.S 2>&1
		)
	status=$?
	[[ $result =~ "ConditionalCheckFailedException" ]] && echo No such cluster && return $status
	echo $result
	return $status
}

# ------------------------------------------------------------------------------
# Process args.

declare -i e_flag=0
declare -i d_flag=0
declare -i p_flag=0
cluster_status=

for i do
	case "$1"
	in
		-h | --help)
			echo frog
			usage; exit 0;;
		-d | --disable | --off)
			d_flag=1; cluster_status=off; shift;;
		-e | --enable | --on)
			e_flag=1; cluster_status=on; shift;;
		-p | --pause)
			p_flag=1; cluster_status=paused; shift;;
		--)	break;;
		-*)	usage; exit 1;;
		*)	break;;
	esac
done

# Check mutually exclusive flags
if [ $((d_flag + e_flag + p_flag)) -gt 1 ]
then
	echo "$PROG: Only one of disable, enable or pause allowed" >&2
	exit 1 
fi

[ $# -eq 0 ] && usage exit 1;

# ------------------------------------------------------------------------------
errors=0
for cluster
do
	if [ "$cluster_status" != "" ]
	then
		# Update cluster status
		result=$(set_cluster_status "$cluster" $cluster_status)
		[ $? -ne 0 ] && errors=1
		echo "$cluster: $result"
	else
		# Show current status
		result=$(get_cluster_status "$cluster")
		[ $? -ne 0 ] && errors=1
		echo "$cluster: $result"
	fi
done

exit $errors


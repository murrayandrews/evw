/*
********************************************************************************
Purpose:      Create the logging table in the logging cluster. Modify the schema
              name, table name and index names as desired.

Columns:
  eventId:    A unique identifier for the load request. Typically based on an
              S3 event ID.
  eventTime:  UTC time of the event in S3.
  startTime:  UTC time when processing of the event by rsDropLoader started.
  endTime:    UTC time when process of the event finished, successfully or
              otherwise.
  eventSource: Where the event originated. Usually aws:s3.
  sourceIp:   IP address of the machine that performed the S3 operation.
  cluster:    Target Redshift cluster alias.
  schemaName: Name of the target schema in Redshift.
  realTableName:
              Name of the actual table to be loaded in Redshift. This may not be
              known when the job starts and will be NULL but should be populated
              by the time the job finishes. For all but "switch" mode loads,
              this will be the same as the tablename. For "switch" mode, the
              tablename is the virtual target of the load (e.g. mytable) and
              realtablename will be either of the two switch tables (e.g.
              mytable_a or mytable_b).
  tableName:  Name of the virtual table to be loaded in Redshift.
  status:     One of "starting", "complete" or "failed".
  bucket:     S3 bucket name of source data.
  object:     S3 object key of source data.
  statusInfo: Free-form text message, typically error messages.

********************************************************************************
*/

CREATE DATABASE IF NOT EXISTS evw;

USE evw;

DROP TABLE IF EXISTS rsdroploader;

CREATE TABLE IF NOT EXISTS rsdroploader (
  eventId       VARCHAR(40)                        NOT NULL
  COMMENT 'Unique event ID for the load request.',

  eventTime     DATETIME                           NOT NULL
  COMMENT 'UTC time of the event in S3.',

  logTime       DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
  COMMENT 'Time the log record was created',

  startTime     DATETIME                           NOT NULL
  COMMENT 'UTC time when processing of the event started.',

  endTime       DATETIME
  COMMENT 'UTC time when process of the event finished,',

  eventSource   VARCHAR(20)                        NOT NULL
  COMMENT 'Where the event originated. Usually aws:s3.',

  sourceIp      VARCHAR(45)
  COMMENT 'IP address of the machine that performed the S3 operation.',

  cluster       VARCHAR(128)                       NOT NULL
  COMMENT 'Target Redshift cluster alias.',

  schemaName    VARCHAR(127)                       NOT NULL
  COMMENT 'Name of the target schema in Redshift.',

  realTableName VARCHAR(127)
  COMMENT 'Name of the actual table to be loaded in Redshift.',

  tableName     VARCHAR(127)                       NOT NULL
  COMMENT 'Name of the virtual table to be loaded in Redshift.',

  status        VARCHAR(10)                        NOT NULL
  COMMENT 'One of "starting", "complete" or "failed".',

  bucket        VARCHAR(63)                        NOT NULL
  COMMENT 'S3 bucket name of source data.',

  object        VARCHAR(1024)                      NOT NULL
  COMMENT 'S3 object key of source data.',

  statusInfo    VARCHAR(256)
  COMMENT 'Free-form text message, typically error messages.'
)
  COMMENT 'Logging table for Redshift droploader (rsDropLoader).';

CREATE INDEX rsdroploader_eventId
  ON rsdroploader (eventId);
CREATE INDEX rsdroploader_rstable
  ON rsdroploader (cluster, schemaname, tablename);
CREATE INDEX rsdroploader_logTime
  ON rsdroploader (logTime)


#!/usr/bin/env python

"""
Sample script for the s3handler plugin.

Return status means:

    0   Success
    28  Retry (Why 28? Why not? I like 28. https://en.wikipedia.org/wiki/28_(number))
    *   Fail

"""

from __future__ import print_function

import argparse
import os
import sys

__author__ = 'Murray Andrews'

PROG = os.path.basename(sys.argv[0])


# ------------------------------------------------------------------------------
def process_cli_args():
    """
    Process the command line arguments.
    :return:    The args namespace.
    """

    argp = argparse.ArgumentParser(
        prog=PROG,
        description='Sample Python script for the s3handler evw plugin.',
        epilog='See https://bitbucket.org/murrayandrews/evw for more information.'
    )

    argp.add_argument('--dt', metavar='DATE-TIME', help='Event datetime in ISO 8601 format.')
    argp.add_argument('--id', metavar='EVENT-ID', help='Event ID.')
    argp.add_argument('--region', metavar='AWS-REGION', help='AWS region containing the S3 bucket.')
    argp.add_argument('--size', metavar='BYTES', help='Size of the S3 object.')
    argp.add_argument('--ts', metavar='SECONDS', help='Event timestamp in seconds since the epoch.')
    argp.add_argument('--type', metavar='EVENT-TYPE', help='S3 event type e.g. ObjectCreated.')
    argp.add_argument('bucket', help='S3 bucket name.')
    argp.add_argument('key', help='S3 object key.')

    args = argp.parse_args()

    return args


# ------------------------------------------------------------------------------
def main():
    """
    Do the business.

    """

    args = process_cli_args()

    # ----------------------------------------
    # Real processing happens here. For the sample, just echo the args.

    print('\nCommand line args:\n')
    for arg in (a for a in sorted(dir(args)) if not a.startswith('_')):
        print('    {:8}{}'.format(arg + ':', getattr(args, arg)))
    print()


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    # exit(main())  # Uncomment for debugging
    try:
        exit(main())
    except Exception as ex:
        print('{}: {}'.format(PROG, ex), file=sys.stderr)
        exit(1)
    except KeyboardInterrupt:
        print('Interrupt', file=sys.stderr)
        exit(2)

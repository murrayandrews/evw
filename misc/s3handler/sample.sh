#!/bin/bash

# Sample script for the s3handler plugin.

# Return status means:
#
#     0   Success
#     28  Retry (Why 28? Why not? I like 28. https://en.wikipedia.org/wiki/28_(number))
#     *   Fail

PROG=$(basename $0)

# ------------------------------------------------------------------------------
function usage {
cat >&2 <<!
Usage: $PROG [-h] [--dt DATE-TIME] [--id EVENT-ID] [--region AWS-REGION]
                 [--size BYTES] [--ts SECONDS] [--type EVENT-TYPE]
                 bucket key
!
}

function help {
	usage
	cat >&2 <<!

Sample shell script for the s3handler evw plugin.

positional arguments:
  bucket               S3 bucket name.
  key                  S3 object key.

optional arguments:
  -h, --help           show this help message and exit
  --dt DATE-TIME       Event datetime in ISO 8601 format.
  --id EVENT-ID        Event ID.
  --region AWS-REGION  AWS region containing the S3 bucket.
  --size BYTES         Size of the S3 object.
  --ts SECONDS         Event timestamp in seconds since the epoch.
  --type EVENT-TYPE    S3 event type e.g. ObjectCreated.

See https://bitbucket.org/murrayandrews/evw for more information.
!
}

# ------------------------------------------------------------------------------
# Command line options

for i
do
	case "$1"
	in
		-h|--help) help; exit;;
		--dt)	dt="$2"; shift 2;;
		--id)	id="$2"; shift 2;;
		--region) region="$2"; shift 2;;
		--size)	size="$2"; shift 2;;
		--ts)	ts="$2"; shift 2;;
		--type) type="$2"; shift 2;;
		-*)	bad="$bad $1"; shift;;
		*)	break;
	esac
done

if [ "$bad" != "" ]
then
	usage
	echo "$PROG: error: unrecognized arguments:$bad" >&2
	exit 1
fi


[ $# -lt 2 ] && usage && echo "$PROG: error: too few arguments" >&2 && exit 1
bucket="$1"
key="$2"
shift 2
[ $# -ne 0 ] && usage && echo "$PROG: error: unrecognized arguments: $*" >&2 && exit 1

# ------------------------------------------------------------------------------
# Real processing happens here. For the sample, just echo the args.

cat <<!

Command line args:

    bucket: $bucket
    dt:     $dt
    id:     $id
    key:    $key
    region: $region
    size:   $size
    ts:     $ts
    type:   $type

!

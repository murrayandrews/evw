
# Slacker (evw plugin) setup

>Generated by [cfdoc](https://bitbucket.org/murrayandrews/cfdoc "Open CFdoc project site") on Mon Jul  2 08:27:04 2018.


## Contents
1.  [Description](#markdown-header-description)
1.  [Details](#markdown-header-details)
1.  [Parameters](#markdown-header-parameters)
1.  [Resource Summary](#markdown-header-resource-summary)
    * [Lambda Resources](#markdown-header-lambda-resources)
    * [DynamoDB Resources](#markdown-header-dynamodb-resources)
    * [IAM Resources](#markdown-header-iam-resources)
1.  [Outputs](#markdown-header-outputs)
1.  [Resource Details](#markdown-header-resource-details)
1. [AWS CloudFormation documentation](http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/Welcome.html "AWS CloudFormation documentation")



## [Description](#markdown-header-slacker-(evw-plugin)-setup)

Setup the basic components for the Slacker evw plugin.




## [Details](#markdown-header-slacker-(evw-plugin)-setup)

|Item|Value|
|-|-|
|Author|Murray Andrews|




## [Parameters](#markdown-header-slacker-(evw-plugin)-setup)

|Parameter|Type|Description|
|-|-|-|
|DefaultSideBarColour|String|Default colour for sidebar on Slack messages (e.g. #ab1234). Must be # followed by 6 hex digits or good, warning or danger.|
|DynamoDbReadCapacity|Number|Read capacity units for the DynamoDB tables.|
|MaxMessageLen|Number|Maximum length of messages sent to Slack. Longer messages are truncated. Must be a non-negative integer.|
|S3CodeBucket|String|S3 bucket containing the Lambda function code bundle.|
|S3CodeKey|String|Location of Lambda function zip file in the code bucket. Must be an S3 object key ending in .zip.|




## [Resource Summary](#markdown-header-slacker-(evw-plugin)-setup)



### [Lambda Resources](#markdown-header-slacker-(evw-plugin)-setup)
|Id|Type|Description|
|-|-|-|
|[lambdaSlacker](#markdown-header-lambdaslacker)|Function|Create the Lambda function. Code is read from code bucket in S3.|


### [DynamoDB Resources](#markdown-header-slacker-(evw-plugin)-setup)
|Id|Type|Description|
|-|-|-|
|[dynWebhooks](#markdown-header-dynwebhooks)|Table|DynamoDB Slack webhooks table.|


### [IAM Resources](#markdown-header-slacker-(evw-plugin)-setup)
|Id|Type|Description|
|-|-|-|
|[iamRoleSlacker](#markdown-header-iamroleslacker)|Role|IAM role for the evw based slacker Lambda function.|







## [Resource Details](#markdown-header-slacker-(evw-plugin)-setup)

### [dynWebhooks](#markdown-header-dynamodb-resources)
|Property|Value|
|-|-|
|Type|[AWS::DynamoDB::Table](http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-dynamodb-table.html "AWS documentation for this resource type")|
|Group|[DynamoDB Resources](#markdown-header-dynamodb-resources) ([dyn](#markdown-header-dynamodb-resources))|
|Description|DynamoDB Slack webhooks table.



```json
{
    "Properties": {
        "AttributeDefinitions": [
            {
                "AttributeName": "sourceId",
                "AttributeType": "S"
            }
        ],
        "KeySchema": [
            {
                "AttributeName": "sourceId",
                "KeyType": "HASH"
            }
        ],
        "ProvisionedThroughput": {
            "ReadCapacityUnits": {
                "Ref": "DynamoDbReadCapacity"
            },
            "WriteCapacityUnits": 1
        },
        "TableName": {
            "Fn::Join": [
                ".",
                [
                    {
                        "Ref": "AWS::StackName"
                    },
                    "webhooks"
                ]
            ]
        }
    },
    "Type": "AWS::DynamoDB::Table"
}
```




### [iamRoleSlacker](#markdown-header-iam-resources)
|Property|Value|
|-|-|
|Type|[AWS::IAM::Role](http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-iam-role.html "AWS documentation for this resource type")|
|Group|[IAM Resources](#markdown-header-iam-resources) ([iam](#markdown-header-iam-resources))|
|Description|IAM role for the evw based slacker Lambda function.



```json
{
    "Properties": {
        "AssumeRolePolicyDocument": {
            "Statement": [
                {
                    "Action": "sts:AssumeRole",
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "lambda.amazonaws.com"
                    }
                }
            ],
            "Version": "2012-10-17"
        },
        "Policies": [
            {
                "PolicyDocument": {
                    "Statement": [
                        {
                            "Action": [
                                "logs:CreateLogGroup",
                                "logs:CreateLogStream",
                                "logs:PutLogEvents",
                                "logs:DescribeLogStreams"
                            ],
                            "Effect": "Allow",
                            "Resource": [
                                "arn:aws:logs:*:*:*"
                            ]
                        }
                    ],
                    "Version": "2012-10-17"
                },
                "PolicyName": "cloudwatchAccess"
            },
            {
                "PolicyDocument": {
                    "Statement": [
                        {
                            "Action": [
                                "dynamodb:ListTables"
                            ],
                            "Effect": "Allow",
                            "Resource": [
                                "*"
                            ],
                            "Sid": "ListTables"
                        },
                        {
                            "Action": [
                                "dynamodb:BatchGetItem",
                                "dynamodb:DescribeTable",
                                "dynamodb:GetItem",
                                "dynamodb:Query",
                                "dynamodb:Scan"
                            ],
                            "Effect": "Allow",
                            "Resource": [
                                {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "arn:aws:dynamodb:",
                                            {
                                                "Ref": "AWS::Region"
                                            },
                                            ":",
                                            {
                                                "Ref": "AWS::AccountId"
                                            },
                                            ":table/",
                                            {
                                                "Ref": "AWS::StackName"
                                            },
                                            ".*"
                                        ]
                                    ]
                                }
                            ],
                            "Sid": "ReadAccess"
                        }
                    ],
                    "Version": "2012-10-17"
                },
                "PolicyName": "dynamoSlackerTablesAccess"
            }
        ],
        "RoleName": {
            "Ref": "AWS::StackName"
        }
    },
    "Type": "AWS::IAM::Role"
}
```




### [lambdaSlacker](#markdown-header-lambda-resources)
|Property|Value|
|-|-|
|Type|[AWS::Lambda::Function](http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-lambda-function.html "AWS documentation for this resource type")|
|Group|[Lambda Resources](#markdown-header-lambda-resources) ([lambda](#markdown-header-lambda-resources))|
|Description|Create the Lambda function. Code is read from code bucket in S3.



```json
{
    "Properties": {
        "Code": {
            "S3Bucket": {
                "Ref": "S3CodeBucket"
            },
            "S3Key": {
                "Ref": "S3CodeKey"
            }
        },
        "Description": "Send AWS event messages to Slack",
        "Environment": {
            "Variables": {
                "EVW_WORKER": "slacker",
                "SLACKER_COLOUR": {
                    "Ref": "DefaultSideBarColour"
                },
                "SLACKER_MSG_LEN": {
                    "Ref": "MaxMessageLen"
                },
                "SLACKER_WEBHOOK_TABLE": {
                    "Fn::Join": [
                        ".",
                        [
                            {
                                "Ref": "AWS::StackName"
                            },
                            "webhooks"
                        ]
                    ]
                }
            }
        },
        "FunctionName": {
            "Ref": "AWS::StackName"
        },
        "Handler": "evlambda.lambda_handler",
        "Role": {
            "Fn::GetAtt": [
                "iamRoleSlacker",
                "Arn"
            ]
        },
        "Runtime": "python2.7",
        "Timeout": 30
    },
    "Type": "AWS::Lambda::Function"
}
```




